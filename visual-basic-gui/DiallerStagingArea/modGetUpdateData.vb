Module modGetUpdateData
    '' Gets the updated data from UNCLE for a given list & compare with database
    Function getUpToDateData(list_id As Integer, list_name As String) As DataTable
        Dim process_type As String = "data_sync"
        Dim process As String = "Get update data"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)

        Dim returnTable As New DataTable

        If ids.Count > 0 Then
            '' get update data from UNCLE via API with the list_id as input parameter
            Dim json As String = post_uncle(list_name, ids.Item("batch_id"))


            '' convert UNCLE data in json format to sql code
            Dim sql As String = getUpdateDataFromJsonSql(json, list_id, list_name, ids.Item("batch_id"))


            '' send data to database to do comparison & get data back from dialler database to update Ultra with
            Try
                '' convert returned data from database into datatable
                returnTable = mySqlGetDataTable(sql)
            Catch ex As Exception
                writeErrorToDB(ex, "Error building DataTable from update sql => " & ex.Message, ids.Item("batch_id"), "sql => " & sql)
            End Try


            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If


        Return returnTable
    End Function


    '' Gets the data to change on Ultra from the UNCLE json compared with database
    Function getUpdateDataFromJsonSql(json As String, list_id As Integer, list_name As String, batch_id As Integer) As String
        Dim sql As String = "call " & mySqlGetConnDetails.Item("database") & ".get_update_data('" & json.Replace("'", "''") & "', " & getNumberArrays(json, "loanno").ToString &
            ", " & list_id.ToString & ", '" & list_name & "', " & batch_id.ToString & ");"

        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\json-sql-" & list_name & ".sql", sql)
        'MsgBox("json sql built")

        Return sql
    End Function


    '' Returns the number of arrays in a json file
    Function getNumberArrays(json As String, start_string As String) As Integer
        Dim end_string As String = Right(start_string, Len(start_string) - 1)

        Return Len(json) - Len(json.Replace(start_string, end_string))
    End Function


    '' Gets the list of lists
    Function getLists() As DataTable
        Dim returnTable As New DataTable
        Dim sql As String = ""

        Try
            Dim schemas As String() = get_ultra_schemas()

            sql = "select distinct list_id, schema_code from " & mySqlGetConnDetails.Item("database") & ".lists where schema_code in ("

            For Each item In schemas
                sql &= "'" & item & "'"

                If Not item Is schemas.Last Then
                    sql &= ", "
                End If
            Next

            sql &= ") order by list_id;"


            ' test limit
            'sql &= " limit 1"
            ' test limit

            'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\get-lists-sql.txt", sql)

            returnTable = mySqlGetDataTable(sql)
        Catch ex As Exception
            writeErrorToDB(ex, "Building list of lists failed => " & ex.Message, 0, "sql => " & sql)
        End Try

        Return returnTable
    End Function


    '' Gets the list of lists with their list names
    Function getListsWithNames() As DataTable
        Dim returnTable As New DataTable
        Dim sql As String = ""

        Try
            Dim schemas As String() = get_ultra_schemas()

            sql = "select distinct list_id, schema_code, list_name from " & mySqlGetConnDetails.Item("database") & ".lists where schema_code in ("

            For Each item In schemas
                sql &= "'" & item & "'"

                If Not item Is schemas.Last Then
                    sql &= ", "
                End If
            Next

            sql &= ") order by list_id;"


            ' test limit
            'sql &= " limit 1"
            ' test limit

            'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\get-lists-sql.txt", sql)

            returnTable = mySqlGetDataTable(sql)
        Catch ex As Exception
            writeErrorToDB(ex, "Building list of lists failed => " & ex.Message, 0, "sql => " & sql)
        End Try

        Return returnTable
    End Function
End Module




'' db compares json table with current_accounts table
''--' db returns data depending on update logic:
'' -> NewUpload => on json table but not on current_accounts table
'' -> Suppress => on max_attempts or suppress table and on json table or current_accounts table
'' -> Deactivate => active on current_accounts table and not on json table
'' -> Reactivate => on json table and inactive on current_accounts table
'' -> Alter => on json table and current_accounts table, but some values are different
'' union update queries to get single dataset as returnTable
