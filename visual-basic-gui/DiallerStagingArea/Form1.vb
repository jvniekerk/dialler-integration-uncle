Option Explicit On
Imports System.ComponentModel


'error releasing app
'https://social.msdn.microsoft.com/Forums/vstudio/en-US/dafcfdde-b238-410b-8c82-4f4afcb9aee5/vs2015-error-8007006e-unable-to-finish-updating-resource-for-binreleaseapppublishsetupexe?forum=visualstudiogeneral


Public Class Form1
    Inherits System.Windows.Forms.Form

    '' Runs when the main form (GUI) is loaded
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '' Sets the GUI buttons up correctly for use
        For Each ctrl As Control In Controls
            If TypeOf ctrl Is Button Then
                Dim btn As Button = ctrl

                btn.FlatStyle = FlatStyle.Flat

                btn.FlatAppearance.MouseOverBackColor = Color.Aqua
            End If
        Next ctrl

        '' Update the displayed time with the current time
        lblCurrDate.Text = Now.ToString

        Application.DoEvents()
    End Sub


    '' Run integration update button clicked
    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        '' Resets the timer so the length of time the sync process has been running can be displayed
        Call ResetTimer()

        '' Resets the number of errors that occurred and the number of alert emails sent
        Call reset_error_handling()

        '' Start the timer
        With tmrElapsedTime
            .Enabled = True
            .Start()
        End With

        '' Start the background async processes
        Call BackgroundDiallerUpdate()
    End Sub


    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        If bgwUpdateDialler.IsBusy Then
            MsgBox("Please STOP the Dialler Update operation before exiting!!", vbOKOnly + vbCritical, "Dialler updating still in progress")
        Else
            If MsgBox("Are you sure you want To Exit?", vbOKCancel + vbInformation, "Exit Staging Area") = 1 Then Call close_app()
        End If
    End Sub


    '' Main form (GUI) closing
    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        '' Check if the user is tring to close the app
        If e.CloseReason = CloseReason.UserClosing Then
            If bgwUpdateDialler.IsBusy Or bgwCallAttempts.IsBusy Then
                MsgBox("Close button clicked - Please WAIT for thread to finish!!", vbOKOnly + vbInformation, "Dialler update")
            End If


            '' Close the app properly
            Call stop_app()
        End If
    End Sub


    '' Closes the app properly
    Sub close_app()
        '' Wait for the async processes to compelte properly before shutting down
        While bgwUpdateDialler.IsBusy Or bgwCallAttempts.IsBusy
            Dim seconds As Integer = 5

            For i As Integer = 0 To seconds * 100
                Threading.Thread.Sleep(10)
                Application.DoEvents()
            Next
        End While


        '' Close the app
        Dispose()
    End Sub


    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        If MsgBox("Cancel button clicked - Please WAIT for thread to finish before exiting!!", vbOKCancel + vbInformation, "Dialler update") = 1 Then   '' 1 means OK was clicked
            Call stop_app()
        End If
    End Sub


    '' Stops the async processes and closes the app properly
    Public Sub stop_app()
        btnStop.Enabled = False

        '' Shows a message that the app is closing down
        btnStopping.Visible = True


        Try
            If bgwUpdateDialler.IsBusy And Not bgwUpdateDialler.CancellationPending Then
                '' Cancel the data sync process
                Call bgwUpdateDialler.CancelAsync()
            End If
        Catch ex As Exception
            MsgBox("Stopping Data sync process => " & ex.Message)
        End Try


        Try
            If bgwCallAttempts.IsBusy And Not bgwCallAttempts.CancellationPending Then
                '' Cancel the data download process
                Call bgwCallAttempts.CancelAsync()
            End If
        Catch ex As Exception
            MsgBox("Stopping Data download process => " & ex.Message)
        End Try


        '' Close the app properly
        Call close_app()
    End Sub


    '' Starts the background async processes
    Private Sub BackgroundDiallerUpdate()
        btnStop.Enabled = True
        btnRun.Enabled = False


        '' Start background workers seperately in order to monitor sender individually
        Call data_sync_sender()

        Call data_download_sender()
    End Sub


    '' Activates the data sync process
    Sub data_sync_sender()
        Try
            Call bgwUpdateDialler.RunWorkerAsync()
        Catch ex As Exception
            MsgBox("Starting Data sync process => " & ex.Message)
        End Try
    End Sub


    '' Activates the data download process
    Sub data_download_sender()
        Try
            Call bgwCallAttempts.RunWorkerAsync()
        Catch ex As Exception
            MsgBox("Starting Data download process => " & ex.Message)
        End Try
    End Sub


    '' Data sync background async worker
    Private Sub bgwUpdateDialler_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwUpdateDialler.DoWork
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)


        '' Process not cancelled
        While e.Cancel = False
            Dim cancelOK As Boolean = CheckCancel(worker)   '' Not cancelled
            Dim timeOK As Boolean = CheckTime()             '' Checks now is in the data sync time wondow


            If (timeOK And cancelOK) Then
                Dim run_every_mins As Integer = 1   '5   '' Sync only runs every 5 minutes
                Dim start_run As DateTime = Now

                '' Starts the data sync
                Call syncData()

                '' Calculate the time remaining in the 5 minutes to sleep
                Dim sleep_for As Integer = ((run_every_mins * 60) - (Now - start_run).TotalSeconds) * 1000

                If Not bgwUpdateDialler.CancellationPending Then Threading.Thread.Sleep(sleep_for)
            ElseIf cancelOK Then    '' Process not cancelled but outside time window
                '' Delete old current_accounts records
                Call deleteOldCurrentAccounts()


                '' Check if account states have been checked in the past 6 hours
                If check_accounts_states() Then
                    Call checkUltraAccountState()
                End If
            End If


            If Not cancelOK Then
                e.Cancel = True
            ElseIf Not timeOK And Not bgwUpdateDialler.CancellationPending Then
                Dim mins_to_sleep As Integer = 10

                Threading.Thread.Sleep(60 * 1000 * mins_to_sleep)
            End If
        End While
    End Sub


    '' Returns the download process start and end times
    Function get_data_download_timeOK(Optional is_start As Boolean = True) As DateTime
        Dim thisTime As DateTime

        If is_start Then
            thisTime = Today & " " & txtDownloadStart.Text.ToString
        Else
            thisTime = Today & " " & txtDownloadEnd.Text.ToString
        End If

        Return thisTime
    End Function


    '' Checks if now is within the download process time window
    Function timeOK_download() As Boolean
        If (Now > get_data_download_timeOK() And Now < get_data_download_timeOK(False)) Then
            Return True
        Else
            Return False
        End If
    End Function


    '' Data download background async worker
    Private Sub bgwCallAttempts_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwCallAttempts.DoWork
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)


        '' Process not cancelled
        While e.Cancel = False
            Dim mins_to_sleep As Integer = 5
            Dim timeOK As Boolean = timeOK_download()
            Dim cancelOK As Boolean = CheckCancel(worker)
            Dim min_rows_to_sleep As Integer = 300          '' If  more than this number of records downloaded on the previous run, don't sleep before next run


            If (timeOK Or download_rows > min_rows_to_sleep) And cancelOK Then
                '' Starts the download data process
                Call downloadDiallerData()

                If Not bgwCallAttempts.CancellationPending And download_rows < min_rows_to_sleep Then
                    Threading.Thread.Sleep(1000 * 60 * mins_to_sleep)
                End If
            ElseIf Not cancelOK Then
                e.Cancel = True
            End If


            If Not timeOK And Not bgwCallAttempts.CancellationPending And download_rows < min_rows_to_sleep Then
                mins_to_sleep = 10

                Threading.Thread.Sleep(1000 * 60 * mins_to_sleep)
            End If
        End While
    End Sub


    '' Runs when the data download process has completed successfully
    Private Sub bgwCallAttempts_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwCallAttempts.RunWorkerCompleted
        'MsgBox("Data download process completed. You can now restart the app without exiting first.")
    End Sub


    '' Runs when the data sync process has completed successfully
    Private Sub bgwUpdateDialler_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwUpdateDialler.RunWorkerCompleted
        btnStop.Enabled = False
        btnRun.Enabled = True

        With tmrElapsedTime
            .Stop()
            .Enabled = False
        End With


        'MsgBox("Data Sync Thread Complete. You can Exit the app." & Environment.NewLine & Environment.NewLine & "Please do NOT click the 'Run Dialler Update' button until the data download process end pop-up is displayed!!")
    End Sub


    '' Returns whether now is within the data sync update window
    Function CheckTime() As Boolean
        Dim startHour As String
        Dim startMinute As String = 0
        Dim endHour As String
        Dim endMinute As String = 0
        Dim todayOK As Boolean = False
        Dim timeOK As Boolean = False
        Dim thisStartBox As TextBox
        Dim thisEndBox As TextBox
        Dim thisDayBox As CheckBox
        Dim thisStart As Date = Today
        Dim thisEnd As Date = Today
        Dim returnResult As Boolean = False


        With Me
            Select Case Now.ToString("dddd")
                Case "Monday"
                    thisDayBox = .chkMonday
                    thisStartBox = .txtMonStart
                    thisEndBox = .txtMonEnd
                Case "Tuesday"
                    thisDayBox = .chkTuesday
                    thisStartBox = .txtTueStart
                    thisEndBox = .txtTueEnd
                Case "Wednesday"
                    thisDayBox = .chkWednesday
                    thisStartBox = .txtWedStart
                    thisEndBox = .txtWedEnd
                Case "Thursday"
                    thisDayBox = .chkThursday
                    thisStartBox = .txtThuStart
                    thisEndBox = .txtThuEnd
                Case "Friday"
                    thisDayBox = .chkFriday
                    thisStartBox = .txtFriStart
                    thisEndBox = .txtFriEnd
                Case "Saturday"
                    thisDayBox = .chkSaturday
                    thisStartBox = .txtSatStart
                    thisEndBox = .txtSatEnd
                Case Else
                    thisDayBox = .chkSunday
                    thisStartBox = .txtSunStart
                    thisEndBox = .txtSunEnd
            End Select
        End With


        If thisDayBox.Checked = True Then todayOK = True


        With thisStartBox.Text
            startHour = GetNumberFromBox(.ToString, 1)
            startMinute = GetNumberFromBox(.ToString, 0)
        End With
        With thisEndBox.Text
            endHour = GetNumberFromBox(.ToString, 1)
            endMinute = GetNumberFromBox(.ToString, 0)
        End With


        ''check if today is the last working day or last working friday and add an hour to start and 2 hours to end hours
        '' This is no longer relevant
        'If isLastWorkingDay(Today) Or isLastWorkingFriday(Today) Then
        '    startHour -= 1
        '    endHour += 2
        'End If


        If startHour.ToString <> "Invalid" And startMinute.ToString <> "Invalid" And endHour.ToString <> "Invalid" And endMinute.ToString <> "Invalid" Then
            thisStart = thisStart & " " & startHour.ToString & ":" & startMinute.ToString
            thisStart = DateAdd(DateInterval.Minute, -90, thisStart)
            thisEnd = thisEnd & " " & endHour.ToString & ":" & endMinute.ToString
        End If


        If Now >= thisStart And Now < thisEnd Then timeOK = True


        If timeOK And todayOK Then
            returnResult = True
        Else
            returnResult = False
        End If


        Return returnResult
    End Function


    '' Resets the data sync timer
    Sub ResetTimer()
        lblProgress.Text = "Dialler Update Time Lapsed: " & 0 & " minutes " & 0 & " seconds"
    End Sub


    '' Progresses the data sync timer
    Sub ProgressTimerUpdate()
        Dim thisText As String = lblProgress.Text.ToString

        thisText = thisText.Replace("Dialler Update Time Lapsed: ", "")
        thisText = thisText.Replace(" seconds", "")

        Dim report_mins As Integer = Microsoft.VisualBasic.Left(thisText, thisText.IndexOf(" minutes"))
        Dim secs As Integer = thisText.Replace(report_mins.ToString & " minutes ", "") + 1


        If secs > 59 Then
            report_mins += 1
            secs -= 60
        End If


        lblProgress.Text = "Dialler Update Time Lapsed: " & report_mins & " minutes " & secs & " seconds"
    End Sub


    '' Runs on each tick of the data sync timer
    Private Sub tmrElapsedTime_Tick(sender As Object, e As EventArgs) Handles tmrElapsedTime.Tick
        Call ProgressTimerUpdate()
    End Sub


    '' Timer for displaying current date & time
    Private Sub tmrCurrDate_Tick(sender As Object, e As EventArgs) Handles tmrCurrDate.Tick
        Call CurrDateTimerUpdate()
    End Sub


    '' Updates the current date & time display
    Sub CurrDateTimerUpdate()
        lblCurrDate.Text = Now.ToString
    End Sub


    Private Sub btnUpdateLists_Click(sender As Object, e As EventArgs) Handles btnUpdateLists.Click
        getUltraDownloadData("lists")

        MsgBox("Lists updates successfully", vbOKOnly, "Update Lits")
    End Sub


    Private Sub btnUpdateAgents_Click(sender As Object, e As EventArgs) Handles btnUpdateAgents.Click
        getUltraDownloadData("users")

        MsgBox("Users updates successfully", vbOKOnly, "Update Users")
    End Sub


    Private Sub btnUpdateUserStates_Click(sender As Object, e As EventArgs) Handles btnUpdateUserStates.Click
        Call getUltraUserStates()
    End Sub


    '' Keeps track of the number of errors and alert emails sent to prevent hundreds of alerts being sent
    Public Shared errors As Integer
    Public Shared emails_sent As Integer
    Public Shared download_rows As Integer

    '' Resets the above variables
    Sub reset_error_handling()
        errors = 0
        emails_sent = 0
        download_rows = 0
    End Sub





    '' Tests to run on the app
    Private Sub cmbTest_Click(sender As Object, e As EventArgs) Handles cmbTest.Click
        'Dim pword As String = "test"

        'cmbTest.Enabled = False


        'If InputBox("Please enter a password to continue", "Password", "Please enter a password") = pword Then
        '    'If InputBox("Please enter a password to continue", "Password", "test") = pword Then


        '    MsgBox("testing")




        Dim sync As String = "Data sync thread running = " & bgwUpdateDialler.IsBusy & Environment.NewLine & "Data sync thread cancelled = " & bgwCallAttempts.CancellationPending
        Dim download As String = "Data download thread running = " & bgwCallAttempts.IsBusy & Environment.NewLine & "Data doanload thread cancelled = " & bgwCallAttempts.CancellationPending
        MsgBox(sync & Environment.NewLine & Environment.NewLine & download)


        ''Try
        ''    Throw New Exception("test alert email addresses - THIS IS A TEST PLEASE IGNORE!! - josef...")
        ''Catch ex As Exception
        ''    writeErrorToDB(ex, ex.Message, 23, "Some request here...", "Some response here...")
        ''End Try






        '    MsgBox("Test complete")



        'Else
        '    MsgBox("Password incorrect. Please try again.")
        'End If


        'cmbTest.Enabled = True


        ''MsgBox("Nothing to see here at the moment!!")
    End Sub
End Class
