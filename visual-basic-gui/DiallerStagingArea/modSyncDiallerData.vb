Imports System.Xml

Module modSyncDiallerData
    '' Syncs Ultra & UNCLE data
    Sub syncData()
        Dim process_type As String = "data_sync"
        Dim process As String = "Data sync procedure"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)


        Try
            Call maxAttempts()
        Catch ex As Exception
            writeErrorToDB(ex, "Max attempts in download data failed!! => " & ex.Message, ids.Item("batch_id"))
        End Try


        If ids.Count > 0 Then
            Dim updateTypes() As String = get_update_types()

            Dim list_id As Integer
            Dim schema As String = ""
            Dim list_name As String = ""
            Dim lists As DataTable = getListsWithNames()


            If lists.Rows.Count() = 0 Then
                Try
                    getUltraDownloadData("lists")

                    lists = getListsWithNames()
                Catch ex As Exception
                    writeErrorToDB(ex, "Getting lists failed => " & ex.Message, ids.Item("batch_id"))
                End Try
            End If


            If lists.Rows.Count() > 0 Then
                Dim UltraLoginDetails As New Dictionary(Of String, String)

                Try
                    UltraLoginDetails = getUltraLoginDetails("update")
                Catch ex As Exception
                    writeErrorToDB(ex, "Building Ultra login details failed => " & ex.Message, ids.Item("batch_id"))
                End Try


                If UltraLoginDetails.Count Then
                    '' loop through the lists table and handle each row update separately
                    For Each row In lists.Rows
                        '' Gets the column ids containing the list_id, schema & list name
                        For Each col In lists.Columns
                            If col.ColumnName = "list_id" Then
                                list_id = row(col).ToString
                            ElseIf col.ColumnName = "schema_code" Then
                                schema = row(col).ToString
                            ElseIf col.ColumnName = "list_name" Then
                                list_name = row(col).ToString
                            End If
                        Next


                        If list_id > 0 And Len(schema) > 0 Then
                            If list_name = "Complaints" Then
                                '' Complaints to be dealt with separately later as it's not in UNCLE but in different complaints system
                            Else
                                '' get the data from LMS to update Ultra with
                                Dim updateTable As New DataTable

                                Try
                                    updateTable = getUpToDateData(list_id, list_name)
                                Catch ex As Exception
                                    writeErrorToDB(ex, "Building updateTable failed => " & ex.Message, ids.Item("batch_id"))
                                End Try


                                If updateTable.Rows.Count() > 0 Then
                                    '' loop through the different update types and update Ultra with each
                                    For Each updateType In updateTypes
                                        '' data from updateTable filtered by updateType
                                        Dim updateTypeTable As New DataTable

                                        Try
                                            Dim view As New DataView(updateTable, "update_type = '" & updateType & "'", "loanno", DataViewRowState.CurrentRows)

                                            updateTypeTable = addUltraColumns(removeUltraColumns(view.ToTable(), updateType), updateType)


                                            'Dim file As String = "C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\" & list_id & "-" & updateType & "-updateTypeTable-rows-" & updateTypeTable.Rows.Count() & ".json"
                                            'saveTextFile(file, getStringFromDataTable(updateTypeTable))
                                        Catch ex As Exception
                                            writeErrorToDB(ex, "Populate updateTypeTable failed => " & ex.Message, ids.Item("batch_id"), "update_type = " & updateType)
                                        End Try


                                        Dim counter As Integer = 1

                                        '' run the API update for each updateType
                                        If updateTypeTable.Rows.Count() > 0 Then
                                            '' run updates in batches to reduce string size
                                            Dim maxRows As Integer = 950
                                            Dim uploadRows As Integer = maxRows
                                            Dim requestTable As DataTable = updateTypeTable.Clone


                                            While updateTypeTable.Rows.Count() > 0
                                                '' checks if there are less rows remaining than the max batch size
                                                If updateTypeTable.Rows.Count < uploadRows Then uploadRows = updateTypeTable.Rows.Count


                                                '' Builds the table relevant to the current update type
                                                Try
                                                    For j As Integer = 0 To uploadRows - 1
                                                        requestTable.ImportRow(updateTypeTable.Rows(0))

                                                        updateTypeTable.Rows(0).Delete()
                                                    Next j
                                                Catch ex As Exception
                                                    writeErrorToDB(ex, "Populating requestTable failed => " & ex.Message, ids.Item("batch_id"))
                                                End Try


                                                If requestTable.Rows.Count() > 0 Then
                                                    '' convert update data to request XML
                                                    Dim request As String = ""

                                                    Try
                                                        request = convertDatabaseDataToSchema(BuildXML(schema, list_id, requestTable, updateType.ToString), schema)
                                                    Catch ex As Exception
                                                        writeErrorToDB(ex, "convertDatabaseDataToSchema failed => " & ex.Message, ids.Item("batch_id"))
                                                    End Try


                                                    'Dim now As String = DateTime.Now.ToString("yyyyMMdd-HHmmss")
                                                    'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\" & list_id & "-" & now & "-" &
                                                    '                updateType.ToString & "-request.xml", request)


                                                    If Len(request) > 0 Then
                                                        '' send update XML to Ultra
                                                        Dim response As String = ""

                                                        Try
                                                            response = GetUltraUpload(request, UltraLoginDetails.Item("user"), UltraLoginDetails.Item("pw"))

                                                            'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\" & list_id & "-" & now & "-" &
                                                            '            updateType.ToString & "-response.xml", response)
                                                        Catch ex As Exception
                                                            writeErrorToDB(ex, "GetUltraUpload(" & updateType & ") failed => " & ex.Message, ids.Item("batch_id"))
                                                        End Try


                                                        Try
                                                            '' store the update data on the dialler database
                                                            storeUpdateData(list_id, updateType, request, response, uploadRows, counter)
                                                        Catch ex As Exception
                                                            writeErrorToDB(ex, "Data sync storing details failed!!" & ex.Message, ids.Item("batch_id"), request, response)
                                                        End Try

                                                        counter += 1


                                                        '' parse Ultra response
                                                        If Len(response) Then
                                                            Dim responseSet As New DataSet
                                                            Dim responseTable As New DataTable
                                                            Dim tableDiff As Integer = 2


                                                            Try
                                                                responseSet.ReadXml(New XmlTextReader(New IO.StringReader(response)))


                                                                If response.IndexOf("error=""") >= 0 Then
                                                                    tableDiff = 3

                                                                    Try
                                                                        Throw New Exception("Error in Ultra New Data Upload Response")
                                                                    Catch ex As Exception
                                                                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), request, response)
                                                                    End Try
                                                                End If

                                                                responseTable = responseSet.Tables(responseSet.Tables.Count - tableDiff)


                                                                If requestTable.Rows.Count <> responseTable.Rows.Count Then
                                                                    Try
                                                                        Throw New Exception("Error in Ultra Data Update Response - request and response rows don't match")
                                                                    Catch ex As Exception
                                                                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), request, response)
                                                                    End Try
                                                                End If
                                                            Catch ex As Exception
                                                                writeErrorToDB(ex, "Building responseTable failed => " & ex.Message, ids.Item("batch_id"))
                                                            End Try


                                                            '' send parsed response to db
                                                            Dim sql As String = getResponseSql(requestTable, responseTable, updateType, list_id, ids.Item("batch_id"))

                                                            If Len(sql) > 0 Then
                                                                Try
                                                                    mySqlPutNoQuery(sql)
                                                                Catch ex As Exception
                                                                    Dim sent As String = "sql => " & sql & Environment.NewLine & "request => " & request

                                                                    writeErrorToDB(ex, "Writing Ultra response to db failed => " & ex.Message, ids.Item("batch_id"), sent, response)
                                                                End Try
                                                            Else
                                                                Try
                                                                    Throw New Exception("getResponseSql returned string of length zero")
                                                                Catch ex As Exception
                                                                    writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), "sql => " & sql, response)
                                                                End Try
                                                            End If

                                                            requestTable.Clear()
                                                        Else
                                                            Try
                                                                Throw New Exception("Ultra Data Update API failed")
                                                            Catch ex As Exception
                                                                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), request, response)
                                                            End Try
                                                        End If
                                                    Else
                                                        Try
                                                            Throw New Exception("Request string not built")
                                                        Catch ex As Exception
                                                            writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
                                                        End Try
                                                    End If
                                                Else
                                                    Try
                                                        Throw New Exception("requestTable population failed")
                                                    Catch ex As Exception
                                                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
                                                    End Try
                                                End If
                                            End While
                                        End If


                                        updateTypeTable.Clear()
                                    Next
                                End If


                                updateTable.Clear()
                            End If
                        Else
                            Try
                                'MsgBox("ERROR: list_id=" & list_id & " list_name='" & list_name & "' schema='" & schema & "' schema_len=" & Len(schema))

                                Throw New Exception("Error finding list_id or schema from Ultra lists")
                            Catch ex As Exception
                                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
                            End Try
                        End If
                    Next
                End If
            Else
                Try
                    Throw New Exception("Getting list of lists failed!!")
                Catch ex As Exception
                    writeErrorToDB(ex, "Getting lists failed => " & ex.Message, ids.Item("batch_id"))
                End Try
            End If


            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    '' Removes certain columns from the table, depending on the Ultra list and update type
    Function removeUltraColumns(table As DataTable, updateType As String) As DataTable
        Dim removeColumns(3) As String

        removeColumns(0) = "list_id"
        removeColumns(1) = "update_type"
        removeColumns(2) = "lms_status"
        removeColumns(3) = "ultra_status"


        If updateType = "NewUpload" Then
            Array.Resize(removeColumns, removeColumns.Length + 1)

            removeColumns(4) = "row_id"
        ElseIf updateType = "Suppress" Or updateType = "Deactivate" Then
            Array.Resize(removeColumns, removeColumns.Length + 16)

            removeColumns(4) = "dial_purpose"
            removeColumns(5) = "cli"
            removeColumns(6) = "priority"
            removeColumns(7) = "phone_number1"
            removeColumns(8) = "phone_number2"
            removeColumns(9) = "phone_number3"
            removeColumns(10) = "loanno"
            removeColumns(11) = "balance"
            removeColumns(12) = "first_name"
            removeColumns(13) = "surname"
            removeColumns(14) = "title"
            removeColumns(15) = "dob"
            removeColumns(16) = "unit"
            removeColumns(17) = "house_number"
            removeColumns(18) = "street"
            removeColumns(19) = "postcode"
        End If


        With table.Columns
            For Each col In removeColumns
                If .Contains(col.ToString) And col.ToString <> "" Then
                    .Remove(col.ToString)
                End If
            Next
        End With

        Return table
    End Function


    '' Add required Ultra columns to the table
    Function addUltraColumns(table As DataTable, updateType As String) As DataTable
        Dim newCol As New DataColumn("_State", GetType(String))
        newCol.DefaultValue = "4"


        If updateType <> "Suppress" And updateType <> "Deactivate" Then
            newCol.DefaultValue = "0"

            If Date.Now.Hour < 8 Then
                Dim scheduledDateCol As New DataColumn("_Schedule", GetType(String))

                scheduledDateCol.DefaultValue = Date.Today.ToString("dd/MM/yyyy") & " 07:00"

                table.Columns.Add(scheduledDateCol)

                scheduledDateCol = Nothing
            End If
        End If


        table.Columns.Add(newCol)

        newCol = Nothing

        Return table
    End Function


    '' Changes the column names to be in line with Ultra field names
    Function convertDatabaseDataToSchema(xml As String, schema As String) As String
        xml = xml.Replace("phone_number1=", "PhoneNumber1=")
        xml = xml.Replace("phone_number2=", "PhoneNumber2=")
        xml = xml.Replace("phone_number3=", "PhoneNumber3=")

        xml = xml.Replace("mobile_number=", "PhoneNumber1=")
        xml = xml.Replace("home_number=", "PhoneNumber2=")
        xml = xml.Replace("work_number=", "PhoneNumber3=")

        xml = xml.Replace("cli=", "CLI=")
        xml = xml.Replace("priority=", "_SchedulePriority=")

        xml = xml.Replace("row_id=", "RowId=")
        xml = xml.Replace("rowid=", "RowId=")
        xml = xml.Replace("State=", "_State=")
        xml = xml.Replace("Schedule=", "_Schedule=")

        xml = xml.Replace("__", "_")

        Return xml
    End Function
End Module




'Function updatePriority(table As DataTable) As DataTable
'    Dim priorityCol As New DataColumn("_SchedulePriority", GetType(String))
'    Dim scheduledCol As New DataColumn("_Scheduled", GetType(String))
'    Dim scheduledUserCol As New DataColumn("_ScheduleUserId", GetType(String))
'    Dim scheduleCol As New DataColumn("_Schedule", GetType(String))

'    scheduledCol.DefaultValue = "false"
'    scheduleCol.DefaultValue = "1901-01-01 00:00:00.000"
'    scheduledUserCol.DefaultValue = "0"


'    '' set the call priority - perhaps do this in the return data from the db instead
'    priorityCol.DefaultValue = "17"

'    With table.Columns
'        .Add(priorityCol)
'        .Add(scheduledCol)
'        .Add(scheduleCol)
'        .Add(scheduledUserCol)
'    End With

'    scheduledCol = Nothing
'    scheduledUserCol = Nothing

'    Return table
'End Function
