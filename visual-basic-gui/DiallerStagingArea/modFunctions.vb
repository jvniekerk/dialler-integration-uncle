Option Explicit On
Imports System.ComponentModel
Imports System.Data.SqlClient

Module modFunctions
    '' Returns number of rown in Ultra response XML
    Function getRowNumbers(response As String) As Integer
        Dim rows As Integer = 0

        rows = response.IndexOf("rows=") + Len("rows=") + 1

        response = Right(response, Len(response) - rows)

        rows = Left(response, response.IndexOf(""""))

        Return rows
    End Function


    '' Changes the order of date formatting
    Function reorderDateTime(inputDate As String, Optional separator As String = "-") As String
        If Len(inputDate) Then
            Dim thisDate As String = Right(Left(inputDate, 10), 4) & separator & Right(Left(inputDate, 5), 2) & separator & Left(inputDate, 2)

            Return "'" & thisDate & Right(inputDate, Len(inputDate) - 10) & "'"
        Else
            Return "null"
        End If
    End Function


    Function removeDuplicateSpaces(text As String) As String
        While text.IndexOf("  ") >= 0
            text = text.Replace("  ", " ")
        End While

        Return text
    End Function


    '' Returns a given value from an XML string
    Function GetValueFromXML(ByVal xmlString As String, ByVal searchValue As String) As String
        searchValue = searchValue & "="""

        Dim startPos As Integer = xmlString.IndexOf(searchValue) + Len(searchValue)

        xmlString = Right(xmlString, Len(xmlString) - startPos)

        Dim endPos As Integer = xmlString.IndexOf("""")

        Return Left(xmlString, endPos)
    End Function


    Function GetRowIdFromXML(ByVal xmlString As String, ByVal searchValue As String) As String
        Dim startPos As Integer
        Dim endPos As Integer
        Dim rowID As String

        searchValue = searchValue & "="""
        startPos = xmlString.IndexOf(searchValue) + Len(searchValue)
        xmlString = Right(xmlString, Len(xmlString) - startPos)
        endPos = xmlString.IndexOf("""")

        If IsNumeric(Left(xmlString, endPos)) Then
            rowID = Left(xmlString, endPos)
        Else
            rowID = 0
        End If

        Return rowID
    End Function


    '' Returns the relevant day start time box for data sync
    Function ReturnStartBox() As TextBox
        With Form1
            Select Case Now.ToString("dddd")
                Case "Monday"
                    Return .txtMonStart
                Case "Tuesday"
                    Return .txtTueStart
                Case "Wednesday"
                    Return .txtWedStart
                Case "Thursday"
                    Return .txtThuStart
                Case "Friday"
                    Return .txtFriStart
                Case "Saturday"
                    Return .txtSatStart
                Case Else
                    Return .txtSunStart
            End Select
        End With
    End Function


    '' Checks if the process has been cancelled
    Function CheckCancel(ByVal worker As BackgroundWorker) As Boolean
        If worker.CancellationPending Then
            Return False
        Else
            Return True
        End If
    End Function


    '' Grid for display on current processes run
    Sub BuildDataGridView(ByVal myReader As SqlDataReader)
        Dim formName As String = Replace(Replace(Replace("newForm" & Now.ToString, "/", "_"), " ", "_"), ":", "_")
        Dim newForm As New Form
        Dim newView As New DataGridView
        Dim dt As New DataTable

        newForm.Name = formName
        newForm.Controls.Add(newView)
        dt.Load(myReader)
        newView.DataSource = dt
        newForm.Show()

        If MsgBox("Delete form '" & newForm.Name.ToString & "'?", vbOKCancel, "Delete Form") Then
            newForm.Close()
            newForm = Nothing
        End If
    End Sub


    '' Builds the Ultra XML string
    Public Function XMLString(ByVal myReader As SqlDataReader) As String
        Dim results As String = ""
        Dim fields As Integer = myReader.FieldCount - 1
        Dim thisString As String = ""

        Do While myReader.Read()
            With myReader
                results = results & "<insertOnly"
                For k As Integer = 0 To fields
                    thisString = .GetString(k)
                    results = results & " " & .GetName(k) & "=""" &
                        Replace(Replace(Replace(Replace(thisString, ">", "&gt;"), "<", "&lt;"), "&", "&amp;"), "%", "&#37;") & """"
                Next
                results = results & "/>"
            End With
        Loop

        myReader.Close()
        Return results
    End Function


    '' Gets a value from a database readr object
    Public Function ReturnValue(ByVal myReader As SqlDataReader) As String
        Dim results As String = ""
        Dim fields As Integer = myReader.FieldCount - 1
        Dim thisString As String = ""

        Do While myReader.Read()
            With myReader
                For k As Integer = 0 To fields
                    If k > 1 Then
                        results = " " & results & .GetString(k)
                    Else
                        results = results & .GetString(k)
                    End If
                Next
            End With
        Loop

        myReader.Close()

        Return results
    End Function


    '' Gets the number from a given text box
    Function GetNumberFromBox(ByVal thisString As String, ByVal hour1minute0 As Integer) As String
        Dim value As Integer
        Dim cnt As Integer = 0

        For Each c As Char In thisString
            If c = ":" Then cnt += 1
        Next

        If cnt = 1 Then
            If hour1minute0 = 1 Then
                thisString = Left(thisString, thisString.IndexOf(":"))
            Else
                thisString = thisString.Replace(Left(thisString, thisString.IndexOf(":") + 1), "")
            End If
        End If

        If Integer.TryParse(thisString, value) Then
            Return value
        Else
            Return "Invalid"
        End If
    End Function


    '' Returns the relevant date and time from given string
    Function GetDateTime(ByVal thisDate As String) As DateTime
        Dim y As Integer = 0
        Dim mo As Integer = 0
        Dim d As Integer = 0
        Dim h As Integer = 0
        Dim mi As Integer = 0
        Dim s As Integer = 0
        Dim ms As Integer = 0
        Dim i As Integer = 0


        i = thisDate.IndexOf(";mo:")
        y = Left(thisDate, i)
        thisDate = Right(thisDate, Len(thisDate) - i - Len(";mo:"))

        i = thisDate.IndexOf(";d:")
        mo = Left(thisDate, i)
        thisDate = Right(thisDate, Len(thisDate) - i - Len(";d:"))

        i = thisDate.IndexOf(";h:")
        d = Left(thisDate, i)
        thisDate = Right(thisDate, Len(thisDate) - i - Len(";h:"))

        i = thisDate.IndexOf(";mi:")
        h = Left(thisDate, i)
        thisDate = Right(thisDate, Len(thisDate) - i - Len(";mi:"))

        i = thisDate.IndexOf(";s:")
        mi = Left(thisDate, i)
        thisDate = Right(thisDate, Len(thisDate) - i - Len(";s:"))

        i = thisDate.IndexOf(";ms:")
        s = Left(thisDate, i)
        ms = Right(thisDate, Len(thisDate) - i - Len(";ms:"))

        Dim returnDate As DateTime = New DateTime(y, mo, d, h, mi, s, ms)
        Return returnDate
    End Function


    Function GetArrayFromDataTable(ByVal table As DataTable) As String(,)
        Dim cols As Integer = table.Columns.Count - 1
        Dim rows As Integer = table.Rows.Count - 1
        Dim result(rows, cols) As String

        For i As Integer = 0 To rows
            For j As Integer = 0 To cols
                result(i, j) = table.Rows(i)(j)
            Next j
        Next i

        Return result
    End Function


    Function getStringFromDataTable(ByVal table As DataTable) As String
        Dim cols As Integer = table.Columns.Count - 1
        Dim rows As Integer = table.Rows.Count - 1
        Dim result As String = ""

        For i As Integer = 0 To rows
            For j As Integer = 0 To cols
                If j = 0 Then result &= "{"

                result &= table.Columns(j).ColumnName & "=""" & table.Rows(i)(j).ToString & """"

                If j < cols Then
                    result &= "; "
                Else
                    result &= "}"
                End If
            Next j

            If i < rows Then result &= "," & Environment.NewLine
        Next i
        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\datatable-string.txt", result)

        Return result
    End Function


    Function XMLfromDataTable(ByVal dataTable As DataTable, ByVal criteria As String) As String
        Dim xmlString As String = ""
        Dim i As Integer = 0
        Dim columns As Integer = dataTable.Columns.Count

        For Each row As DataRow In dataTable.Rows
            If criteria = "NewUpload" Then
                xmlString = xmlString & "<insertOnly"
            Else
                xmlString = xmlString & "<updateOnly"
            End If

            i = 0
            For Each item In row.ItemArray
                xmlString = xmlString & " " & dataTable.Columns(i).ColumnName & "=""" & item.ToString & """"

                i = i + 1
            Next item

            xmlString = xmlString & "/>"
        Next row

        Return xmlString
    End Function


    Public Sub saveTextFile(ByVal fileName As String, ByVal data As String)
        'C:\Users\jvniekerk.UNCLEBUCK\Desktop\test.txt
        If System.IO.File.Exists(fileName) Then
            System.IO.File.Delete(fileName)
        End If

        My.Computer.FileSystem.WriteAllText(fileName, data, True)
    End Sub


    '' Cheks if a given day is a bank holiday
    Public Function checkHoliday(thisDate As Date) As Boolean
        Dim holidays As New List(Of Date)
        '2018
        holidays.Add("27/8/2018")
        holidays.Add("25/12/2018")
        holidays.Add("26/12/2018")

        '2019
        holidays.Add("01/01/2019")
        holidays.Add("19/04/2019")
        holidays.Add("22/04/2019")
        holidays.Add("06/05/2019")
        holidays.Add("27/05/2019")
        holidays.Add("26/08/2019")
        holidays.Add("25/12/2019")
        holidays.Add("26/12/2019")

        '2020
        holidays.Add("01/01/2020")
        holidays.Add("10/04/2020")
        holidays.Add("13/04/2020")
        holidays.Add("04/05/2020")
        holidays.Add("25/05/2020")
        holidays.Add("31/08/2020")
        holidays.Add("25/12/2020")
        holidays.Add("26/12/2020")
        holidays.Add("28/12/2020")

        '2021
        holidays.Add("01/01/2021")
        holidays.Add("02/04/2021")
        holidays.Add("05/04/2021")
        holidays.Add("03/05/2021")
        holidays.Add("31/05/2021")
        holidays.Add("30/08/2021")
        holidays.Add("25/12/2021")
        holidays.Add("26/12/2021")
        holidays.Add("27/12/2021")
        holidays.Add("28/12/2021")


        For i As Integer = 0 To holidays.Count() - 1
            If thisDate = holidays(i) Then Return True
        Next

        Return False
    End Function


    Public Function getLastWorkingDay(thisYear As Integer, thisMonth As Integer) As Date
        '' get d => number of days in the month
        Dim d As Integer = Date.DaysInMonth(thisYear, thisMonth)


        '' loop down from d and get the max day that is not a holiday or weekend day
        Dim thisDate As Date
        For i As Integer = d To 1 Step -1
            thisDate = New DateTime(thisYear, thisMonth, i)

            If Not (thisDate.ToString("dddd") = "Saturday" Or thisDate.ToString("dddd") = "Sunday" Or checkHoliday(thisDate)) Then Exit For
        Next

        Return thisDate
    End Function


    Public Function isLastWorkingDay(thisDate As Date) As Boolean
        If thisDate = getLastWorkingDay(thisDate.Year, thisDate.Month) Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function getLastWorkingFriday(thisYear As Integer, thisMonth As Integer) As Date
        '' get d => number of days in the month
        Dim d As Integer = Date.DaysInMonth(thisYear, thisMonth)


        '' loop down from d and get the max day that is not a holiday or weekend day
        Dim thisDate As Date
        For i As Integer = d To 1 Step -1
            thisDate = New DateTime(thisYear, thisMonth, i)

            If thisDate.ToString("dddd") = "Friday" And Not (checkHoliday(thisDate)) Then Exit For
        Next

        Return thisDate
    End Function


    Public Function isLastWorkingFriday(thisDate As Date) As Boolean
        If thisDate = getLastWorkingFriday(thisDate.Year, thisDate.Month) Then
            Return True
        Else
            Return False
        End If
    End Function
End Module






'Function GetArrayFromXMLwithHeaders(ByVal xmlString As String) As String(,)
'    Dim thisArray As String(,)
'    Dim rows As Integer = getRowNumbers(xmlString)
'    Dim workingText As String
'    Dim cols As Integer = 0
'    Dim colName As String = ""

'    If rows > 0 Then
'        xmlString = Right(xmlString, Len(xmlString) - Len(Left(xmlString, xmlString.IndexOf("<row "))))
'        workingText = Left(xmlString, xmlString.IndexOf("/>") + 2).Replace("<row ", "")
'        cols = Text.RegularExpressions.Regex.Split(workingText, "=""").Length - 1
'        ReDim thisArray(rows, cols)

'        For i As Integer = 0 To cols - 1
'            colName = Left(workingText, workingText.IndexOf("="""))
'            thisArray(0, i) = colName
'            colName = colName & "="""
'            workingText = workingText.Replace(colName, "")
'            colName = Left(workingText, workingText.IndexOf("""") + 2)
'            workingText = Right(workingText, Len(workingText) - Len(colName))
'        Next i


'        For i As Integer = 1 To rows
'            workingText = Left(xmlString, xmlString.IndexOf("/>") + 2)
'            xmlString = xmlString.Replace(workingText, "")


'            For j As Integer = 0 To cols - 1
'                thisArray(i, j) = GetValueFromXML(workingText, thisArray(0, j))
'            Next j
'        Next i
'    Else
'        ReDim thisArray(0, 0)
'    End If

'    Return thisArray
'End Function


'Function GetArrayFromXMLnoHeaders(ByVal xmlString As String) As String(,)
'    Dim thisArray As String(,)
'    Dim rows As Integer = getRowNumbers(xmlString)
'    Dim workingText As String
'    Dim cols As Integer = 0
'    Dim colName As String = ""

'    If rows > 0 Then
'        xmlString = Right(xmlString, Len(xmlString) - Len(Left(xmlString, xmlString.IndexOf("<row "))))
'        workingText = Left(xmlString, xmlString.IndexOf("/>") + 2).Replace("<row ", "")
'        cols = System.Text.RegularExpressions.Regex.Split(workingText, "=""").Length - 1
'        ReDim thisArray(rows - 1, cols)


'        For i As Integer = 0 To rows - 1
'            workingText = Left(xmlString, xmlString.IndexOf("/>") + 2)
'            xmlString = xmlString.Replace(workingText, "")


'            For j As Integer = 0 To cols - 1
'                thisArray(i, j) = GetValueFromXML(workingText, thisArray(0, j))
'            Next j
'        Next i
'    Else
'        ReDim thisArray(0, 0)
'    End If

'    Return thisArray
'End Function


'Function GetBatchID(ByVal listID As String, ByVal dataTable As DataTable, ByVal criteria As String) As String
'    Dim batchID As String = ""
'    Dim i As Integer = 1
'    Dim listFilter As String = "ListID = '" & listID & "'"
'    Dim view As DataView

'    view = New DataView(dataTable, listFilter, "ListID", DataViewRowState.CurrentRows)
'    view.RowFilter = BuildCriteria(criteria, listID)
'    dataTable = view.ToTable()
'    view = Nothing

'    Dim rows() As DataRow = dataTable.Select("ListID = '" & listID & "'")
'    If rows.Count > 0 Then
'        batchID = rows(0).Item("ChangeBatchID")
'    End If

'    Return batchID
'End Function


'Function FilterDataTable(ByVal startTable As DataTable, ByVal listID As String, ByVal criteria As String, ByVal remove() As String) As DataTable
'    Dim listFilter As String = "ListID = '" & listID & "'"
'    Dim endTable As DataTable
'    Dim view As DataView

'    view = New DataView(startTable, listFilter, "ListID", DataViewRowState.CurrentRows)
'    view.RowFilter = BuildCriteria(criteria, listID)

'    endTable = view.ToTable()
'    view = Nothing

'    With endTable.Columns
'        .Remove("ListID")
'        .Remove("ChangeBatchID")
'        If criteria <> "UpdateState" And criteria <> "UpdateSchedule" Then
'            .Remove("ChangeID")
'        End If

'        For i As Integer = 0 To remove.Length - 1
'            If remove(i).ToString <> "Alter" Then
'                .Remove(remove(i))
'            End If
'        Next i
'    End With


'    Dim priorityCol As New DataColumn("_SchedulePriority", GetType(String))

'    If criteria <> "UpdateState" And criteria <> "UpdateSchedule" Then
'        Dim newCol As New DataColumn("_State", GetType(String))
'        Dim scheduledDateCol As New DataColumn("_Schedule", GetType(String))


'        If criteria = "Suppress" Or criteria = "Deactivate" Then
'            newCol.DefaultValue = "4"
'        Else
'            newCol.DefaultValue = "0"
'            If Date.Now.Hour < 9 Then
'                Dim dt As Date = Date.Today

'                scheduledDateCol.DefaultValue = dt.ToString("dd/MM/yyyy") & " 07:00"

'                endTable.Columns.Add(scheduledDateCol)

'                scheduledDateCol = Nothing
'            End If
'        End If

'        endTable.Columns.Add(newCol)

'        newCol = Nothing
'    ElseIf criteria = "UpdateSchedule" Then
'        Dim scheduledCol As New DataColumn("_Scheduled", GetType(String))
'        Dim scheduledUserCol As New DataColumn("_ScheduleUserId", GetType(String))
'        Dim scheduleCol As New DataColumn("_Schedule", GetType(String))

'        scheduledCol.DefaultValue = "false"

'        scheduleCol.DefaultValue = "1901-01-01 00:00:00.000"

'        scheduledUserCol.DefaultValue = "0"

'        priorityCol.DefaultValue = "17"


'        endTable.Columns.Add(priorityCol)

'        endTable.Columns.Add(scheduledCol)

'        endTable.Columns.Add(scheduleCol)

'        endTable.Columns.Add(scheduledUserCol)


'        scheduledCol = Nothing

'        scheduledUserCol = Nothing
'    ElseIf criteria = "NewUpload" Then
'        endTable.Columns.Remove("RowID")

'        priorityCol.DefaultValue = "1"

'        endTable.Columns.Add(priorityCol)
'    End If

'    priorityCol = Nothing

'    Return endTable
'End Function


'Function BuildCriteria(ByVal criteria As String, ByVal listID As String) As String
'    Select Case criteria.ToString
'        Case "NewUpload"
'            Return "NewUpload = '1' AND Suppress = '0' AND Deactivate = '0' AND Reactivate = '0' AND ListID = '" & listID.ToString & "'"
'        Case "Suppress"
'            Return "Suppress = '1' AND ListID = '" & listID.ToString & "'"
'        Case "Deactivate"
'            Return "Deactivate = '1' AND ListID = '" & listID.ToString & "'"
'        Case "Reactivate"
'            Return "NewUpload = '0' AND Suppress = '0' AND Deactivate = '0' AND Reactivate = '1' AND ListID = '" & listID.ToString & "'"
'        Case "Alter"
'            Return "NewUpload = '0' AND Suppress = '0' AND Deactivate = '0' AND Reactivate = '0' AND ListID = '" & listID.ToString & "'"
'        Case Else
'            Return "ListID = '" & listID.ToString & "'"
'    End Select
'End Function
