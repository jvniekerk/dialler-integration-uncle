Module modDeleteOldAccounts
    Sub deleteOldCurrentAccounts()
        Dim process_type As String = "data_cleanup"
        Dim process As String = "Delete old accounts procedure"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)


        If ids.Count > 0 Then
            Dim lists As DataTable = getLists()

            '' Updates the lists if nothing on db
            If lists.Rows.Count() = 0 Then
                Try
                    getUltraDownloadData("lists")

                    lists = getLists()
                Catch ex As Exception
                    writeErrorToDB(ex, "Getting lists failed => " & ex.Message, ids.Item("batch_id"))
                End Try
            End If


            If lists.Rows.Count() > 0 Then
                For Each row As DataRow In lists.Rows
                    Dim list_id As Integer
                    Dim schema As String = ""

                    '' Gets the column ids that contain the list_id and schema
                    For Each col In lists.Columns
                        If col.ColumnName = "list_id" Then
                            list_id = row(col).ToString
                        ElseIf col.ColumnName = "schema_code" Then
                            schema = row(col).ToString
                        End If
                    Next


                    If list_id > 0 And Len(schema) > 0 Then
                        Dim sql As String
                        Dim interval As String = "week"     '' Intevals to go back to delete old accounts
                        Dim intervals As Integer = 1        '' Number of intervals to go back
                        Dim accounts As Integer = 950       '' Number of rows to process at a time
                        Dim table As New DataTable
                        Dim rows As Integer


                        '' get list of old accounts from current_accounts
                        sql = "
                            select distinct ca.row_id from " & mySqlGetConnDetails.Item("database") & ".current_accounts ca
                            where ca.lms_status =4 and ca.updated_at < DATE_ADD(CURRENT_DATE, interval -" & intervals.ToString & " " & interval & ")
                                and ca.list_id = " & list_id.ToString & " 
                            limit " & accounts.ToString & ";"


                        ''''' test sql ''''
                        ''''' test sql ''''
                        'interval = "minute"
                        'accounts = 3
                        'intervals = 3


                        'sql = "
                        'select distinct ca.row_id from " & mySqlGetConnDetails.Item("database") & ".current_accounts ca
                        'where ca.lms_status =4 and ca.updated_at < DATE_ADD(now(), interval -" & intervals.ToString & " " & interval & ")
                        '    and ca.list_id = " & list_id.ToString & " 
                        'limit " & accounts.ToString & ";"
                        ''MsgBox(sql)


                        ''''' test sql ''''
                        ''''' test sql ''''

                        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\delete-old-accounts-mysql.sql", sql)
                        'MsgBox(sql)


                        Try
                            table = mySqlGetDataTable(sql)

                            rows = table.Rows.Count()
                        Catch ex As Exception
                            writeErrorToDB(ex, "Error getting old current_accounts to delete from dialler db => " & ex.Message, ids.Item("batch_id"), "sql => " & sql)
                        End Try



                        '' loop through list and delete from ultra
                        If rows Then
                            Dim UltraLoginDetails As New Dictionary(Of String, String)

                            Try
                                UltraLoginDetails = getUltraLoginDetails("update")
                            Catch ex As Exception
                                writeErrorToDB(ex, "Building Ultra login details failed => " & ex.Message, ids.Item("batch_id"))
                            End Try


                            If UltraLoginDetails.Count() Then
                                Dim request As String = ""
                                Dim response As String = ""


                                For i As Integer = 0 To rows - 1
                                    request &= Environment.NewLine & "  <deleteOnly RowId='" & table.Rows(i).Item(0).ToString & "' />"
                                Next


                                If Len(request) Then
                                    Try
                                        response = getUltraUpdateStateResponse(request, schema, list_id, UltraLoginDetails)
                                    Catch ex As Exception
                                        writeErrorToDB(ex, "Delete old Ultra accounts failed => " & ex.Message, ids.Item("batch_id"), request)
                                    End Try


                                    If Len(response) Then
                                        Try
                                            sql = "delete from " & mySqlGetConnDetails.Item("database") & ".current_accounts where list_id = " & list_id.ToString & " and row_id in ("

                                            For i As Integer = 0 To rows - 1
                                                sql &= table.Rows(i).Item(0).ToString

                                                If i < rows - 1 Then
                                                    sql &= ", "
                                                Else
                                                    sql &= ");"
                                                End If
                                            Next


                                            Try
                                                Call mySqlPutNoQuery(sql)
                                            Catch ex As Exception
                                                writeErrorToDB(ex, "Deleting old current_accounts row failed => " & ex.Message, sql, response)
                                            End Try
                                        Catch ex As Exception
                                            Dim text As String = "sql => " & sql & Environment.NewLine & " => request => " & request
                                            writeErrorToDB(ex, "Deleting old Ultra account failed => " & ex.Message, ids.Item("batch_id"), text, response)
                                        End Try


                                        Try
                                            '' store the update data on the dialler database
                                            storeUpdateData(list_id, process, request, response, rows,, process_type)
                                        Catch ex As Exception
                                            writeErrorToDB(ex, "Storing delete old accounts failed!!" & ex.Message, ids.Item("batch_id"), request, response)
                                        End Try
                                    Else
                                        Try
                                            Throw New Exception("Failed to get Ultra response when deleting old accounts!!")
                                        Catch ex As Exception
                                            writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), request)
                                        End Try
                                    End If
                                Else
                                    Try
                                        Throw New Exception("Delete old accounts build request failed!!")
                                    Catch ex As Exception
                                        writeErrorToDB(ex, "Getting lists failed => " & ex.Message, ids.Item("batch_id"))
                                    End Try
                                End If
                            End If
                        End If
                    End If
                Next
            Else
                Try
                    Throw New Exception("Getting list of lists failed!!")
                Catch ex As Exception
                    writeErrorToDB(ex, "Getting lists failed => " & ex.Message, ids.Item("batch_id"))
                End Try
            End If

            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    '' Downlaods the data Ultra currently hold on list rows
    Function getUltraAccountDetails(schema As String, list_id As Integer, row_id As Integer) As String
        Dim UltraLoginDetails As New Dictionary(Of String, String)

        Try
            UltraLoginDetails = getUltraLoginDetails("update")
        Catch ex As Exception
            writeErrorToDB(ex, "Building Ultra login details failed => " & ex.Message, 0)
        End Try


        If UltraLoginDetails.Count() Then
            Dim response As String = ""
            Dim request As String = "<data clientid=""" & modKeys.ultra_client_id & """><" & schema & " BaseIndexId=""" & list_id.ToString & """ RowId=""" & row_id.ToString & """/></data>"


            Try
                response = GetUltraDownload(request, UltraLoginDetails.Item("user"), UltraLoginDetails.Item("pw"))
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, 0, request, response)
            End Try


            Return response
        Else
            Try
                Throw New Exception("Setting Ultra login details to get account details failed!!")
            Catch ex As Exception
                Dim text As String = "schema=" & schema & "; list_id=" & list_id.ToString & "; row_id=" & row_id.ToString

                writeErrorToDB(ex, ex.Message, 0, text)
            End Try

            Return "Couldn't get account details => Check error_log"
        End If
    End Function
End Module
