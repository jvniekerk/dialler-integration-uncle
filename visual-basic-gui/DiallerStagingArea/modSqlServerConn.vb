Option Explicit On
Imports System.Data.SqlClient

Module modSqlServerConn
    'Function BuildXML(ByVal schema As String, ByVal listID As String, ByVal thisTable As DataTable, ByVal criteria As String) As String
    '    Dim xml As String = "<upload clientid=""" & ultra_client_id & """><" & schema & " BaseIndexId=""" & listID & """><rows>"

    '    xml &= XMLfromDataTable(thisTable, criteria)

    '    xml &= "</rows></" & schema & "></upload>"

    '    Return xml
    'End Function


    '' Returns a value or XML string from SQLSVR
    Public Function ConnectToSql(ByVal sqlQuery As String, Optional ByVal download As Boolean = True, Optional ByVal table As Boolean = False) As String
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader
        Dim results As String = ""


        myConn = New SqlConnection(GetConnectionString())

        myCmd = myConn.CreateCommand

        myCmd.CommandTimeout = 0

        myCmd.CommandText = sqlQuery

        myConn.Open()

        myReader = myCmd.ExecuteReader()

        If download Then
            results = XMLString(myReader)
        Else
            results = ReturnValue(myReader)
        End If

        myReader.Close()

        myReader = Nothing

        myConn.Close()

        myConn = Nothing

        Return results
    End Function


    Function sqlToDataTable(ByVal sqlQuery As String) As DataTable
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader

        myConn = New SqlConnection(GetConnectionString())

        myCmd = myConn.CreateCommand

        myCmd.CommandTimeout = 0

        myCmd.CommandText = sqlQuery

        myConn.Open()

        myReader = myCmd.ExecuteReader()

        Dim results As New DataTable

        results.Load(myReader)

        myReader.Close()

        myReader = Nothing

        myConn.Close()

        myConn = Nothing

        Return results
    End Function
End Module
