﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.bgwUpdateDialler = New System.ComponentModel.BackgroundWorker()
        Me.cmbTest = New System.Windows.Forms.Button()
        Me.btnRebuildTables = New System.Windows.Forms.Button()
        Me.chkMonday = New System.Windows.Forms.CheckBox()
        Me.chkTuesday = New System.Windows.Forms.CheckBox()
        Me.chkWednesday = New System.Windows.Forms.CheckBox()
        Me.chkThursday = New System.Windows.Forms.CheckBox()
        Me.chkFriday = New System.Windows.Forms.CheckBox()
        Me.chkSaturday = New System.Windows.Forms.CheckBox()
        Me.chkSunday = New System.Windows.Forms.CheckBox()
        Me.lblDay = New System.Windows.Forms.Label()
        Me.lblStartTime = New System.Windows.Forms.Label()
        Me.txtMonStart = New System.Windows.Forms.TextBox()
        Me.txtTueStart = New System.Windows.Forms.TextBox()
        Me.txtWedStart = New System.Windows.Forms.TextBox()
        Me.txtThuStart = New System.Windows.Forms.TextBox()
        Me.txtFriStart = New System.Windows.Forms.TextBox()
        Me.txtSatStart = New System.Windows.Forms.TextBox()
        Me.txtSunStart = New System.Windows.Forms.TextBox()
        Me.txtSunEnd = New System.Windows.Forms.TextBox()
        Me.txtSatEnd = New System.Windows.Forms.TextBox()
        Me.txtFriEnd = New System.Windows.Forms.TextBox()
        Me.txtThuEnd = New System.Windows.Forms.TextBox()
        Me.txtWedEnd = New System.Windows.Forms.TextBox()
        Me.txtTueEnd = New System.Windows.Forms.TextBox()
        Me.txtMonEnd = New System.Windows.Forms.TextBox()
        Me.lblEndTime = New System.Windows.Forms.Label()
        Me.pnlDiallerTimes = New System.Windows.Forms.Panel()
        Me.btnAddUser = New System.Windows.Forms.Button()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.tmrElapsedTime = New System.Windows.Forms.Timer(Me.components)
        Me.lblCurrDate = New System.Windows.Forms.Label()
        Me.tmrCurrDate = New System.Windows.Forms.Timer(Me.components)
        Me.bgwUnderwriting = New System.ComponentModel.BackgroundWorker()
        Me.bgwCallAttempts = New System.ComponentModel.BackgroundWorker()
        Me.bgwCollections = New System.ComponentModel.BackgroundWorker()
        Me.btnLists = New System.Windows.Forms.Button()
        Me.bgwAccountStateCheck = New System.ComponentModel.BackgroundWorker()
        Me.txtDownloadStart = New System.Windows.Forms.TextBox()
        Me.txtDownloadEnd = New System.Windows.Forms.TextBox()
        Me.gpbDownloadTimes = New System.Windows.Forms.GroupBox()
        Me.lblDownloadEnd = New System.Windows.Forms.Label()
        Me.lblDownloadStart = New System.Windows.Forms.Label()
        Me.gpbUpdateProgress = New System.Windows.Forms.GroupBox()
        Me.lsbUpdateProgress = New System.Windows.Forms.ListBox()
        Me.gpbDownloadProgress = New System.Windows.Forms.GroupBox()
        Me.lsbDownloadProgress = New System.Windows.Forms.ListBox()
        Me.btnUpdateAgents = New System.Windows.Forms.Button()
        Me.btnUpdateLists = New System.Windows.Forms.Button()
        Me.btnUpdateUserStates = New System.Windows.Forms.Button()
        Me.btnStopping = New System.Windows.Forms.Button()
        Me.pcbUB_Logo = New System.Windows.Forms.PictureBox()
        Me.pnlDiallerTimes.SuspendLayout()
        Me.gpbDownloadTimes.SuspendLayout()
        Me.gpbUpdateProgress.SuspendLayout()
        Me.gpbDownloadProgress.SuspendLayout()
        CType(Me.pcbUB_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnRun
        '
        Me.btnRun.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnRun.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnRun.FlatAppearance.BorderSize = 5
        Me.btnRun.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnRun.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnRun.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRun.Location = New System.Drawing.Point(24, 12)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(137, 61)
        Me.btnRun.TabIndex = 0
        Me.btnRun.Text = "Run Dialler Update"
        Me.btnRun.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnExit.FlatAppearance.BorderSize = 5
        Me.btnExit.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnExit.Location = New System.Drawing.Point(346, 79)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(136, 61)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Exit Application"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'btnStop
        '
        Me.btnStop.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnStop.Enabled = False
        Me.btnStop.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnStop.FlatAppearance.BorderSize = 5
        Me.btnStop.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnStop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnStop.Location = New System.Drawing.Point(24, 79)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(137, 61)
        Me.btnStop.TabIndex = 2
        Me.btnStop.Text = "Stop Dialler Update"
        Me.btnStop.UseVisualStyleBackColor = False
        '
        'bgwUpdateDialler
        '
        Me.bgwUpdateDialler.WorkerReportsProgress = True
        Me.bgwUpdateDialler.WorkerSupportsCancellation = True
        '
        'cmbTest
        '
        Me.cmbTest.BackColor = System.Drawing.Color.Gray
        Me.cmbTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmbTest.Location = New System.Drawing.Point(346, 12)
        Me.cmbTest.Name = "cmbTest"
        Me.cmbTest.Size = New System.Drawing.Size(137, 61)
        Me.cmbTest.TabIndex = 3
        Me.cmbTest.Text = "Run Test"
        Me.cmbTest.UseVisualStyleBackColor = False
        '
        'btnRebuildTables
        '
        Me.btnRebuildTables.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnRebuildTables.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnRebuildTables.FlatAppearance.BorderSize = 5
        Me.btnRebuildTables.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnRebuildTables.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnRebuildTables.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnRebuildTables.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRebuildTables.Location = New System.Drawing.Point(587, 282)
        Me.btnRebuildTables.Name = "btnRebuildTables"
        Me.btnRebuildTables.Size = New System.Drawing.Size(124, 58)
        Me.btnRebuildTables.TabIndex = 7
        Me.btnRebuildTables.Text = "Rebuild DB Tables"
        Me.btnRebuildTables.UseVisualStyleBackColor = False
        Me.btnRebuildTables.Visible = False
        '
        'chkMonday
        '
        Me.chkMonday.AutoSize = True
        Me.chkMonday.Checked = True
        Me.chkMonday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMonday.Location = New System.Drawing.Point(10, 31)
        Me.chkMonday.Name = "chkMonday"
        Me.chkMonday.Size = New System.Drawing.Size(84, 24)
        Me.chkMonday.TabIndex = 12
        Me.chkMonday.Text = "Monday"
        Me.chkMonday.UseVisualStyleBackColor = True
        '
        'chkTuesday
        '
        Me.chkTuesday.AutoSize = True
        Me.chkTuesday.Checked = True
        Me.chkTuesday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTuesday.Location = New System.Drawing.Point(10, 63)
        Me.chkTuesday.Name = "chkTuesday"
        Me.chkTuesday.Size = New System.Drawing.Size(88, 24)
        Me.chkTuesday.TabIndex = 13
        Me.chkTuesday.Text = "Tuesday"
        Me.chkTuesday.UseVisualStyleBackColor = True
        '
        'chkWednesday
        '
        Me.chkWednesday.AutoSize = True
        Me.chkWednesday.Checked = True
        Me.chkWednesday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWednesday.Location = New System.Drawing.Point(10, 95)
        Me.chkWednesday.Name = "chkWednesday"
        Me.chkWednesday.Size = New System.Drawing.Size(112, 24)
        Me.chkWednesday.TabIndex = 14
        Me.chkWednesday.Text = "Wednesday"
        Me.chkWednesday.UseVisualStyleBackColor = True
        '
        'chkThursday
        '
        Me.chkThursday.AutoSize = True
        Me.chkThursday.Checked = True
        Me.chkThursday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkThursday.Location = New System.Drawing.Point(10, 127)
        Me.chkThursday.Name = "chkThursday"
        Me.chkThursday.Size = New System.Drawing.Size(93, 24)
        Me.chkThursday.TabIndex = 15
        Me.chkThursday.Text = "Thursday"
        Me.chkThursday.UseVisualStyleBackColor = True
        '
        'chkFriday
        '
        Me.chkFriday.AutoSize = True
        Me.chkFriday.Checked = True
        Me.chkFriday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFriday.Location = New System.Drawing.Point(10, 159)
        Me.chkFriday.Name = "chkFriday"
        Me.chkFriday.Size = New System.Drawing.Size(71, 24)
        Me.chkFriday.TabIndex = 16
        Me.chkFriday.Text = "Friday"
        Me.chkFriday.UseVisualStyleBackColor = True
        '
        'chkSaturday
        '
        Me.chkSaturday.AutoSize = True
        Me.chkSaturday.Checked = True
        Me.chkSaturday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSaturday.Location = New System.Drawing.Point(10, 191)
        Me.chkSaturday.Name = "chkSaturday"
        Me.chkSaturday.Size = New System.Drawing.Size(92, 24)
        Me.chkSaturday.TabIndex = 17
        Me.chkSaturday.Text = "Saturday"
        Me.chkSaturday.UseVisualStyleBackColor = True
        '
        'chkSunday
        '
        Me.chkSunday.AutoSize = True
        Me.chkSunday.Checked = True
        Me.chkSunday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSunday.Location = New System.Drawing.Point(10, 223)
        Me.chkSunday.Name = "chkSunday"
        Me.chkSunday.Size = New System.Drawing.Size(82, 24)
        Me.chkSunday.TabIndex = 18
        Me.chkSunday.Text = "Sunday"
        Me.chkSunday.UseVisualStyleBackColor = True
        '
        'lblDay
        '
        Me.lblDay.AutoSize = True
        Me.lblDay.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDay.Location = New System.Drawing.Point(34, 0)
        Me.lblDay.Name = "lblDay"
        Me.lblDay.Size = New System.Drawing.Size(54, 26)
        Me.lblDay.TabIndex = 19
        Me.lblDay.Text = "Day"
        '
        'lblStartTime
        '
        Me.lblStartTime.AutoSize = True
        Me.lblStartTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartTime.Location = New System.Drawing.Point(154, 0)
        Me.lblStartTime.Name = "lblStartTime"
        Me.lblStartTime.Size = New System.Drawing.Size(122, 26)
        Me.lblStartTime.TabIndex = 20
        Me.lblStartTime.Text = "Start Time"
        '
        'txtMonStart
        '
        Me.txtMonStart.Location = New System.Drawing.Point(129, 29)
        Me.txtMonStart.Name = "txtMonStart"
        Me.txtMonStart.Size = New System.Drawing.Size(159, 26)
        Me.txtMonStart.TabIndex = 21
        Me.txtMonStart.Text = "8:00"
        Me.txtMonStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTueStart
        '
        Me.txtTueStart.Location = New System.Drawing.Point(128, 61)
        Me.txtTueStart.Name = "txtTueStart"
        Me.txtTueStart.Size = New System.Drawing.Size(159, 26)
        Me.txtTueStart.TabIndex = 22
        Me.txtTueStart.Text = "8:00"
        Me.txtTueStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtWedStart
        '
        Me.txtWedStart.Location = New System.Drawing.Point(128, 93)
        Me.txtWedStart.Name = "txtWedStart"
        Me.txtWedStart.Size = New System.Drawing.Size(159, 26)
        Me.txtWedStart.TabIndex = 23
        Me.txtWedStart.Text = "8:00"
        Me.txtWedStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtThuStart
        '
        Me.txtThuStart.Location = New System.Drawing.Point(128, 125)
        Me.txtThuStart.Name = "txtThuStart"
        Me.txtThuStart.Size = New System.Drawing.Size(159, 26)
        Me.txtThuStart.TabIndex = 24
        Me.txtThuStart.Text = "8:00"
        Me.txtThuStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFriStart
        '
        Me.txtFriStart.Location = New System.Drawing.Point(128, 157)
        Me.txtFriStart.Name = "txtFriStart"
        Me.txtFriStart.Size = New System.Drawing.Size(159, 26)
        Me.txtFriStart.TabIndex = 25
        Me.txtFriStart.Text = "8:00"
        Me.txtFriStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSatStart
        '
        Me.txtSatStart.Location = New System.Drawing.Point(128, 189)
        Me.txtSatStart.Name = "txtSatStart"
        Me.txtSatStart.Size = New System.Drawing.Size(159, 26)
        Me.txtSatStart.TabIndex = 26
        Me.txtSatStart.Text = "9:00"
        Me.txtSatStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSunStart
        '
        Me.txtSunStart.Location = New System.Drawing.Point(128, 221)
        Me.txtSunStart.Name = "txtSunStart"
        Me.txtSunStart.Size = New System.Drawing.Size(159, 26)
        Me.txtSunStart.TabIndex = 27
        Me.txtSunStart.Text = "9:00"
        Me.txtSunStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSunEnd
        '
        Me.txtSunEnd.Location = New System.Drawing.Point(309, 221)
        Me.txtSunEnd.Name = "txtSunEnd"
        Me.txtSunEnd.Size = New System.Drawing.Size(159, 26)
        Me.txtSunEnd.TabIndex = 35
        Me.txtSunEnd.Text = "18:00"
        Me.txtSunEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSatEnd
        '
        Me.txtSatEnd.Location = New System.Drawing.Point(309, 189)
        Me.txtSatEnd.Name = "txtSatEnd"
        Me.txtSatEnd.Size = New System.Drawing.Size(159, 26)
        Me.txtSatEnd.TabIndex = 34
        Me.txtSatEnd.Text = "18:00"
        Me.txtSatEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFriEnd
        '
        Me.txtFriEnd.Location = New System.Drawing.Point(309, 157)
        Me.txtFriEnd.Name = "txtFriEnd"
        Me.txtFriEnd.Size = New System.Drawing.Size(159, 26)
        Me.txtFriEnd.TabIndex = 33
        Me.txtFriEnd.Text = "21:30"
        Me.txtFriEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtThuEnd
        '
        Me.txtThuEnd.Location = New System.Drawing.Point(309, 125)
        Me.txtThuEnd.Name = "txtThuEnd"
        Me.txtThuEnd.Size = New System.Drawing.Size(159, 26)
        Me.txtThuEnd.TabIndex = 32
        Me.txtThuEnd.Text = "21:30"
        Me.txtThuEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtWedEnd
        '
        Me.txtWedEnd.Location = New System.Drawing.Point(309, 93)
        Me.txtWedEnd.Name = "txtWedEnd"
        Me.txtWedEnd.Size = New System.Drawing.Size(159, 26)
        Me.txtWedEnd.TabIndex = 31
        Me.txtWedEnd.Text = "21:30"
        Me.txtWedEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTueEnd
        '
        Me.txtTueEnd.Location = New System.Drawing.Point(309, 61)
        Me.txtTueEnd.Name = "txtTueEnd"
        Me.txtTueEnd.Size = New System.Drawing.Size(159, 26)
        Me.txtTueEnd.TabIndex = 30
        Me.txtTueEnd.Text = "21:30"
        Me.txtTueEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtMonEnd
        '
        Me.txtMonEnd.Location = New System.Drawing.Point(310, 29)
        Me.txtMonEnd.Name = "txtMonEnd"
        Me.txtMonEnd.Size = New System.Drawing.Size(159, 26)
        Me.txtMonEnd.TabIndex = 29
        Me.txtMonEnd.Text = "21:30"
        Me.txtMonEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblEndTime
        '
        Me.lblEndTime.AutoSize = True
        Me.lblEndTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndTime.Location = New System.Drawing.Point(335, 0)
        Me.lblEndTime.Name = "lblEndTime"
        Me.lblEndTime.Size = New System.Drawing.Size(113, 26)
        Me.lblEndTime.TabIndex = 28
        Me.lblEndTime.Text = "End Time"
        '
        'pnlDiallerTimes
        '
        Me.pnlDiallerTimes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlDiallerTimes.Controls.Add(Me.txtFriEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.txtSunEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkMonday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtSatEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkTuesday)
        Me.pnlDiallerTimes.Controls.Add(Me.chkWednesday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtThuEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkThursday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtWedEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkFriday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtTueEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkSaturday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtMonEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkSunday)
        Me.pnlDiallerTimes.Controls.Add(Me.lblEndTime)
        Me.pnlDiallerTimes.Controls.Add(Me.lblDay)
        Me.pnlDiallerTimes.Controls.Add(Me.txtSunStart)
        Me.pnlDiallerTimes.Controls.Add(Me.lblStartTime)
        Me.pnlDiallerTimes.Controls.Add(Me.txtSatStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtMonStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtFriStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtTueStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtThuStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtWedStart)
        Me.pnlDiallerTimes.Location = New System.Drawing.Point(12, 296)
        Me.pnlDiallerTimes.Name = "pnlDiallerTimes"
        Me.pnlDiallerTimes.Size = New System.Drawing.Size(498, 266)
        Me.pnlDiallerTimes.TabIndex = 36
        '
        'btnAddUser
        '
        Me.btnAddUser.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnAddUser.Enabled = False
        Me.btnAddUser.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnAddUser.FlatAppearance.BorderSize = 5
        Me.btnAddUser.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnAddUser.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnAddUser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnAddUser.Location = New System.Drawing.Point(587, 94)
        Me.btnAddUser.Name = "btnAddUser"
        Me.btnAddUser.Size = New System.Drawing.Size(124, 61)
        Me.btnAddUser.TabIndex = 37
        Me.btnAddUser.Text = "Manage Agents"
        Me.btnAddUser.UseVisualStyleBackColor = False
        Me.btnAddUser.Visible = False
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Location = New System.Drawing.Point(8, 231)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(359, 20)
        Me.lblProgress.TabIndex = 38
        Me.lblProgress.Text = "Dialler Update Time Lapsed: 0 minutes 0 seconds"
        '
        'tmrElapsedTime
        '
        Me.tmrElapsedTime.Interval = 1000
        '
        'lblCurrDate
        '
        Me.lblCurrDate.AutoSize = True
        Me.lblCurrDate.Location = New System.Drawing.Point(12, 264)
        Me.lblCurrDate.Name = "lblCurrDate"
        Me.lblCurrDate.Size = New System.Drawing.Size(44, 20)
        Me.lblCurrDate.TabIndex = 39
        Me.lblCurrDate.Text = "Date"
        '
        'tmrCurrDate
        '
        Me.tmrCurrDate.Enabled = True
        Me.tmrCurrDate.Interval = 1000
        '
        'bgwUnderwriting
        '
        Me.bgwUnderwriting.WorkerReportsProgress = True
        Me.bgwUnderwriting.WorkerSupportsCancellation = True
        '
        'bgwCallAttempts
        '
        Me.bgwCallAttempts.WorkerReportsProgress = True
        Me.bgwCallAttempts.WorkerSupportsCancellation = True
        '
        'bgwCollections
        '
        Me.bgwCollections.WorkerReportsProgress = True
        Me.bgwCollections.WorkerSupportsCancellation = True
        '
        'btnLists
        '
        Me.btnLists.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnLists.Enabled = False
        Me.btnLists.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnLists.FlatAppearance.BorderSize = 5
        Me.btnLists.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnLists.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnLists.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnLists.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnLists.Location = New System.Drawing.Point(587, 27)
        Me.btnLists.Name = "btnLists"
        Me.btnLists.Size = New System.Drawing.Size(124, 61)
        Me.btnLists.TabIndex = 40
        Me.btnLists.Text = "Rebuild Lists Table"
        Me.btnLists.UseVisualStyleBackColor = False
        Me.btnLists.Visible = False
        '
        'txtDownloadStart
        '
        Me.txtDownloadStart.Location = New System.Drawing.Point(71, 31)
        Me.txtDownloadStart.Name = "txtDownloadStart"
        Me.txtDownloadStart.Size = New System.Drawing.Size(78, 26)
        Me.txtDownloadStart.TabIndex = 22
        Me.txtDownloadStart.Text = "5:00"
        Me.txtDownloadStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtDownloadEnd
        '
        Me.txtDownloadEnd.Location = New System.Drawing.Point(221, 31)
        Me.txtDownloadEnd.Name = "txtDownloadEnd"
        Me.txtDownloadEnd.Size = New System.Drawing.Size(78, 26)
        Me.txtDownloadEnd.TabIndex = 23
        Me.txtDownloadEnd.Text = "22:30"
        Me.txtDownloadEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gpbDownloadTimes
        '
        Me.gpbDownloadTimes.Controls.Add(Me.lblDownloadEnd)
        Me.gpbDownloadTimes.Controls.Add(Me.lblDownloadStart)
        Me.gpbDownloadTimes.Controls.Add(Me.txtDownloadEnd)
        Me.gpbDownloadTimes.Controls.Add(Me.txtDownloadStart)
        Me.gpbDownloadTimes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbDownloadTimes.Location = New System.Drawing.Point(12, 146)
        Me.gpbDownloadTimes.Name = "gpbDownloadTimes"
        Me.gpbDownloadTimes.Size = New System.Drawing.Size(315, 71)
        Me.gpbDownloadTimes.TabIndex = 42
        Me.gpbDownloadTimes.TabStop = False
        Me.gpbDownloadTimes.Text = "Data Download Times"
        '
        'lblDownloadEnd
        '
        Me.lblDownloadEnd.AutoSize = True
        Me.lblDownloadEnd.Location = New System.Drawing.Point(155, 34)
        Me.lblDownloadEnd.Name = "lblDownloadEnd"
        Me.lblDownloadEnd.Size = New System.Drawing.Size(42, 20)
        Me.lblDownloadEnd.TabIndex = 44
        Me.lblDownloadEnd.Text = "End:"
        '
        'lblDownloadStart
        '
        Me.lblDownloadStart.AutoSize = True
        Me.lblDownloadStart.Location = New System.Drawing.Point(6, 35)
        Me.lblDownloadStart.Name = "lblDownloadStart"
        Me.lblDownloadStart.Size = New System.Drawing.Size(48, 20)
        Me.lblDownloadStart.TabIndex = 43
        Me.lblDownloadStart.Text = "Start:"
        '
        'gpbUpdateProgress
        '
        Me.gpbUpdateProgress.Controls.Add(Me.lsbUpdateProgress)
        Me.gpbUpdateProgress.Location = New System.Drawing.Point(587, 445)
        Me.gpbUpdateProgress.Name = "gpbUpdateProgress"
        Me.gpbUpdateProgress.Size = New System.Drawing.Size(266, 88)
        Me.gpbUpdateProgress.TabIndex = 43
        Me.gpbUpdateProgress.TabStop = False
        Me.gpbUpdateProgress.Text = "Dialler Update Progress"
        Me.gpbUpdateProgress.Visible = False
        '
        'lsbUpdateProgress
        '
        Me.lsbUpdateProgress.Enabled = False
        Me.lsbUpdateProgress.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsbUpdateProgress.FormattingEnabled = True
        Me.lsbUpdateProgress.ItemHeight = 16
        Me.lsbUpdateProgress.Location = New System.Drawing.Point(6, 25)
        Me.lsbUpdateProgress.Name = "lsbUpdateProgress"
        Me.lsbUpdateProgress.SelectionMode = System.Windows.Forms.SelectionMode.None
        Me.lsbUpdateProgress.Size = New System.Drawing.Size(256, 52)
        Me.lsbUpdateProgress.TabIndex = 0
        '
        'gpbDownloadProgress
        '
        Me.gpbDownloadProgress.Controls.Add(Me.lsbDownloadProgress)
        Me.gpbDownloadProgress.Location = New System.Drawing.Point(587, 351)
        Me.gpbDownloadProgress.Name = "gpbDownloadProgress"
        Me.gpbDownloadProgress.Size = New System.Drawing.Size(266, 88)
        Me.gpbDownloadProgress.TabIndex = 44
        Me.gpbDownloadProgress.TabStop = False
        Me.gpbDownloadProgress.Text = "Data Download Progress"
        Me.gpbDownloadProgress.Visible = False
        '
        'lsbDownloadProgress
        '
        Me.lsbDownloadProgress.Enabled = False
        Me.lsbDownloadProgress.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsbDownloadProgress.FormattingEnabled = True
        Me.lsbDownloadProgress.ItemHeight = 16
        Me.lsbDownloadProgress.Location = New System.Drawing.Point(6, 25)
        Me.lsbDownloadProgress.Name = "lsbDownloadProgress"
        Me.lsbDownloadProgress.SelectionMode = System.Windows.Forms.SelectionMode.None
        Me.lsbDownloadProgress.Size = New System.Drawing.Size(256, 52)
        Me.lsbDownloadProgress.TabIndex = 1
        '
        'btnUpdateAgents
        '
        Me.btnUpdateAgents.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnUpdateAgents.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnUpdateAgents.FlatAppearance.BorderSize = 5
        Me.btnUpdateAgents.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnUpdateAgents.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnUpdateAgents.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnUpdateAgents.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnUpdateAgents.Location = New System.Drawing.Point(190, 79)
        Me.btnUpdateAgents.Name = "btnUpdateAgents"
        Me.btnUpdateAgents.Size = New System.Drawing.Size(137, 61)
        Me.btnUpdateAgents.TabIndex = 45
        Me.btnUpdateAgents.Text = "Update Users"
        Me.btnUpdateAgents.UseVisualStyleBackColor = False
        '
        'btnUpdateLists
        '
        Me.btnUpdateLists.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnUpdateLists.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnUpdateLists.FlatAppearance.BorderSize = 5
        Me.btnUpdateLists.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnUpdateLists.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnUpdateLists.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnUpdateLists.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnUpdateLists.Location = New System.Drawing.Point(190, 12)
        Me.btnUpdateLists.Name = "btnUpdateLists"
        Me.btnUpdateLists.Size = New System.Drawing.Size(137, 61)
        Me.btnUpdateLists.TabIndex = 46
        Me.btnUpdateLists.Text = "Update Lists"
        Me.btnUpdateLists.UseVisualStyleBackColor = False
        '
        'btnUpdateUserStates
        '
        Me.btnUpdateUserStates.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnUpdateUserStates.Enabled = False
        Me.btnUpdateUserStates.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnUpdateUserStates.FlatAppearance.BorderSize = 5
        Me.btnUpdateUserStates.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnUpdateUserStates.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnUpdateUserStates.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnUpdateUserStates.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnUpdateUserStates.Location = New System.Drawing.Point(587, 169)
        Me.btnUpdateUserStates.Name = "btnUpdateUserStates"
        Me.btnUpdateUserStates.Size = New System.Drawing.Size(124, 61)
        Me.btnUpdateUserStates.TabIndex = 47
        Me.btnUpdateUserStates.Text = "Update User States"
        Me.btnUpdateUserStates.UseVisualStyleBackColor = False
        Me.btnUpdateUserStates.Visible = False
        '
        'btnStopping
        '
        Me.btnStopping.BackColor = System.Drawing.Color.Red
        Me.btnStopping.Enabled = False
        Me.btnStopping.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStopping.Location = New System.Drawing.Point(66, 27)
        Me.btnStopping.Name = "btnStopping"
        Me.btnStopping.Size = New System.Drawing.Size(396, 155)
        Me.btnStopping.TabIndex = 48
        Me.btnStopping.Text = "App Stopping... Please Wait..."
        Me.btnStopping.UseVisualStyleBackColor = False
        Me.btnStopping.Visible = False
        '
        'pcbUB_Logo
        '
        Me.pcbUB_Logo.BackgroundImage = Global.DiallerStagingArea.My.Resources.Resources.ub_logo
        Me.pcbUB_Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pcbUB_Logo.Location = New System.Drawing.Point(333, 156)
        Me.pcbUB_Logo.Name = "pcbUB_Logo"
        Me.pcbUB_Logo.Size = New System.Drawing.Size(177, 82)
        Me.pcbUB_Logo.TabIndex = 49
        Me.pcbUB_Logo.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(525, 590)
        Me.Controls.Add(Me.btnStopping)
        Me.Controls.Add(Me.btnUpdateUserStates)
        Me.Controls.Add(Me.btnUpdateLists)
        Me.Controls.Add(Me.btnUpdateAgents)
        Me.Controls.Add(Me.gpbDownloadProgress)
        Me.Controls.Add(Me.gpbUpdateProgress)
        Me.Controls.Add(Me.gpbDownloadTimes)
        Me.Controls.Add(Me.btnLists)
        Me.Controls.Add(Me.lblCurrDate)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.btnAddUser)
        Me.Controls.Add(Me.pnlDiallerTimes)
        Me.Controls.Add(Me.btnRebuildTables)
        Me.Controls.Add(Me.cmbTest)
        Me.Controls.Add(Me.btnStop)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.pcbUB_Logo)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Form1"
        Me.RightToLeftLayout = True
        Me.Text = "UNCLE Dialler Integration Ultracomms"
        Me.pnlDiallerTimes.ResumeLayout(False)
        Me.pnlDiallerTimes.PerformLayout()
        Me.gpbDownloadTimes.ResumeLayout(False)
        Me.gpbDownloadTimes.PerformLayout()
        Me.gpbUpdateProgress.ResumeLayout(False)
        Me.gpbDownloadProgress.ResumeLayout(False)
        CType(Me.pcbUB_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnRun As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents btnStop As Button
    Friend WithEvents bgwUpdateDialler As System.ComponentModel.BackgroundWorker
    Friend WithEvents Button1 As Button
    Friend WithEvents cmbTest As Button
    Friend WithEvents btnRebuildTables As Button
    Friend WithEvents chkMonday As CheckBox
    Friend WithEvents chkTuesday As CheckBox
    Friend WithEvents chkWednesday As CheckBox
    Friend WithEvents chkThursday As CheckBox
    Friend WithEvents chkFriday As CheckBox
    Friend WithEvents chkSaturday As CheckBox
    Friend WithEvents chkSunday As CheckBox
    Friend WithEvents lblDay As Label
    Friend WithEvents lblStartTime As Label
    Friend WithEvents txtMonStart As TextBox
    Friend WithEvents txtTueStart As TextBox
    Friend WithEvents txtWedStart As TextBox
    Friend WithEvents txtThuStart As TextBox
    Friend WithEvents txtFriStart As TextBox
    Friend WithEvents txtSatStart As TextBox
    Friend WithEvents txtSunStart As TextBox
    Friend WithEvents txtSunEnd As TextBox
    Friend WithEvents txtSatEnd As TextBox
    Friend WithEvents txtFriEnd As TextBox
    Friend WithEvents txtThuEnd As TextBox
    Friend WithEvents txtWedEnd As TextBox
    Friend WithEvents txtTueEnd As TextBox
    Friend WithEvents txtMonEnd As TextBox
    Friend WithEvents lblEndTime As Label
    Friend WithEvents pnlDiallerTimes As Panel
    Friend WithEvents btnAddUser As Button
    Friend WithEvents lblProgress As Label
    Friend WithEvents tmrElapsedTime As Timer
    Friend WithEvents lblCurrDate As Label
    Friend WithEvents tmrCurrDate As Timer
    Friend WithEvents bgwUnderwriting As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwCallAttempts As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwCollections As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnLists As Button
    Friend WithEvents bgwAccountStateCheck As System.ComponentModel.BackgroundWorker
    Friend WithEvents txtDownloadEnd As TextBox
    Friend WithEvents txtDownloadStart As TextBox
    Friend WithEvents gpbDownloadTimes As GroupBox
    Friend WithEvents lblDownloadEnd As Label
    Friend WithEvents lblDownloadStart As Label
    Friend WithEvents gpbUpdateProgress As GroupBox
    Friend WithEvents gpbDownloadProgress As GroupBox
    Public WithEvents lsbUpdateProgress As ListBox
    Public WithEvents lsbDownloadProgress As ListBox
    Friend WithEvents btnUpdateAgents As Button
    Friend WithEvents btnUpdateLists As Button
    Friend WithEvents btnUpdateUserStates As Button
    Friend WithEvents btnStopping As Button
    Friend WithEvents pcbUB_Logo As PictureBox
End Class
