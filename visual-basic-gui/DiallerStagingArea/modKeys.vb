Module modKeys
    Public ultra_client_id As Integer = "10864"

    '' mySql connection details
    Function mySqlGetConnDetails() As Dictionary(Of String, String)
        Dim connDetails As Dictionary(Of String, String) = New Dictionary(Of String, String) From {
            {"server", "192.168.10.10"},
            {"user", "homestead"},
            {"pw", "secret"},
            {"database", "ultra_integration_uncle"}
        }

        Return connDetails
    End Function


    '' SQLSRV conneciton details
    Function GetConnectionString() As String
        Dim dataSource = "ub-avgr01.unclebuck.ukfast"
        Dim user = "DiallerStagingArea"
        Dim pw = "8GG&67f6!£sar2"

        Return "Initial Catalog=DiallerStagingArea;Data Source=" & dataSource & ";User ID=" & user & ";Password=" & pw &
                ";Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
    End Function


    '' Schemas relevant to this integration
    Public Function get_ultra_schemas() As String()
        Dim schemas(1) As String
        schemas(0) = "UNCLE_Integration"
        schemas(1) = "UNCLE_Integration_IVR_pmts"

        Return schemas
    End Function


    Function getUltraLoginDetails(loginType As String) As Dictionary(Of String, String)
        Dim loginDetails As Dictionary(Of String, String) = New Dictionary(Of String, String)


        ''If loginType = "call_attempts" Then
        ''    loginDetails.Add("user", "Int_CA")
        ''    loginDetails.Add("pw", "Unc13Buck")
        ''ElseIf loginType = "dispositions" Then
        ''    loginDetails.Add("user", "Int_Dispositions")
        ''    loginDetails.Add("pw", "Unc13Buck")
        ''ElseIf loginType = "agent_states" Then
        ''    loginDetails.Add("user", "Int_AgentState")
        ''    loginDetails.Add("pw", "Unc13Buck")
        ''ElseIf loginType = "lists" Then
        ''    loginDetails.Add("user", "Int_Lists")
        ''    loginDetails.Add("pw", "Unc13Buck")
        ''ElseIf loginType = "users" Then
        ''    loginDetails.Add("user", "Int_AgentState")
        ''    loginDetails.Add("pw", "Unc13Buck")
        ''    'ElseIf loginType = "update" Then
        ''ElseIf loginType = "user_states" Then
        ''    loginDetails.Add("user", "Int_State")
        ''    loginDetails.Add("pw", "Unc13Buck")
        ''    'ElseIf loginType = "update" Then
        ''Else
        ''    loginDetails.Add("user", "Int_Update")
        ''    loginDetails.Add("pw", "Unc13Buck")
        ''End If


        '' Use test login details to not conflict with current live process
        If loginType = "call_attempts" Or loginType = "dispositions" Or loginType = "user_states" Or loginType = "agent_states" Or loginType = "lists" Or loginType = "users" Then
            loginDetails.Add("user", "Integration_Test")
            loginDetails.Add("pw", "Unc13Buck")
        Else
            loginDetails.Add("user", "Integration_Test_sync")
            loginDetails.Add("pw", "Unc13Buck")
        End If

        Return loginDetails
    End Function


    '' UNCLE API security keys
    Function get_uncle_keys() As Dictionary(Of String, String)
        Dim keys As New Dictionary(Of String, String)

        keys.Add("public_key", "vG1pX58dxsTH1KWd")
        keys.Add("private_key", "tShgyITXc8JmKgV2j5x0fxuyun21h2OO")

        Return keys
    End Function


    Function get_download_types(Optional type As String = "data_download") As String()
        Dim downloadTypes(2) As String

        If type = "data_download" Then
            downloadTypes(0) = "call_attempts"
            downloadTypes(1) = "dispositions"
            downloadTypes(2) = "agent_states"
        Else
            ReDim downloadTypes(1)

            If type = "lists" Then
                downloadTypes(0) = "lists"
                downloadTypes(1) = "users"
            ElseIf type = "call_types" Then
                downloadTypes(0) = "call_types"
                downloadTypes(1) = "call_causes"
            End If
        End If


        Return downloadTypes
    End Function


    Function get_update_types() As String()
        Dim updateTypes(4) As String

        updateTypes(0) = "NewUpload"
        updateTypes(1) = "Suppress"
        updateTypes(2) = "Deactivate"
        updateTypes(3) = "Reactivate"
        updateTypes(4) = "Alter"

        Return updateTypes
    End Function


    '' Details of gmail address details used to send alert emails
    Function get_alert_email_details() As Dictionary(Of String, String)
        Dim email_details As Dictionary(Of String, String) = New Dictionary(Of String, String) From {
            {"account_email", "dialler.integration.unclebuck@gmail.com"},
            {"account_pw", "Unc13Buck"},
            {"account_host", "smtp.gmail.com"},
            {"account_port", "587"},
            {"account_EnableSsl", True},
            {"account_UseDefaultCredentials", False},
            {"default_alert_email", "jvniekerk@unclebuck.co.uk"}
        }
        '{"default_alert_email", "oncall@unclebuck.co.uk"},

        Return email_details
    End Function
End Module
