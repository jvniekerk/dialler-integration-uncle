Module modProcessSteps
    Sub putProcessStepOnDB(process_type As String, process As String)
        Dim batch_id As Integer
        Dim sql As String = ""
        Dim batch_id_increment As Integer = 0

        If process.IndexOf(" procedure") >= 0 Then batch_id_increment = 1


        Try
            batch_id = getBatchId()
        Catch ex As Exception
            writeErrorToDB(ex, "Error getting batch_id => " & ex.Message, batch_id, "process_type: " & process_type & " => process: " & process)
        End Try


        batch_id += batch_id_increment


        Try
            sql = "
                insert into process_run
                (
                    created_at,
                    updated_at,
                    batch_id,
                    process_type,
                    process,
                    completed
                )
                select now(), now(), " & batch_id.ToString & ", '" & process_type & "', '" & process & "', 0;"


            Call mySqlPutNoQuery(sql)
        Catch ex As Exception
            writeErrorToDB(ex, "putProcessStepOnDB failed!! => " & ex.Message, batch_id, "process_type: " & process_type & " => process: " & process, "sql => " & sql)
        End Try
    End Sub


    Function getBatchId() As Integer
        Dim sql As String = "select ifnull(max(batch_id), 0)  from " & mySqlGetConnDetails.Item("database") & ".process_run"

        Return mySqlGetScalar(sql)
    End Function


    Function getLastBatchID(process_type As String, process As String) As Dictionary(Of String, Integer)
        Dim ids As New Dictionary(Of String, Integer)
        Dim sql As String = ""

        Call putProcessStepOnDB(process_type, process)

        Try
            sql = "
                select max(id) as id, max(batch_id) as batch_id from " & mySqlGetConnDetails.Item("database") & ".process_run
                where process_type = '" & process_type & "' and process = '" & process & "' and completed =0;"

            Dim table As DataTable = mySqlGetDataTable(sql)

            ids.Add("id", table.Rows(0).Item(0))
            ids.Add("batch_id", table.Rows(0).Item(1))
        Catch ex As Exception
            writeErrorToDB(ex, "getLastBatchID failed!! => " & ex.Message, ids.Item("batch_id"), "process_type: " & process_type & " => process: " & process, "sql => " & sql)
        End Try

        Return ids
    End Function


    Sub updateProcessStep(ids As Dictionary(Of String, Integer))
        Dim sql As String = ""

        Try
            sql = "
                update " & mySqlGetConnDetails.Item("database") & ".process_run p set
                    p.updated_at = now(), p.completed =1
                where p.id = " & ids.Item("id").ToString & " and p.batch_id = " & ids.Item("batch_id").ToString & ";"

            Call mySqlPutNoQuery(sql)
        Catch ex As Exception
            writeErrorToDB(ex, "updateProcessStep failed!! => " & ex.Message, ids.Item("batch_id"), "id=" & ids.Item("id").ToString, "sql => " & sql)
        End Try
    End Sub
End Module

