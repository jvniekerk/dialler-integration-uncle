Module modUpdateData
    Sub storeUpdateData(list_id As Integer, update_type As String, request As String, response As String, rows As Integer, Optional counter As Integer = 0, Optional process_type As String = "data_sync")
        Dim process As String = "Store " & process_type & " => list_id=" & list_id.ToString & " update_type=""" & update_type & """ rows=" & rows.ToString
        If counter Then process &= " counter=" & counter.ToString

        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)


        If ids.Count Then
            Try
                request = removeDuplicateSpaces(request).Replace("'", "''")
                response = removeDuplicateSpaces(response).Replace("'", "''")


                Dim sql As String = "
            insert into " & mySqlGetConnDetails.Item("database") & ".update_data
            (
                created_at,
                updated_at,
                list_id,
                update_type,
                batch_id,
                sub_id,
                request,
                response
            ) values
            (
                now(), now(),
                '" & list_id.ToString & "',
                '" & update_type & "',
                " & ids.Item("batch_id").ToString & ",
                " & ids.Item("id").ToString & ",
                '" & request & "',
                '" & response & "'
            )"

                mySqlPutNoQuery(sql)
            Catch ex As Exception
                writeErrorToDB(ex, "Error executing storeUpdateData(" & list_id & ", " & update_type & ") => " & ex.Message, ids.Item("batch_id"), request, response)
            End Try

            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    Function getResponseSql(requestTable As DataTable, responseTable As DataTable, updateType As String, list_id As Integer, batch_id As Integer) As String
        Dim sql As String = ""

        Dim cli As Integer
        Dim phone_number1 As Integer
        Dim phone_number2 As Integer
        Dim phone_number3 As Integer
        Dim loanno As Integer
        Dim balance As Integer
        Dim first_name As Integer
        Dim surname As Integer
        Dim title As Integer
        Dim dob As Integer
        Dim unit As Integer
        Dim house_number As Integer
        Dim street As Integer
        Dim postcode As Integer
        Dim dial_purpose As Integer
        Dim priority As Integer

        Dim i As Integer = 0

        Dim rows As Integer = requestTable.Rows.Count
        Dim cols As Integer = requestTable.Columns.Count


        Try
            If updateType <> "Suppress" And updateType <> "Deactivate" Then
                For Each col In requestTable.Columns
                    If col.columnname = "cli" Then cli = i
                    If col.columnname = "phone_number1" Then phone_number1 = i
                    If col.columnname = "phone_number2" Then phone_number2 = i
                    If col.columnname = "phone_number3" Then phone_number3 = i
                    If col.columnname = "loanno" Then loanno = i
                    If col.columnname = "balance" Then balance = i
                    If col.columnname = "first_name" Then first_name = i
                    If col.columnname = "surname" Then surname = i
                    If col.columnname = "title" Then title = i
                    If col.columnname = "dob" Then dob = i
                    If col.columnname = "unit" Then unit = i
                    If col.columnname = "house_number" Then house_number = i
                    If col.columnname = "street" Then street = i
                    If col.columnname = "postcode" Then postcode = i
                    If col.columnname = "dial_purpose" Then dial_purpose = i
                    If col.columnname = "priority" Then priority = i

                    i += 1
                Next
            End If


            If updateType = "NewUpload" Then
                sql = "
                    insert into " & mySqlGetConnDetails.Item("database") & ".current_accounts
                    (
                        created_at,
                        updated_at,
                        list_id,
                        row_id,
                        lms_status,
                        ultra_status,
                        cli,
                        phone_number1,
                        phone_number2,
                        phone_number3,
                        loanno,
                        balance,
                        first_name,
                        surname,
                        title,
                        dob,
                        unit,
                        house_number,
                        street,
                        postcode,
                        dial_purpose,
                        priority
                    ) values
                    "


                For i = 0 To rows - 1
                    If IsNumeric(responseTable.Rows(i).Item(1)) Then
                        With requestTable.Rows(i)
                            sql &= "(now(), now(), '" &
                                list_id & "', '" &
                                responseTable.Rows(i).Item(1) & "', 2, 0, '" &
                                .Item(cli) & "', '" &
                                .Item(phone_number1) & "', '" &
                                .Item(phone_number2) & "', '" &
                                .Item(phone_number3) & "', '" &
                                .Item(loanno) & "', '" &
                                .Item(balance) & "', '" &
                                .Item(first_name) & "', '" &
                                .Item(surname) & "', '" &
                                .Item(title) & "', " &
                                reorderDateTime(.Item(dob)) & ", '" &
                                .Item(unit) & "', '" &
                                .Item(house_number) & "', '" &
                                .Item(street) & "', '" &
                                .Item(postcode) & "', '" &
                                .Item(dial_purpose) & "', '" &
                                .Item(priority)
                        End With

                        sql &= "')"

                        If i < rows - 1 Then
                            sql &= ", "
                        End If
                    End If
                Next
            Else
                Dim row_id As Integer

                i = 0

                For Each col In requestTable.Columns
                    If col.columnname = "row_id" Then row_id = i

                    i += 1
                Next


                If updateType = "Suppress" Or updateType = "Deactivate" Then
                    sql = "
                            update " & mySqlGetConnDetails.Item("database") & ".current_accounts set
                                lms_status = 4, ultra_status = 4
                            where
                                list_id = " & list_id & "
                                and
                                ("

                    For i = 0 To rows - 1
                        sql &= "
                                    row_id = " & requestTable.Rows(i).Item(row_id) & "
                                "

                        If i < rows - 1 Then sql &= "   or"
                    Next

                    sql &= ")"
                Else
                    '' create temp table
                    sql = "
                        DROP TEMPORARY TABLE If EXISTS update_table;

                        CREATE TEMPORARY TABLE update_table (
                            row_id int unsigned,
                            cli varchar(20),
                            phone_number1 varchar(20),
                            phone_number2 varchar(20),
                            phone_number3 varchar(20),
                            balance float,
                            first_name varchar(100),
                            surname varchar(100),
                            title varchar(20),
                            dob date,
                            unit varchar(20),
                            house_number varchar(20),
                            street varchar(200),
                            postcode varchar(20),
                            dial_purpose varchar(100),
                            priority int unsigned
                        );"


                    '' insert update data into temp table
                    sql &= "
                        insert into update_table
                            values"

                    For i = 0 To rows - 1
                        sql &= "
                            (
                                row_id = " & requestTable.Rows(i).Item(row_id).ToString & ",
                                cli = '" & requestTable.Rows(i).Item(cli).ToString.Replace("'", "''") & "',
                                phone_number1 = '" & requestTable.Rows(i).Item(phone_number1).ToString.Replace("'", "''") & "',
                                phone_number2 = '" & requestTable.Rows(i).Item(phone_number2).ToString.Replace("'", "''") & "',
                                phone_number3 = '" & requestTable.Rows(i).Item(phone_number3).ToString.Replace("'", "''") & "',
                                balance = " & requestTable.Rows(i).Item(balance).ToString & ",
                                first_name = '" & requestTable.Rows(i).Item(first_name).ToString.Replace("'", "''") & "',
                                surname = '" & requestTable.Rows(i).Item(surname).ToString.Replace("'", "''") & "',
                                title = '" & requestTable.Rows(i).Item(title).ToString.Replace("'", "''") & "',
                                dob = " & reorderDateTime(requestTable.Rows(i).Item(dob).ToString) & ",
                                unit = '" & requestTable.Rows(i).Item(unit).ToString.Replace("'", "''") & "',
                                house_number = '" & requestTable.Rows(i).Item(house_number).ToString.Replace("'", "''") & "',
                                street = '" & requestTable.Rows(i).Item(street).ToString.Replace("'", "''") & "',
                                postcode = '" & requestTable.Rows(i).Item(postcode).ToString.Replace("'", "''") & "',
                                dial_purpose = '" & requestTable.Rows(i).Item(dial_purpose).ToString.Replace("'", "''") & "',
                                priority = " & requestTable.Rows(i).Item(priority).ToString & "
                            )"

                        If i < rows - 1 Then sql &= ","
                    Next i


                    '' update db table joined to temp table
                    sql &= ";
                        update " & mySqlGetConnDetails.Item("database") & ".current_accounts ca
	                        join update_table n on list_id = " & list_id & " and ca.row_id = n.row_id
                        set
		                    ca.updated_at = now(),
                            ca.lms_status = 2,
                            ca.ultra_status = 0,
                            ca.cli = n.cli,
                            ca.phone_number1 = n.phone_number1,
                            ca.phone_number2 = n.phone_number2,
                            ca.phone_number3 = n.phone_number3,
                            ca.balance = n.balance,
                            ca.first_name = n.first_name,
                            ca.surname = n.surname,
                            ca.title = n.title,
                            ca.dob = n.dob,
                            ca.unit = n.unit,
                            ca.house_number = n.house_number,
                            ca.street = n.street,
                            ca.postcode = n.postcode,
                            ca.dial_purpose = n.dial_purpose,
                            ca.priority = n.priority;
                        "

                    '' drop temp table
                    sql &= "
                        DROP TEMPORARY TABLE IF EXISTS update_table;
                        "

                    'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\sql-" & list_id & "-" & updateType & ".txt", sql)
                    'MsgBox(sql)
                End If

                'MsgBox(sql)
            End If
        Catch ex As Exception
            writeErrorToDB(ex, "getResponseSql failed => " & ex.Message, batch_id, "sql => " & sql)
        End Try


        Return sql
    End Function
End Module
