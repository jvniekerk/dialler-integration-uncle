Imports MySql.Data.MySqlClient

Module modMySQLConn
    Function mySqlGetConnString() As String
        Dim mySQLconnDetails As Dictionary(Of String, String) = mySqlGetConnDetails()

        Dim ConnectionString As String = "server=" & mySQLconnDetails.Item("server") &
                                            "; user id=" & mySQLconnDetails.Item("user") &
                                            "; password=" & mySQLconnDetails.Item("pw") &
                                            "; database=" & mySQLconnDetails.Item("database")

        Return ConnectionString
    End Function


    Function mySqlGetDataTable(sql As String) As DataTable
        Dim reader As MySqlDataReader
        Dim mySQLconn As MySqlConnection = New MySqlConnection()
        Dim table As DataTable = New DataTable


        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\sql-request.txt", sql)

        With mySQLconn
            .ConnectionString = mySqlGetConnString()

            .Open()

            Dim cmd As MySqlCommand = New MySqlCommand(removeDuplicateSpaces(sql), mySQLconn)

            cmd.CommandTimeout = 0

            reader = cmd.ExecuteReader()

            table.Load(reader)
            'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\sql-return_table.txt", getStringFromDataTable(table))

            .Close()

            .Dispose()
        End With


        Return table
    End Function


    Function mySqlGetScalar(sql As String) As String
        Dim id As String
        Dim mySQLconn As MySqlConnection = New MySqlConnection()

        With mySQLconn
            .ConnectionString = mySqlGetConnString()

            .Open()

            Dim cmd As MySqlCommand = New MySqlCommand(removeDuplicateSpaces(sql), mySQLconn)

            cmd.CommandTimeout = 0

            id = cmd.ExecuteScalar().ToString

            .Close()

            .Dispose()
        End With

        Return id
    End Function


    Sub mySqlPutNoQuery(sql As String)
        Dim mySQLconn As MySqlConnection = New MySqlConnection()

        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\sql-request-mySqlPutNoQuery.sql", sql)

        With mySQLconn
            .ConnectionString = mySqlGetConnString()

            .Open()

            Dim cmd As MySqlCommand = New MySqlCommand(removeDuplicateSpaces(sql), mySQLconn)

            cmd.CommandTimeout = 0

            cmd.ExecuteNonQuery()

            .Close()

            .Dispose()
        End With
    End Sub
End Module
