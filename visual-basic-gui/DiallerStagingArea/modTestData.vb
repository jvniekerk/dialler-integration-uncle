Module modTestData
    Function getTestAccountDataSql() As String
        Return "
            declare @funcs int =5
			
			select top 200
	            list_id = ABS(CHECKSUM(NewId())) % 4 +257,
	            row_id = c.RowID,
	            lms_status = c.StagingAreaStatus,
	            ultra_status = c.DiallerStatus,
	            cli = '',
                /*
	            phone_number1 = PhoneNumber1,
	            phone_number2 = PhoneNumber2,
	            phone_number3 = PhoneNumber3,
                */
	            PhoneNumber1 = '02031892512',
	            PhoneNumber2 = '02031892512',
	            PhoneNumber3 = '',
	            loanno = LoanNo,
	            balance = CurrentBalance,
	            first_name = FirstName,
	            surname = Surname,
	            title = Title,
	            dob = DOB,
	            unit = Unit,
	            house_number = HouseNumber,
	            street = Street,
	            postcode = PostCode,
	            dial_purpose = DialPurpose,
				priority = ABS(CHECKSUM(NewId())) % 4 +1,
				update_type = case when ABS(CHECKSUM(NewId())) % @funcs <= 1./@funcs then 'NewUpload'
								when ABS(CHECKSUM(NewId())) % @funcs <= 2./@funcs then 'Suppress'
								when ABS(CHECKSUM(NewId())) % @funcs <= 3./@funcs then 'Deactivate'
								when ABS(CHECKSUM(NewId())) % @funcs <= 4./@funcs then 'Reactivate' else 'Alter' end
            from DiallerStagingArea.dbo.CurrentAccounts c
            where c.StagingAreaStatus =2 and c.ListID =194
            order by newid()
            "
    End Function


    Function getTestJsonDataSql() As String
        Return "
            select top 20
                /*
	            mobile_number = PhoneNumber1,
	            home_number = PhoneNumber2,
	            work_number = PhoneNumber3,
                */
	            PhoneNumber1 = '02031892512',
	            PhoneNumber2 = '02031892512',
	            PhoneNumber3 = '',
	            loanno = c.LoanNo,
	            balance = CurrentBalance,
	            first_name = FirstName,
	            surname = Surname,
	            title = Title,
	            dob = DOB,
	            unit = Unit,
	            house_number = HouseNumber,
	            street = Street,
	            postcode = PostCode,
				dial_purpose = case when ABS(CHECKSUM(NewId())) % 2 < 0.5 then 'Underwriting' else 'Application' end,
                priority = ABS(CHECKSUM(NewId())) % 4 +1
            from DiallerStagingArea.dbo.CurrentAccounts c
				join LAPS.dbo.Loan ln on c.LoanID = ln.LoanID
            where c.StagingAreaStatus =2 and c.ListID =194
            order by newid()
            "
    End Function


    'Function getTestAccountDataJson(list_id As Integer, list_name As String) As String
    Function getTestAccountDataJson(list_name As String) As String
        Dim json As String

        Dim sql As String = getTestJsonDataSql()

        Dim returnTable As DataTable = sqlToDataTable(sql)
        'MsgBox("returnTable rows: " & returnTable.Rows.Count)


        Dim rows As Integer = returnTable.Rows.Count

        json = "
                {""" & list_name & """: ["

        For i As Integer = 0 To rows - 1
            With returnTable.Rows(i)
                json &= "
                    {
                        ""mobile_number"": """ & .Item(0) & """,
                        ""home_number"": """ & .Item(1) & """,
                        ""work_number"": """ & .Item(2) & """,
                        ""loanno"": """ & .Item(3) & """,
                        ""balance"": """ & .Item(4) & """,
                        ""first_name"": """ & .Item(5) & """,
                        ""surname"": """ & .Item(6) & """,
                        ""title"": """ & .Item(7) & """,
                        ""dob"": """ & .Item(8) & """,
                        ""unit"": """ & .Item(9) & """,
                        ""house_number"": """ & .Item(10) & """,
                        ""street"": """ & .Item(11) & """,
                        ""postcode"": """ & .Item(12) & """,
                        ""dial_purpose"": """ & .Item(13) & """,
                        ""priority"": """ & .Item(14) & """
                    }"
            End With

            If i < rows - 1 Then json &= ","
        Next

        json &= "
                ] }"

        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\json-" & list_id & ".txt", removeDuplicateSpaces(json))
        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\json-" & list_id & ".json", removeDuplicateSpaces(json))
        'MsgBox(json)


        Return json
    End Function


    Sub insertTestDataCurrentAccounts()
        Dim test_data As DataTable = sqlToDataTable(getTestAccountDataSql())

        Dim rows As Integer = test_data.Rows.Count
        Dim cols As Integer = test_data.Columns.Count
        MsgBox("Rows test data: " & rows & "; cols: " & cols)


        If rows Then
            Dim sql As String = "
                insert into " & mySqlGetConnDetails.Item("database") & ".current_accounts
                (
                    created_at,
                    updated_at,
                    list_id,
                    row_id,
                    lms_status,
                    ultra_status,
                    cli,
                    phone_number1,
                    phone_number2,
                    phone_number3,
                    loanno,
                    balance,
                    first_name,
                    surname,
                    title,
                    dob,
                    unit,
                    house_number,
                    street,
                    postcode,
                    dial_purpose,
                    priority
                ) values
                "


            For i As Integer = 1 To rows
                sql &= "(now(), now(),"

                For j As Integer = 1 To cols
                    Dim item As String = test_data.Rows(i - 1).Item(j - 1).ToString

                    If item <> "update_type" Then
                        sql &= "'" & item & "'"

                        If j < cols Then
                            sql &= ","
                        End If
                    End If
                Next

                sql &= ")"

                If i < rows Then
                    sql &= ", "
                End If
            Next

            'MsgBox(sql)


            mySqlPutNoQuery(sql)
        Else
            MsgBox("No rows returned")
        End If
    End Sub


    Function getTestEmailsSMSsql() As String
        'Return "
        '    select top 500
        '     loanno = d.LoanNo
        '     ,
        '     emails = count(distinct case when d.CommType = 'email' then d.CommID end)
        '     ,
        '     sms = count(distinct case when d.CommType = 'sms' then d.CommID end)
        '    from (
        '      select distinct
        '       ln.LoanNo
        '       ,
        '       a.Aem_DateSent as DateSent
        '       ,
        '       case when right(a.Aem_To, len('@secure.itagg.com')) = '@secure.itagg.com' then 'SMS' else 'Email' end as CommType
        '       ,
        '       a.AutoEmailID as CommID
        '      from LAPS.dbo.AutoEmail a with (nolock)
        '       join LAPS.dbo.Loan ln with (nolock) on a.Aem_ForeignIDG = ln.LoanID
        '      where
        '       a.Aem_DateSent >= dateadd(day, -0, convert(date, getdate()))
        '       and
        '       a.Aem_HTMLBody not like '%verification%'
        '       and
        '       a.Aem_DateSent >= convert(date, dateadd(day, 1, ln.Lon_DateFunded))
        '       and
        '       ln.Lon_DateFunded is not null
        '    ) d
        '    group by d.LoanNo
        '    order by loanno
        '    "

        Return "
            select --top 500
	            loanno = es.LoanNo
	            ,
	            emails = count(case when es.CommType = 'email' then es.CommID end)
	            ,
	            sms = count(case when es.CommType = 'sms' then es.CommID end)
            from DiallerStagingArea.dbo.EmailsSMS es
            where es.DateSent >= convert(date, getdate())
            group by LoanNo
            order by newid()
"
    End Function


    Function getTestEmailSMSjson() As String
        Dim json As String

        Dim sql As String = getTestEmailsSMSsql()

        Dim table As DataTable = sqlToDataTable(sql)

        Dim rows As Integer = table.Rows.Count
        'MsgBox("returnTable rows: " & rows)


        json = "
                {""emails_sms"": ["

        For i As Integer = 0 To rows - 1
            With table.Rows(i)
                json &= "
                    {
                        ""loanno"": """ & .Item(0) & """,
                        ""emails"": """ & .Item(1) & """,
                        ""sms"": """ & .Item(2) & """
                    }"
            End With

            If i < rows - 1 Then json &= ","
        Next

        json &= "
                ] }"

        saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\email-sms.json", json)
        'MsgBox("emails sms json created")


        Return json
    End Function


    Function getTodayDownloadIDs(download_type As String) As String
        Dim sql As String = ""


        If download_type = "call_attempts" Then
            sql = "select convert(varchar(20), min(ca.UltraID) -1) as id from DiallerStagingArea.dbo.CallAttempts ca where ca.DateStart >= convert(date, getdate())"
        ElseIf download_type = "dispositions" Then
            sql = "select convert(varchar(20), min(ca.UltraID) -1) as id from DiallerStagingArea.dbo.Dispositions ca where ca.DateCreated >= convert(date, getdate())"
        ElseIf download_type = "agent_states" Then
            sql = "select convert(varchar(20), min(ca.Id) -1) as id from DiallerStagingArea.dbo.AgentStates ca where ca.[start] >= convert(date, getdate())"
        End If

        'MsgBox(download_type & ": sql => " & sql)


        Dim id As String = ConnectToSql(sql).ToString.Replace("<insertOnly id=""", "").Replace("""/>", "")
        'MsgBox(download_type & " id: " & id)


        If id = "20190300000567140" Then id = "20190300000567139"

        Return id
    End Function
End Module
