Imports System.Security.Cryptography
Imports System.Net
Imports System.Text

Module modUNCLE
    Function get_md5_hash(input As String) As String
        Using hasher As MD5 = MD5.Create()    ' create hash object

            ' Convert to byte array and get hash
            Dim dbytes As Byte() =
             hasher.ComputeHash(Encoding.UTF8.GetBytes(input))

            ' sb to create string from bytes
            Dim sBuilder As New StringBuilder()

            ' convert byte data to hex string
            For n As Integer = 0 To dbytes.Length - 1
                sBuilder.Append(dbytes(n).ToString("X2"))
            Next n

            Return sBuilder.ToString()
        End Using
    End Function


    Function get_uncle_signature(list_function As String, public_key As String, private_key As String) As String
        Return get_md5_hash(public_key & "|" & list_function & "|" & private_key).ToLower
    End Function


    '' Returns the UNCLE request json string
    Function json_post(list_function As String) As String
        Dim keys As Dictionary(Of String, String) = get_uncle_keys()

        Return "{
    ""key"" : """ & keys.Item("public_key") & """,
    ""func"" : """ & list_function & """,
    ""signature"" : """ & get_uncle_signature(list_function, keys.Item("public_key"), keys.Item("private_key")) & """
}"
    End Function


    Function get_uncle_post_url(list_function As String) As String
        Dim path As String = "https://staging.lms.unclebuck.link/api/dialler/list/"

        If list_function = "PreFund" Then
            Return path & "pre-fund"
        ElseIf list_function = "Funded" Then
            Return path & "funded"
        ElseIf list_function = "Arrears" Then
            Return path & "arrears"
        ElseIf list_function = "Complaints" Then
            Return path & "complaints"
        ElseIf list_function = "IVR_Payments" Then
            Return path & "ivr-payments"
        ElseIf list_function = "Emails-SMS" Then
            Return path & "emails-sms"
        Else
            Return path
        End If
    End Function


    '' Posts the request to UNCLE
    Function post_uncle(list_function As String, batch_id As Integer) As String
        Dim webClient As New WebClient()
        Dim resByte As Byte()
        Dim response As String = ""
        Dim reqString() As Byte
        Dim json As String = json_post(list_function)

        'Dim path As String = "C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\uncle-json-request-" & list_function & "-" & batch_id.ToString & ".json"
        'saveTextFile(path, json)

        Try
            webClient.Headers("content-type") = "application/json"

            reqString = Encoding.Default.GetBytes(json)

            resByte = webClient.UploadData(get_uncle_post_url(list_function), "post", reqString)

            response = Encoding.Default.GetString(resByte)

            webClient.Dispose()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            writeErrorToDB(ex, ex.Message, batch_id.ToString, json, response)
        End Try


        response = response.Replace("'", "''")


        Return fix_response_header(response, list_function)
    End Function


    Function fix_response_header(response As String, list_function As String) As String
        'Dim path As String = "C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\uncle-response-" & list_function & ".json"

        response = response.Replace(":null", ":""""")


        If list_function = "PreFund" Then
            response = response.Replace("prefund", "PreFund")
        ElseIf list_function = "Funded" Then
            response = response.Replace("funded", "Funded")
        ElseIf list_function = "Arrears" Then
            response = response.Replace("arrears", "Arrears")
        ElseIf list_function = "Complaints" Then
            response = response.Replace("complaints", "Complaints")
        ElseIf list_function = "IVR_Payments" Then
            response = response.Replace("ivr_payments", "IVR_Payments").Replace("IvrPayments", "IVR_Payments")

            'saveTextFile(path, response)
        ElseIf list_function = "Emails-SMS" Then
            response = response.Replace("emails-sms", "emails_sms")
        End If

        'saveTextFile(path, response)

        Return response
    End Function
End Module
