Module modDownloadData
    Sub downloadDiallerData()
        Dim process_type As String = "data_download"
        Dim process As String = "Data download procedure"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)

        If ids.Count > 0 Then
            Dim downloadTypes() As String

            '' Checks if lists have been updated today and updates if not
            If Not checkListsUpdatedToday() Then
                '' lists & users
                ReDim downloadTypes(1)

                downloadTypes = get_download_types("lists")

                For Each downloadType In downloadTypes
                    If Len(downloadType) Then
                        Try
                            Call getUltraDownloadData(downloadType)
                        Catch ex As Exception
                            writeErrorToDB(ex, "Update " & downloadType & " for today failed!! => " & ex.Message, ids.Item("batch_id"))
                        End Try
                    End If
                Next


                '' Call types & causes
                ReDim downloadTypes(1)

                downloadTypes = get_download_types("call_types")

                For Each downloadType In downloadTypes
                    Try
                        Call putUltraCallTypesCauses(downloadType)
                    Catch ex As Exception
                        writeErrorToDB(ex, "Update " & downloadType & " for today failed!! => " & ex.Message, ids.Item("batch_id"))
                    End Try
                Next downloadType
            End If


            '' call attempts, dispositions & agent states
            ReDim downloadTypes(2)

            downloadTypes = get_download_types("data_download")


            For Each downloadType In downloadTypes
                If Len(downloadType) Then
                    Try
                        Call getUltraDownloadData(downloadType)
                    Catch ex As Exception
                        writeErrorToDB(ex, "Update " & downloadType & " failed!! => " & ex.Message, ids.Item("batch_id"))
                    End Try
                End If
            Next


            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    Function checkListsUpdatedToday() As Boolean
        Dim sql As String = "select count(*) as updates from " & mySqlGetConnDetails.Item("database") & ".process_run where created_at >= CURRENT_DATE and process_type = 'data_download' and process = 'lists';"

        Try
            Dim count As Integer = mySqlGetScalar(sql)

            If count Then
                Return True
            End If
        Catch ex As Exception
            writeErrorToDB(ex, "Getting lists updated today check failed!! => " & ex.Message, 0, "sql => " & sql)
        End Try


        Return False
    End Function
End Module
