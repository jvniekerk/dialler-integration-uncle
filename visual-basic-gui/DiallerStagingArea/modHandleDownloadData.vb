Module modHandleDownloadData
    Sub getUltraDownloadData(download_type As String)
        Dim process_type As String = "data_download"
        Dim process As String = download_type
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)


        If ids.Count > 0 Then
            Dim sql As String = ""          '' sql code to get last ID from db to update from
            Dim rowCount As Integer = 950   '' number of rows to download at a time
            Dim request As String = ""      '' request sent to Ultra
            Dim response As String = ""     '' return value from Ultra
            Dim id As String                '' last download id to use for next batch of transactions


            Try
                '' Get the last Ultra ID from the database that was already downloaded
                id = getUltraID(download_type)


                If id.IndexOf("Invalid") < 0 Then
                    request = getUltraDownloadXML(download_type, id.ToString, rowCount)
                Else
                    Try
                        Throw New Exception("Ultra Download ID not defined")
                    Catch ex As Exception
                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), "Ultra id => " & id.ToString)
                    End Try
                End If


                If request.IndexOf("Invalid") < 0 Then
                    Dim UltraLoginDetails As Dictionary(Of String, String) = getUltraLoginDetails(download_type)

                    response = GetUltraDownload(request, UltraLoginDetails.Item("user"), UltraLoginDetails.Item("pw"))

                    'Dim filepath As String = "C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\ultra-response-" & download_type & ".xml"
                    'saveTextFile(filepath, response)


                    '' Store the number of rows so the app doesn't sleep if there are lots or rows to download
                    If download_type = "call_attempts" Then
                        Form1.download_rows = getRowNumbers(response)
                    ElseIf download_type = "dispositions" Or download_type = "agent_states" Then
                        Dim rows As Integer = getRowNumbers(response)

                        If rows > Form1.download_rows Then Form1.download_rows = rows
                    End If


                    Dim processXML As Boolean = True    '' check if the response XML should be parsed or not - not currently used


                    '' Checks if any errors occurred in the download and report it
                    If response.IndexOf("error=") >= 0 Then
                        Try
                            Throw New Exception("Error in Ultra Response")
                        Catch ex As Exception
                            writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), request, response)
                        End Try
                    End If


                    '' Store the request and response to the database
                    If processXML Then
                        '' Write data to UNCLE db
                        mySqlPutNoQuery(getUploadStringFromXML(response, download_type, ids.Item("batch_id")))


                        storeDownloadData(download_type, request, response)
                    End If
                Else
                    Try
                        Throw New Exception("Invalid request for download_type: " & download_type)
                    Catch ex As Exception
                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), request)
                    End Try
                End If


                '' In addition to downloading the users, also download the user_states at the same time
                If download_type = "users" Then Call getUltraUserStates()
            Catch ex As Exception
                If Len(request) = 0 Then request = "Request not set yet!!"

                Dim sent As String = "sql: " & sql & " => request: " & request

                writeErrorToDB(ex, "Error executing getUltraDownloadData(" & download_type & ") => " & ex.Message, ids.Item("batch_id"), sent, response)
            End Try


            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    Sub storeDownloadData(download_type As String, request As String, response As String)
        Dim process_type As String = "data_download"
        Dim process As String = "Store data_download """ & download_type & """"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)

        If ids.Count Then
            Try
                request = removeDuplicateSpaces(request).Replace("'", "''")
                response = removeDuplicateSpaces(response).Replace("'", "''")


                Dim sql As String = "
                    insert into " & mySqlGetConnDetails.Item("database") & ".data_download
                    (
                        created_at,
                        updated_at,
                        batch_id,
                        download_type,
                        request,
                        response
                    ) values
                    (
                        now(), now(),
                        " & ids.Item("batch_id").ToString & ",
                        '" & download_type & "',
                        '" & request & "',
                        '" & response & "'
                    )"

                mySqlPutNoQuery(sql)
            Catch ex As Exception
                writeErrorToDB(ex, "Error executing storeDownloadData(" & download_type & ") => " & ex.Message, ids.Item("batch_id"), request, response)
            End Try

            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    Function getUploadStringFromXML(input As String, download_type As String, batch_id As Integer) As String
        Dim xml As XElement = XElement.Parse(input)
        Console.WriteLine(xml)

        Dim xmlRows As IEnumerable(Of XElement) = From row In xml...<row> Select row

        Dim sql As String = ""

        Dim rows As Integer = xmlRows.Count()


        If rows Then
            Dim i As Integer = 1

            Try
                If download_type = "call_attempts" Then
                    sql = "insert ignore into " & mySqlGetConnDetails.Item("database") & "." & download_type & "
                    (
	                    created_at,
                        updated_at,
                        ultra_id,
	                    customer_number,
	                    cli_ddi,
	                    start,
	                    end,
	                    causes_id,
	                    type_id,
	                    transaction_id,
	                    event_source,
	                    client_ref,
	                    user_id,
	                    team_id,
	                    row_id,
	                    list_id,
	                    campaign_id,
	                    initial_service_id,
	                    final_service_id,
	                    dial_time,
	                    ivr_time,
	                    call_wait_time,
	                    talk,
	                    wrap,
	                    station,
                        loanno
                    ) values
                        "


                    For Each el As XElement In xmlRows
                        sql &= "("

                        sql &= "now(), now(), "
                        sql &= "'" & el.@ID & "', "
                        sql &= "'" & el.@CustomerNumber & "', "
                        sql &= "'" & el.@CLI_DDI & "', "
                        sql &= reorderDateTime(el.@Start) & ", "
                        sql &= reorderDateTime(el.@End) & ", "
                        sql &= "'" & el.@CauseID & "', "
                        sql &= "'" & el.@TypeID & "', "
                        sql &= "'" & el.@TransactionID & "', "
                        sql &= "'" & el.@EventSource & "', "
                        sql &= "'" & el.@ClientRef & "', "
                        sql &= "'" & el.@UserID.Replace("-", "") & "', "
                        sql &= "'" & el.@TeamID & "', "
                        sql &= "'" & el.@RowID & "', "
                        sql &= "'" & el.@ListID & "', "
                        sql &= "'" & el.@CampaignID & "', "
                        sql &= "'" & el.@InitialServiceID & "', "
                        sql &= "'" & el.@FinalServiceID & "', "
                        sql &= "'" & el.@DialTime & "', "
                        sql &= "'" & el.@IVRTime & "', "
                        sql &= "'" & el.@CallWaitTime & "', "
                        sql &= "'" & el.@Talk & "', "
                        sql &= "'" & el.@Wrap & "', "
                        sql &= "'" & el.@Station & "', "
                        sql &= "'0'"


                        If i < rows Then
                            sql &= "),"
                        Else
                            sql &= ");"
                        End If

                        i += 1
                    Next
                ElseIf download_type = "dispositions" Then
                    sql = "insert ignore into " & mySqlGetConnDetails.Item("database") & "." & download_type & "
                    (
	                    created_at,
                        updated_at,
                        ultra_id,
                        date_created,
	                    disposition_code,
	                    description,
	                    transaction_id,
	                    campaign_id,
	                    list_id,
	                    row_id,
	                    user_id,
                        loanno
                    ) values
                    "


                    For Each el As XElement In xmlRows
                        sql &= "("

                        sql &= "now(), now(), "
                        sql &= "'" & el.@ID & "', "
                        sql &= reorderDateTime(el.@CreatedDate) & ", "
                        sql &= "'" & el.@DispositionCode & "', "
                        sql &= "'" & el.@Description & "', "
                        sql &= "'" & el.@TransactionID & "', "
                        sql &= "'" & el.@CampaignID & "', "
                        sql &= "'" & el.@ListID & "', "
                        sql &= "'" & el.@RowID & "', "
                        sql &= "'" & el.@UserID.Replace("-", "") & "', "
                        sql &= "'0'"


                        If i < rows Then
                            sql &= "),"
                        Else
                            sql &= ");"
                        End If

                        i += 1
                    Next
                ElseIf download_type = "agent_states" Then
                    sql = "insert ignore into " & mySqlGetConnDetails.Item("database") & "." & download_type & "
                    (
	                    created_at,
                        updated_at,
                        ultra_id,
                        state,
	                    start,
	                    end,
	                    transaction_id,
	                    user_id,
	                    team_id,
	                    chain_id,
	                    list_id,
                        station
                    ) values
                        "


                    For Each el As XElement In xmlRows
                        sql &= "("

                        sql &= "now(), now(), "
                        sql &= "'" & el.@Id & "', "
                        sql &= "'" & el.@state & "', "
                        sql &= reorderDateTime(el.@start) & ", "
                        sql &= reorderDateTime(el.@end) & ", "
                        sql &= "'" & el.@transactionid & "', "
                        sql &= "'" & el.@userid.Replace("-", "") & "', "
                        sql &= "'" & el.@teamid & "', "
                        sql &= "'" & el.@chainid & "', "
                        sql &= "'" & el.@baseid & "', "
                        sql &= "'" & el.@station & "'"


                        If i < rows Then
                            sql &= "),"
                        Else
                            sql &= ");"
                        End If

                        i += 1
                    Next
                ElseIf download_type = "lists" Then
                    sql = "
                    DROP TEMPORARY TABLE IF EXISTS ultra_" & download_type & ";

                    CREATE TEMPORARY TABLE ultra_lists (
	                    created_at datetime,
	                    updated_at datetime,
	                    list_id int unsigned,
                        list_name varchar(200),
                        schema_code varchar(200)
                    );

                    insert into ultra_" & download_type & "
                    (
	                    created_at,
	                    updated_at,
	                    list_id,
                        list_name,
                        schema_code
                    ) values
                    "


                    For Each el As XElement In xmlRows
                        sql &= "("

                        sql &= "now(), now(), "
                        sql &= "'" & el.@BaseIndexId & "', "
                        sql &= "'" & el.@BaseName & "', "
                        sql &= "'" & el.@SchemaCode & "'"


                        If i < rows Then
                            sql &= "),"
                        Else
                            sql &= ")"
                        End If

                        i += 1
                    Next


                    sql &= ";

                    insert ignore into " & mySqlGetConnDetails.Item("database") & "." & download_type & "
                    (
	                    created_at,
	                    updated_at,
	                    list_id,
                        list_name,
                        schema_code
                    )
	                select distinct
                        u.created_at,
	                    u.updated_at,
	                    u.list_id,
                        u.list_name,
                        u.schema_code
                    from ultra_" & download_type & " u
                    order by u.list_id;

                    DROP TEMPORARY TABLE IF EXISTS ultra_" & download_type & ";
                "
                ElseIf download_type = "users" Then
                    sql = "
                    DROP TEMPORARY TABLE IF EXISTS ultra_" & download_type & ";

                    CREATE TEMPORARY TABLE ultra_users (
	                    created_at datetime,
	                    updated_at datetime,
	                    user_id int unsigned,
                        username varchar(200),
                        email_address varchar(200),
                        first_name varchar(200),
                        last_name varchar(200),
                        telephone_number varchar(20),
                        last_activity_date datetime,
                        last_login_date datetime
                    );

                    insert into ultra_" & download_type & "
                    (
	                    created_at,
	                    updated_at,
	                    user_id,
                        username,
                        email_address,
                        first_name,
                        last_name,
                        telephone_number,
                        last_activity_date,
                        last_login_date
                    ) values
                    "


                    For Each el As XElement In xmlRows
                        If el.@EmailAddress.ToString.IndexOf("unclebuck") > 0 Then  '' only import unclebuck users
                            sql &= "("

                            sql &= "now(), "
                            sql &= "now(), "
                            sql &= "'" & el.@UserId.Replace("-", "") & "', "
                            sql &= "'" & el.@Username.Replace("'", "''") & "', "
                            sql &= "'" & el.@EmailAddress & "', "
                            sql &= "'" & el.@FirstName.Replace("'", "''") & "', "
                            sql &= "'" & el.@LastName.Replace("'", "''") & "', "
                            sql &= "'" & el.@TelephoneNumber & "', "
                            sql &= reorderDateTime(el.@LastActivityDate) & ", "
                            sql &= reorderDateTime(el.@LastLoginDate)


                            If i < rows Then
                                sql &= "),"
                            Else
                                sql &= ");"
                            End If
                        End If

                        i += 1
                    Next


                    sql &= "

                    insert ignore into " & mySqlGetConnDetails.Item("database") & "." & download_type & "
                    (
	                    created_at,
	                    updated_at,
	                    user_id,
                        username,
                        email_address,
                        first_name,
                        last_name,
                        telephone_number,
                        last_activity_date,
                        last_login_date
                    )
	                select distinct
                        u.created_at,
	                    u.updated_at,
	                    u.user_id,
                        u.username,
                        u.email_address,
                        u.first_name,
                        u.last_name,
                        u.telephone_number,
                        u.last_activity_date,
                        u.last_login_date
                    from ultra_" & download_type & " u
                    order by u.user_id;
    
                    update " & mySqlGetConnDetails.Item("database") & "." & download_type & " u
	                    join ultra_" & download_type & " n on u.user_id = n.user_id
                    set
		                u.updated_at = n.updated_at,
		                u.username = n.username,
		                u.email_address = n.email_address,
		                u.first_name = n.first_name,
		                u.last_name = n.last_name,
		                u.telephone_number = n.telephone_number,
		                u.last_activity_date = n.last_activity_date,
		                u.last_login_date = n.last_login_date;

                    DROP TEMPORARY TABLE IF EXISTS ultra_" & download_type & ";
                "
                End If
            Catch ex As Exception
                writeErrorToDB(ex, "Error executing getUploadStringFromXML => " & ex.Message, batch_id, download_type & ": " & input)
            End Try
        End If


        '' update loanno on call_attemps & dispositions tables
        If download_type = "call_attempts" Or download_type = "dispositions" Then
            sql &= getUpdateLoannoSql(download_type)


            ''''' change list_id for testing ''''
            ''''' change list_id for testing ''''
            'sql &= Environment.NewLine & " update " & mySqlGetConnDetails.Item("database") & "." & download_type & " set list_id =257
            '    where list_id <>0;"
            ''''' change list_id for testing ''''
            ''''' change list_id for testing ''''
        End If

        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\mysql-" & download_type & ".txt", sql)
        'MsgBox("mySql update/insert code built for download_type: " & download_type)


        Return sql
    End Function


    Function getUpdateLoannoSql(download_type As String) As String
        Return Environment.NewLine & "
            update " & mySqlGetConnDetails.Item("database") & "." & download_type & " ca 
                join " & mySqlGetConnDetails.Item("database") & ".current_accounts c on c.list_id = ca.list_id and c.row_id = ca.row_id
            set ca.loanno = c.loanno
            where ca.loanno =0;
            "
    End Function


    Function getUltraID(download_type As String) As String
        Dim id As String
        Dim sql As String = ""


        If download_type = "call_attempts" Or download_type = "agent_states" Then
            sql = "select cast(ifnull(max(ultra_id),
                        concat(cast(extract(year from date_add(current_date(), interval -13 month)) as char)
                        , (case when extract(month from date_add(current_date(), interval -13 month)) <10 then '0' else '' end)
                        , cast(extract(month from date_add(current_date(), interval -13 month)) as char)
                        , '00000000000')) as char) as id
                    FROM " & mySqlGetConnDetails.Item("database") & "." & download_type & ";"
            'sql = "select cast(ifnull(max(ultra_id), '20180200000000000')) as char) as id FROM " & mySqlGetConnDetails.Item("database") & "." & download_type & ";"
        ElseIf download_type = "dispositions" Then
            sql = "select cast(ifnull(max(ultra_id), '7644186') as char) as id FROM " & mySqlGetConnDetails.Item("database") & "." & download_type & ";"
        ElseIf download_type = "lists" Or download_type = "users" Then
            sql = "SQL code not required for download_type: " & download_type & "!!"
        Else
            sql = "Invalid download_type: " & download_type & "!!"
        End If


        If sql.IndexOf("select ") >= 0 Then
            id = mySqlGetScalar(sql).ToString


            '''' test sql ''''
            '''' test sql ''''
            '''' test sql ''''
            '''' test sql ''''
            If download_type = "call_attempts" Or download_type = "dispositions" Or download_type = "agent_states" Then
                sql = "select count(*) as table_rows FROM " & mySqlGetConnDetails.Item("database") & "." & download_type & ";"
                'MsgBox(sql)

                Dim rows As Integer = mySqlGetScalar(sql)


                If rows = 0 Then
                    Dim date_field As String
                    Dim id_field As String = "Ultra_ID"

                    If download_type = "agent_states" Then
                        id_field = "Id"
                    End If


                    If download_type = "call_attempts" Then
                        date_field = "DateStart"
                    ElseIf download_type = "dispositions" Then
                        date_field = "DateCreated"
                    Else
                        date_field = "[start]"
                    End If


                    sql = "select convert(varchar(50), (min(" & id_field & ") -1)) as id FROM DiallerStagingArea.dbo." & download_type.Replace("_", "") & " with (nolock) where " & date_field & " >= convert(date, getdate());"
                    'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\test-sql-" & download_type & ".txt", sql)
                    'MsgBox(download_type & " => " & sql)


                    '''' test ids from old db ''''
                    '''' test ids from old db ''''
                    id = getTodayDownloadIDs(download_type)
                    '''' test ids from old db ''''
                    '''' test ids from old db ''''
                End If
            End If
            '''' test sql ''''
            '''' test sql ''''
            '''' test sql ''''
            '''' test sql ''''


            Return id
        Else
            Return sql
        End If
    End Function


    Function getUltraDownloadXML(download_type As String, id As String, rowCount As Integer) As String
        Dim xml As String = "<data clientid=""" & ultra_client_id.ToString & """>"

        Dim xml_end As String = "</data>"

        Dim rowCount_id As String = " ID=""" & id.ToString & """ RowCount=""" & rowCount.ToString & """"

        Dim download_end As String = rowCount_id & " />" & xml_end


        If download_type = "call_attempts" Then
            xml &= "<Report.CustomerCDRId" & download_end
        ElseIf download_type = "dispositions" Then
            xml &= "<Report.Disposition.CDR" & download_end
        ElseIf download_type = "agent_states" Then
            xml &= "<Report.Agent.Status.CDRId" & download_end.Replace("ID=", "Id=")
        ElseIf download_type = "lists" Then
            xml &= "<System.BaseIndex />" & xml_end
        ElseIf download_type = "users" Then
            xml &= "<Framework.User />" & xml_end
        Else
            xml = "Invalid download_type: " & download_type & "!!"
        End If

        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\test-xml-" & download_type & ".xml", xml)


        Return xml
    End Function
End Module
