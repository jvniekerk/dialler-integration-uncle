Module modAccountState
    '' Updates Ultracomms lists with current_accounts data
    Sub checkUltraAccountState()
        Dim process_type As String = "data_check"
        Dim process As String = "Account states check procedure"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)

        If ids.Count Then
            '' Gets a list of lists to update
            Dim lists As DataTable = getLists()


            '' Update lists if nothing on db
            If lists.Rows.Count() = 0 Then
                Try
                    getUltraDownloadData("lists")

                    lists = getLists()
                Catch ex As Exception
                    writeErrorToDB(ex, "Getting lists failed => " & ex.Message, ids.Item("batch_id"))
                End Try
            End If


            If lists.Rows.Count() > 0 Then
                For Each row As DataRow In lists.Rows
                    Dim list_id As Integer
                    Dim schema As String = ""

                    '' Gets the column ids that contain the list_id & schema
                    For Each col In lists.Columns
                        If col.ColumnName = "list_id" Then
                            list_id = row(col).ToString
                        ElseIf col.ColumnName = "schema_code" Then
                            schema = row(col).ToString
                        End If
                    Next


                    If list_id > 0 And Len(schema) > 0 Then
                        Dim db_table As New DataTable

                        '' get list of accounts from dialler db
                        Dim sql As String = "
                        select
                            ca.list_id,
                            ca.row_id as RowId,
                            ca.ultra_status as _State,
                            ca.phone_number1 as PhoneNumber1,
                            ca.phone_number2 as PhoneNumber2,
                            ca.phone_number3 as PhoneNumber3,
                            ca.loanno,
                            ca.balance,
                            ca.first_name,
                            ca.surname,
                            ca.title,
                            ca.dob,
                            ca.unit,
                            ca.house_number,
                            ca.street,
                            ca.postcode,
                            ca.dial_purpose,
                            ca.priority as _SchedulePriority
                        from " & mySqlGetConnDetails.Item("database") & ".current_accounts ca
                        where ca.list_id = " & list_id.ToString & "
                        order by ca.row_id;
                        "


                        Try
                            db_table = mySqlGetDataTable(sql)
                        Catch ex As Exception
                            writeErrorToDB(ex, "Getting accounts list to check status failed => " & ex.Message, ids.Item("batch_id"))
                        End Try


                        Dim rows As Integer = db_table.Rows.Count()

                        If rows > 0 Then
                            '' get Ultra login details
                            Dim UltraLoginDetails As New Dictionary(Of String, String)

                            Try
                                UltraLoginDetails = getUltraLoginDetails("update")
                            Catch ex As Exception
                                writeErrorToDB(ex, "Building Ultra login details failed => " & ex.Message, ids.Item("batch_id"))
                            End Try


                            '' Parses & processes the data returned from the database
                            If UltraLoginDetails.Count() > 0 Then
                                Dim row_id_col As Integer
                                Dim state_col As Integer
                                Dim phone1_col As Integer
                                Dim phone2_col As Integer
                                Dim phone3_col As Integer
                                Dim loanno_col As Integer
                                Dim balance_col As Integer
                                Dim first_name_col As Integer
                                Dim surname_col As Integer
                                Dim title_col As Integer
                                Dim dob_col As Integer
                                Dim unit_col As Integer
                                Dim house_nr_col As Integer
                                Dim street_col As Integer
                                Dim postcode_col As Integer
                                Dim dial_purpose_col As Integer
                                Dim priority_col As Integer


                                Dim i As Integer = 0

                                '' Gets the relevant column ids for each field
                                For Each col In db_table.Columns
                                    Dim colName As String = col.ColumnName

                                    If row_id_col = 0 And colName = "RowId" Then
                                        row_id_col = i
                                    ElseIf state_col = 0 And colName = "_State" Then
                                        state_col = i
                                    ElseIf phone1_col = 0 And colName = "PhoneNumber1" Then
                                        phone1_col = i
                                    ElseIf phone2_col = 0 And colName = "PhoneNumber2" Then
                                        phone2_col = i
                                    ElseIf phone3_col = 0 And colName = "PhoneNumber3" Then
                                        phone3_col = i
                                    ElseIf loanno_col = 0 And colName = "loanno" Then
                                        loanno_col = i
                                    ElseIf balance_col = 0 And colName = "balance" Then
                                        balance_col = i
                                    ElseIf first_name_col = 0 And colName = "first_name" Then
                                        first_name_col = i
                                    ElseIf surname_col = 0 And colName = "surname" Then
                                        surname_col = i
                                    ElseIf title_col = 0 And colName = "title" Then
                                        title_col = i
                                    ElseIf dob_col = 0 And colName = "dob" Then
                                        dob_col = i
                                    ElseIf unit_col = 0 And colName = "unit" Then
                                        unit_col = i
                                    ElseIf house_nr_col = 0 And colName = "house_number" Then
                                        house_nr_col = i
                                    ElseIf street_col = 0 And colName = "street" Then
                                        street_col = i
                                    ElseIf postcode_col = 0 And colName = "postcode" Then
                                        postcode_col = i
                                    ElseIf dial_purpose_col = 0 And colName = "dial_purpose" Then
                                        dial_purpose_col = i
                                    ElseIf priority_col = 0 And colName = "_SchedulePriority" Then
                                        priority_col = i
                                    End If

                                    i += 1
                                Next


                                Dim min_row_id As Integer = db_table.Rows(0).Item(row_id_col)
                                Dim max_row_id As Integer = db_table.Rows(rows - 1).Item(row_id_col)

                                If min_row_id > 0 Then
                                    Dim check_margin As Integer = 1000  '' Number of rows before and after the min and max row_ids to check over

                                    '' loop through max(1, min_row_id -check_margin) to max_row_id +check_margin
                                    '' on Sunday night do the above but starting at row_id =1 to 2*(max_row_id - min_row_id)
                                    If Today.Date.DayOfWeek = DayOfWeek.Sunday Then
                                        max_row_id += (max_row_id - min_row_id)

                                        min_row_id = 1
                                    Else
                                        max_row_id += check_margin

                                        If min_row_id - check_margin > 0 Then
                                            min_row_id -= check_margin
                                        Else
                                            min_row_id = 1
                                        End If
                                    End If


                                    Dim updateOnly As String = ""
                                    Dim toUpdate As Integer = 0
                                    Dim j As Integer = 0


                                    Try
                                        For i = min_row_id To max_row_id
                                            '' if the row exists in dialler db add it to updateOnly list else add to deleteOnly list
                                            If j < rows Then   '' row j exists in table
                                                If db_table.Rows(j).Item(row_id_col) = i Then   '' i is an existing row_id
                                                    '' add to updateOnly
                                                    updateOnly &= Environment.NewLine &
                                            "   <updateOnly RowId=""" & i.ToString & """ _State=""" & db_table.Rows(j).Item(state_col) &
                                            """  PhoneNumber1=""" & db_table.Rows(j).Item(phone1_col) & """ PhoneNumber2=""" & db_table.Rows(j).Item(phone2_col) &
                                            """ PhoneNumber3=""" & db_table.Rows(j).Item(phone3_col) & """ loanno=""" & db_table.Rows(j).Item(loanno_col) &
                                            """ balance=""" & db_table.Rows(j).Item(balance_col) & """ first_name=""" & db_table.Rows(j).Item(first_name_col) &
                                            """ surname=""" & db_table.Rows(j).Item(surname_col) & """ title=""" & db_table.Rows(j).Item(title_col) &
                                            """ dob=""" & db_table.Rows(j).Item(dob_col) & """ unit=""" & db_table.Rows(j).Item(unit_col) &
                                            """ house_number=""" & db_table.Rows(j).Item(house_nr_col) & """ street=""" & db_table.Rows(j).Item(street_col) &
                                            """ postcode=""" & db_table.Rows(j).Item(postcode_col) & """ dial_purpose=""" & db_table.Rows(j).Item(dial_purpose_col) &
                                            """ _SchedulePriority=""" & db_table.Rows(j).Item(priority_col) & """ />"

                                                    j += 1
                                                Else
                                                    '' add to deleteOnly
                                                    updateOnly &= Environment.NewLine & "   <deleteOnly RowId=""" & i.ToString & """ />"
                                                End If
                                            Else
                                                '' add to deleteOnly
                                                updateOnly &= Environment.NewLine & "   <deleteOnly RowId=""" & i.ToString & """ />"
                                            End If

                                            toUpdate += 1


                                            '' if one of these list sizes reaches 950 send contents to Ultra and start building list again
                                            Dim send_rows As Integer = 950

                                            If toUpdate = send_rows Then
                                                Try
                                                    Dim response As String = getUltraUpdateStateResponse(updateOnly, schema, list_id, UltraLoginDetails)


                                                    Try
                                                        '' store the update data on the dialler database
                                                        storeUpdateData(list_id, process, updateOnly, response, rows,, process_type)
                                                    Catch ex As Exception
                                                        writeErrorToDB(ex, "Storing delete old accounts failed!!" & ex.Message, ids.Item("batch_id"), updateOnly, response)
                                                    End Try
                                                Catch ex As Exception
                                                    writeErrorToDB(ex, "Executing Ultra state update failed!! => " & ex.Message, ids.Item("batch_id"), "Account state request => " & updateOnly)
                                                End Try

                                                toUpdate = 0
                                            End If
                                        Next


                                        '' check if still rows to be sent to Ultra
                                        If toUpdate Then
                                            Dim response As String = getUltraUpdateStateResponse(updateOnly, schema, list_id, UltraLoginDetails)


                                            Try
                                                '' store the update data on the dialler database
                                                storeUpdateData(list_id, process, updateOnly, response, rows,, process_type)
                                            Catch ex As Exception
                                                writeErrorToDB(ex, "Storing delete old accounts failed!!" & ex.Message, ids.Item("batch_id"), updateOnly, response)
                                            End Try
                                        End If
                                    Catch ex As Exception
                                        Dim text As String

                                        If j < rows Then
                                            text = "i=" & i.ToString & " update row " & j & " row_id=" & db_table.Rows(j).Item(row_id_col) & " => updateOnly => " & updateOnly
                                        Else
                                            text = "Some issue after completing table loop => updateOnly => " & updateOnly
                                        End If

                                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), text)
                                    End Try
                                Else
                                    Try
                                        Throw New Exception("Couldn't set min_row_id to compare account states")
                                    Catch ex As Exception
                                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), getStringFromDataTable(db_table))
                                    End Try
                                End If
                            End If
                        End If
                    End If
                Next
            Else
                Try
                    Throw New Exception("Getting list of lists failed!!")
                Catch ex As Exception
                    writeErrorToDB(ex, "Getting lists failed => " & ex.Message, ids.Item("batch_id"))
                End Try
            End If


            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    Function getUltraUpdateStateResponse(updateOnly As String, schema As String, list_id As Integer, UltraLoginDetails As Dictionary(Of String, String)) As String
        Dim pre_xml As String = "<upload clientid='10864'><" & schema & " BaseIndexId='" & list_id.ToString & "'><rows>"
        Dim post_xml As String = Environment.NewLine & "</rows></" & schema & "></upload>"


        Dim request As String = pre_xml & updateOnly & post_xml
        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\request.xml", request)
        'MsgBox(request)


        Dim response As String = GetUltraUpload(request, UltraLoginDetails.Item("user"), UltraLoginDetails.Item("pw"))
        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\response.xml", response)
        'MsgBox(response)


        Return response
    End Function


    '' Check if states were updated in the past 6 hours
    Function check_accounts_states() As Boolean
        Dim hours_to_check As Integer = 6

        Dim sql As String = "select count(*) as successful_runs from process_run p where p.process_type = 'data_check' and completed =1 and time_to_sec(TIMEDIFF(now(), created_at)) /(60 * 60.) < " & hours_to_check.ToString & " ;"

        Dim successful_runs As Integer = mySqlGetScalar(sql)


        If successful_runs Then
            Return False
        Else
            Return True
        End If
    End Function
End Module
