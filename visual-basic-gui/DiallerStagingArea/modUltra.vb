Option Explicit On
Imports System.ComponentModel

Module modUltra
    Public Function GetUltraDownload(ByVal request As String, ByVal un As String, ByVal pw As String) As String
        Dim thisDownload As New net.ultraasp.client.download.Service

        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12

        Return thisDownload.DownloadDataByUserString("unclebuck", un, pw, request)
    End Function


    Public Function GetUltraUpload(ByVal request As String, ByVal un As String, ByVal pw As String) As String
        Dim thisDownload As New net.ultraasp.client.upload.Service

        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12

        Return thisDownload.UploadDataByUserString("unclebuck", un, pw, request)
    End Function


    Function BuildXML(ByVal schema As String, ByVal listID As String, ByVal thisTable As DataTable, ByVal criteria As String) As String
        Dim xml As String = "<upload clientid=""" & ultra_client_id & """><" & schema & " BaseIndexId=""" & listID & """><rows>"

        xml &= XMLfromDataTable(thisTable, criteria)

        xml &= "</rows></" & schema & "></upload>"

        Return xml
    End Function
End Module






'Sub CallAttemptsDownloadByDate()
'    Try
'        Dim query As String = "exec DiallerStagingArea.dbo.sp_CallAttempts_GetLastStart "

'        Dim startDate As DateTime = New DateTime()

'        startDate = GetDateTime(ConnectToSql(query, False))

'        Dim endDate As Date = DateAdd(DateInterval.Hour, 9, startDate)

'        Dim request As String = "<data clientid=""" & ultra_client_id & """><Report.CustomerCDR Start=""" & startDate & """ End=""" & endDate & """ RowCount=""950"" /></data>"

'        Dim returnVal As String = UltraDownload(request)

'        query = "exec DiallerStagingArea.[dbo].[sp_DownloadDataStoreXML] 'CDR' , '" & returnVal & "' "

'        Call ConnectToSql(query, False)
'    Catch
'    End Try
'End Sub


'Public Function UltraDownload(ByVal request As String) As String
'    Dim testDownload As New net.ultraasp.client.download.Service

'    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12

'    Return testDownload.DownloadDataByUserString("unclebuck", "Int_CA", "Unc13Buck", request)
'End Function


'Public Sub updateDialler(ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs)
'    'If worker.CancellationPending Then
'    '    e.Cancel = True
'    '    MsgBox("Upload Thread Complete. You can exit now or restart the update.")
'    'Else
'    'Call syncData()
'    'End If



'    'Try
'    '    If worker.CancellationPending Then
'    '        e.Cancel = True
'    '        MsgBox("Upload Thread Complete. You can exit now or restart the update.")
'    '    Else
'    '        Try
'    '            'MsgBox("About to initialise SDB update")

'    '            Call ConnectSqlSendSOAPs("exec DiallerStagingArea.dbo.sp_UpdateSDB")
'    '        Catch ex As Exception
'    '            MsgBox(ex.Message)
'    '        End Try

'    '        'Dim runNow As String = ConnectToSql("DiallerStagingArea.dbo.sp_AccountsToCompare_CheckToRun ", False)
'    '        'If runNow.ToString = "1" Then
'    '        '    sqlToDataTable("insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Account State to be Checked' ")
'    '        '    Call CheckAccountStates()
'    '        'End If

'    '        'MsgBox("dialler update done")
'    '    End If
'    'Catch ex As Exception
'    '    MsgBox(ex.Message)
'    'End Try
'End Sub


'Public Function buildEnvelope(ByVal xmlString As String, ByVal schema As String, ByVal baseIndex As String) As String
'    'Dim sEnv As String = "<?xml version=""1.0"" encoding=""utf-8""?>"
'    'sEnv = sEnv & "<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">"
'    'sEnv = sEnv & "<soap12:Body>"
'    'sEnv = sEnv & "<UploadDataByUser xmlns=""http://ultraasp.net/"">"
'    'sEnv = sEnv & "<domain>unclebuck</domain>"
'    'sEnv = sEnv & "<username>UploaderSQL</username>"
'    'sEnv = sEnv & "<password>Password1</password>"
'    'sEnv = sEnv & "<xmlData>"
'    'sEnv = sEnv & "<upload clientid=""10864"">"
'    'sEnv = sEnv & "<" & schema & " BaseIndexId=""" & baseIndex & """>"
'    'sEnv = sEnv & "<rows>"
'    'sEnv = sEnv & xmlString
'    'sEnv = sEnv & "</rows>"
'    'sEnv = sEnv & "</" & schema & ">"
'    'sEnv = sEnv & "</upload>"
'    'sEnv = sEnv & "</xmlData>"
'    'sEnv = sEnv & "</UploadDataByUser>"
'    'sEnv = sEnv & "</soap12:Body>"
'    'sEnv = sEnv & "</soap12:Envelope>"

'    'Return sEnv

'    Return 1
'End Function


'Public Function sendSoap(ByVal url As String, ByVal envelope As String, Optional ByVal listID As Integer = 0) As String
'    '' Set and Instantiate the working objects
'    'Dim ObjHTTP As Object = CreateObject("MSXML2.ServerXMLHTTP")

'    '' Invoke the web service
'    'With ObjHTTP
'    '    .open("POST", url, False)
'    '    .setRequestHeader("Content-Type", "application/soap+xml; charset=utf-8")
'    '    .setTimeouts(60000, 60000, 1200000, 1200000)
'    '    .send(envelope)

'    '    Dim myString As String = .responseText
'    '    Dim status As Integer = .status.ToString()

'    '    If status = 200 Then
'    '        Return myString
'    '    Else
'    '        Return "Status code: " & status
'    '    End If
'    'End With
'    'ObjHTTP = Nothing

'    Return 1
'End Function


'Public Function sendJson(ByVal url As String, ByVal envelope As String, Optional ByVal listID As Integer = 0) As String
'    '' Set and Instantiate the working objects
'    'Dim ObjHTTP As Object = CreateObject("MSXML2.ServerXMLHTTP")

'    '' Invoke the web service
'    'With ObjHTTP
'    '    .open("POST", url, False)
'    '    .setRequestHeader("Content-Type", "application/json; charset=utf-8")
'    '    .setTimeouts(60000, 60000, 1200000, 1200000)
'    '    .send(envelope)

'    '    Dim myString As String = .responseText
'    '    Dim status As Integer = .status.ToString()

'    '    If status = 200 Then
'    '        Return myString
'    '    Else
'    '        Return "Status code: " & status
'    '    End If
'    'End With
'    'ObjHTTP = Nothing

'    Return 1
'End Function
