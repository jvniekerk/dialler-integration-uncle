Module modUserStates
    Sub getUltraUserStates()
        Dim process_type As String = "data_download"
        Dim process As String = "User states"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)

        If ids.Count > 0 Then
            '' get Ultra login details
            Dim UltraLoginDetails As New Dictionary(Of String, String)

            Try
                UltraLoginDetails = getUltraLoginDetails("user_states")
            Catch ex As Exception
                writeErrorToDB(ex, "Building Ultra login details failed => " & ex.Message, ids.Item("batch_id"))
            End Try


            If UltraLoginDetails.Count() > 0 Then
                Dim response As String = ""
                Dim request As String = "<data clientid=""" & ultra_client_id & """><Client.Users.State.States /></data>"

                Try
                    response = GetUltraDownload(request, UltraLoginDetails.Item("user"), UltraLoginDetails.Item("pw"))
                    'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\user-states.xml", response)
                Catch ex As Exception
                    writeErrorToDB(ex, "Building Ultra login details failed => " & ex.Message, ids.Item("batch_id"), request, response)
                End Try


                If Len(response) > 0 Then
                    Dim sql As String = getUserStatesSql(response)

                    If Len(sql) > 0 Then
                        Try
                            Call mySqlPutNoQuery(sql)
                        Catch ex As Exception
                            writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), "request => " & request & Environment.NewLine & " => sql= " & sql, response)
                        End Try
                    Else
                        Try
                            Throw New Exception("Setting SQL code to update user_states failed!!")
                        Catch ex As Exception
                            writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), request, response)
                        End Try

                    End If
                Else
                    Try
                        Throw New Exception("Getting Ultra response to update user_states failed!!")
                    Catch ex As Exception
                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"), request, response)
                    End Try
                End If
            End If


            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    Function getUserStatesSql(response As String) As String
        Dim xml As XElement = XElement.Parse(response)
        Console.WriteLine(xml)

        Dim xmlRows As IEnumerable(Of XElement) = From row In xml...<row> Select row

        Dim rows As Integer = xmlRows.Count()

        Dim i As Integer = 1


        Dim sql As String = "
            drop table if exists states;
            
            create table states
            (
                id int,
                stateid int,
                systemstate int,
                statedescription varchar(100),
                statered int,
                stategreen int,
                stateblue int,
                category varchar(100),
                duration int,
                productive varchar(10),
                scheduled varchar(10),
                working varchar(10)
            );
            insert into states
            values
                "


        Try
            For Each el As XElement In xmlRows
                sql &= "("

                sql &= "'" & el.@id & "', "
                sql &= "'" & el.@stateid & "', "
                sql &= "'" & el.@systemstate & "', "
                sql &= "'" & el.@statedescription & "', "
                sql &= "'" & el.@statered & "', "
                sql &= "'" & el.@stategreen & "', "
                sql &= "'" & el.@stateblue & "', "
                sql &= "'" & el.@category & "', "
                sql &= "'" & el.@duration & "', "
                sql &= "'" & el.@productive & "', "
                sql &= "'" & el.@scheduled & "', "
                sql &= "'" & el.@working & "'"


                If i < rows Then
                    sql &= "),
                "
                Else
                    sql &= ");
                    "
                End If

                i += 1
            Next


            sql &= "
            insert ignore into " & mySqlGetConnDetails.Item("database") & ".user_states
            (
                created_at,
                updated_at,
                ultra_id,
                state_id,
                system_state,
                system_description,
                state_red,
                state_green,
                state_blue,
                category,
                duration,
                productive,
                scheduled,
                working
            )
            select now(), now(), s.* from states s order by s.id;

            update " & mySqlGetConnDetails.Item("database") & ".user_states us
                join states s on us.state_id = s.stateid
            set
                us.updated_at = now(),
                us.system_state = s.systemstate,
                us.system_description = s.statedescription,
                us.state_red = s.statered,
                us.state_green = s.stategreen,
                us.state_blue = s.stateblue,
                us.category = s.category,
                us.duration = s.duration,
                us.productive = s.productive,
                us.scheduled = s.scheduled,
                us.working = s.working
            where
                us.system_state <> s.systemstate or
                us.system_description <> s.statedescription or
                us.state_red <> s.statered or
                us.state_green <> s.stategreen or
                us.state_blue <> s.stateblue or
                us.category <> s.category or
                us.duration <> s.duration or
                us.productive <> s.productive or
                us.scheduled <> s.scheduled or
                us.working <> s.working;

            drop table if exists states;
                "


            'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\mysql-user-states.sql", sql)
        Catch ex As Exception
            writeErrorToDB(ex, "Error executing getUserStatesSql => " & ex.Message, 0, "sql => " & sql)
        End Try

        Return sql
    End Function
End Module
