Module modErrorHandling
    Public nr_errors_to_exit As Integer = 10    '' Exit the app if this number of errors have been logged or alert emails sent

    Sub writeErrorToDB(ex As Exception, subject As String, batch_id As Integer, Optional request As String = "", Optional response As String = "", Optional sendEmail As Boolean = True)
        If Form1.errors < nr_errors_to_exit Then
            Dim sql As String = ""


            Try
                'MsgBox("errors=" & Form1.errors)

                Dim message As String = ex.Message & " => " & ex.StackTrace & " => " & ex.ToString

                subject = subject.Replace("\", "/")
                message = message.Replace("\", "/")
                request = request.Replace("\", "/")
                response = response.Replace("\", "/")


                'Dim timeStamp As String = DateTime.Now.ToString("yyyyMMdd-HHmmss")
                'Dim path As String = "C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\error-"

                'saveTextFile(path & timeStamp & "-details.txt",
                '             "BATCH_ID => " & Environment.NewLine & batch_id.ToString & Environment.NewLine & Environment.NewLine &
                '             "SUBJECT => " & Environment.NewLine & subject & Environment.NewLine & Environment.NewLine &
                '             "SEND EMAIL => " & sendEmail & Environment.NewLine & Environment.NewLine &
                '             "MESSAGE => " & Environment.NewLine & message & Environment.NewLine & Environment.NewLine &
                '             "SENT => " & Environment.NewLine & request & Environment.NewLine & Environment.NewLine &
                '             "RESPONSE => " & Environment.NewLine & response)


                subject = removeDuplicateSpaces(subject.Replace("'", "''"))
                message = removeDuplicateSpaces(message.Replace("'", "''"))
                request = removeDuplicateSpaces(request.Replace("'", "''"))
                response = removeDuplicateSpaces(response.Replace("'", "''"))


                '' SQL to write error to database
                sql = "
                insert into " & mySqlGetConnDetails.Item("database") & ".error_log
                (
                    created_at,
                    updated_at,
                    batch_id,
                    subject,
                    message,
                    sent,
                    response
                ) values
                (
                    now(),
                    now(),
                    '" & batch_id.ToString & "',
                    '" & subject & "',
                    '" & message & "',
                    '" & request & "',
                    '" & response & "'
                )"

                'saveTextFile(path & timeStamp & "-sql.txt", sql)
                'MsgBox(subject & " => Message: " & ex.Message)


                Try
                    mySqlPutNoQuery(sql)


                    '' Send  and alert email
                    If sendEmail And Form1.emails_sent < nr_errors_to_exit Then
                        'MsgBox("emails=" & Form1.emails_sent)

                        sendErrorEmail(request, response, subject, ex, batch_id)

                        Form1.emails_sent += 1
                    End If
                Catch exep As Exception
                    Dim text As String = "sql => " & sql & Environment.NewLine & "request => " & request

                    writeErrorToDB(ex, "Write error to database failed!! => " & exep.Message, batch_id, text, response)
                End Try
            Catch exp As Exception
                writeErrorToDB(exp, "writeErrorToDB failed!!" & exp.Message & " => original exception message => " & subject, batch_id, request, response)
            End Try


            Form1.errors += 1
        Else
            Call Form1.stop_app()
        End If
    End Sub


    Sub sendErrorEmail(request As String, response As String, ex_subject As String, ex As Exception, batch_id As Integer)
        Dim subject As String = "Dialler Integration UNCLE error"

        Dim body As String = "Exception subject => " & ex_subject & Environment.NewLine & Environment.NewLine &
            "batch_id => " & batch_id.ToString & Environment.NewLine & Environment.NewLine &
            "request => " & request & Environment.NewLine & Environment.NewLine &
            "response => " & response & Environment.NewLine & Environment.NewLine &
            "exception => " & ex.ToString


        Call sendAlertEmail(subject, body, batch_id)
    End Sub
End Module
