'' Updates the call types and causes
Module modCallTypesCauses
    Sub putUltraCallTypesCauses(type As String)
        Dim process_type As String = "data_download"
        Dim process As String = "Call types & causes"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)

        If ids.Count Then
            Dim sql As String = ""

            Try
                sql = getCallTypesCausesSql(type)

                If Len(sql) Then
                    Call mySqlPutNoQuery(sql)
                Else
                    Try
                        Throw New Exception("Getting sql code for updating " & type & " failed!!")
                    Catch ex As Exception
                        writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
                    End Try
                End If
            Catch ex As Exception
                writeErrorToDB(ex, "Updating " & type & " failed => " & ex.Message, ids.Item("batch_id"), "sql => " & sql)
            End Try

            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub


    Function getCallTypesCausesSql(type As String)
        Dim sql As String
        Dim cols As String = ""
        Dim values As String = ""
        Dim id_col As String = ""
        Dim desc_col As String = ""

        If type = "call_types" Then
            id_col = "type_id"

            desc_col = "call_type"

            values = "
                    ( 1, 'Outbound' ),
                    ( 2, 'Postdial' ),
                    ( 3, 'Predial' ),
                    ( 4, 'Transfer' ),
                    ( 5, 'Inbound' )"
        ElseIf type = "call_causes" Then
            id_col = "cause_id"

            desc_col = "call_cause"

            values = "
                    ( -4, 'Busy' ),
                    ( -3, 'No Answer' ),
                    ( -2, 'User Disconnect' ),
                    ( -1, 'Normal Disconnect' ),
                    ( 0, 'Inactive' ),
                    ( 1, 'Call Failed' ),
                    ( 2, 'Abandoned no users available' ),
                    ( 3, 'Abandoned user connection failed' ),
                    ( 4, 'Predictive Hang Up' ),
                    ( 5, 'Out of Hours Inbound' ),
                    ( 6, 'Answerphone' ),
                    ( 7, 'AMD' )"
        End If

        sql = "
                drop table if exists types;
                
                create table types
                (
                    type_id int,
                    call_type varchar(50)
                );

                insert into types
                values" & values & ";
                

                insert ignore into " & mySqlGetConnDetails.Item("database") & "." & type & "
                (
                    created_at,
                    updated_at,
                    " & id_col & ",
                    " & desc_col & "
                )
                select now(), now(), t.* from types t order by t.type_id;


                update " & mySqlGetConnDetails.Item("database") & "." & type & " ct
                    join types t on ct." & id_col & " = t.type_id
                set
                    updated_at = now(),
                    ct." & desc_col & " = t.call_type
                where ct." & desc_col & " <> t.call_type;


                drop table if exists types;
               "

        'saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\" & type & ".sql", sql)

        Return sql
    End Function
End Module
