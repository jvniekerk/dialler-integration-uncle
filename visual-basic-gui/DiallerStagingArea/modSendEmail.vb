Imports System.Net.Mail

Module modSendEmail
    Sub sendAlertEmail(subject As String, body As String, batch_id As Integer)
        Try
            Dim email_details As Dictionary(Of String, String) = get_alert_email_details()

            Dim Smtp_Server As New SmtpClient

            Smtp_Server.UseDefaultCredentials = email_details.Item("account_UseDefaultCredentials")
            Smtp_Server.Credentials = New System.Net.NetworkCredential(email_details.Item("account_email"), email_details.Item("account_pw"))
            Smtp_Server.Port = email_details.Item("account_port")
            Smtp_Server.EnableSsl = email_details.Item("account_EnableSsl")
            Smtp_Server.Host = email_details.Item("account_host")


            Dim email As New MailMessage()
            email = New MailMessage()

            Dim emails_addresses As String() = get_alert_email_addresses()
            Dim rows As Integer = emails_addresses.Count()

            With email
                .From = New MailAddress(email_details.Item("account_email"))

                If rows Then
                    For i As Integer = 0 To rows - 1
                        .To.Add(emails_addresses(i))
                    Next i
                Else
                    .To.Add(email_details.Item("default_alert_email"))
                End If


                .Subject = subject

                .IsBodyHtml = False
                .Body = body
            End With


            Try
                Smtp_Server.Send(email)

                'MsgBox("Alert email sent", vbOKOnly, "Error Alert Email")
            Catch ex As SmtpFailedRecipientException
                writeErrorToDB(ex, "Sending email failed!! => " & ex.Message, batch_id, "subject => " & subject, "body => " & body, False)
            End Try
        Catch ex As Exception
            writeErrorToDB(ex, "Sending alert email failed!! => " & ex.Message, batch_id, "Subject => " & subject, "Body => " & body, False)
        End Try
    End Sub


    Function get_alert_email_addresses() As String()
        Dim sql As String = "select distinct email from " & mySqlGetConnDetails.Item("database") & ".alert_email_addresses where active = 1;"

        Dim emails As DataTable = mySqlGetDataTable(sql)

        Dim rows As Integer = emails.Rows.Count() - 1

        Dim emails_list(rows) As String
        Dim i As Integer = 0

        For Each row As DataRow In emails.Rows
            emails_list(i) = row.Item(0).ToString

            i += 1
        Next row


        Return emails_list
    End Function
End Module
