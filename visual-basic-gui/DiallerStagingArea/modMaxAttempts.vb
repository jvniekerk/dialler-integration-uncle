Option Explicit On

Module modMaxAttempts
    Sub maxAttempts()
        Dim process_type As String = "data_sync"
        Dim process As String = "Max attempts process"
        Dim ids As Dictionary(Of String, Integer) = getLastBatchID(process_type, process)

        If ids.Count Then
            '' get number of emails & SMSs from UNCLE and write to dialler db
            Dim json As String = post_uncle("Emails-SMS", ids.Item("batch_id"))

            Dim rows As Integer = getNumberArrays(json, "loanno")

            Dim sql As String = "call " & mySqlGetConnDetails.Item("database") & ".update_emails_sms('" & json & "', " & rows.ToString & ");"


            Try
                Call mySqlPutNoQuery(sql)
            Catch ex As Exception
                writeErrorToDB(ex, "Error updating email SMS data => " & ex.Message, ids.Item("batch_id"), "sql => " & sql)
            End Try


            updateProcessStep(ids)
        Else
            Try
                Throw New Exception("Getting process steps ids for process """ & process & """ on process_type """ & process_type & """ failed!!")
            Catch ex As Exception
                writeErrorToDB(ex, ex.Message, ids.Item("batch_id"))
            End Try
        End If
    End Sub
End Module
