
DROP PROCEDURE IF EXISTS max_attempts;

CREATE PROCEDURE max_attempts()
BEGIN
	declare max_contacts int;
    set max_contacts = 6;
    
    
    insert into ultra_integration_uncle.max_attempts
    (
        created_at,
        updated_at,
        loanno,
        calls,
        emails,
        sms
    )
    select
        now() as created_at,
        now() as updated_at,
        ca.loanno,
        ca.calls,
        ca.emails,
        ca.sms
    from ultra_integration_uncle.contact_attempts ca
        left join ultra_integration_uncle.max_attempts ma on ca.loanno = ma.loanno and ma.created_at >= CURRENT_DATE
    where
        ca.created_at >= CURRENT_DATE
        and
        ma.loanno is null
        and
        ca.emails + ca.sms + ca.calls >= max_contacts;

--     select * from ultra_integration_uncle.max_attempts where created_at >= CURRENT_DATE;



    insert into ultra_integration_uncle.suppress
    (
        created_at,
        updated_at,
        loanno,
        reason
    )
    select 
        now() as created_at,
        now() as updated_at,
        ma.loanno,
        concat('Max contact attempts: ', ma.emails + ma.sms + ma.calls, '; calls: ', ma.calls, '; emails: ', ma.emails,
                '; sms: ', ma.sms)
    from ultra_integration_uncle.max_attempts ma
        left join ultra_integration_uncle.suppress s on ma.loanno = s.loanno and s.created_at >= CURRENT_DATE
    where 
        s.loanno is null 
        and 
        ma.created_at >= CURRENT_DATE;

--     select * from ultra_integration_uncle.suppress where created_at >= CURRENT_DATE;
END;