

DROP PROCEDURE IF EXISTS aggragate_last_month_calls;

CREATE PROCEDURE aggragate_last_month_calls(
)
BEGIN
    declare start_this_month date;
    declare start_last_month date;


    set start_this_month = concat(extract(year from CURRENT_DATE), '/', extract(month from CURRENT_DATE), '/01');
    set start_last_month = date_add(start_this_month, interval -1 month);


    drop table if exists last_month;


    create table last_month
    (
        date_called date,
        list_id int,
        causes_id int,
        type_id int,
        user_id int,
        disposition_code int,
        disposition_description varchar(200),
        calls int,
        dial_time int,
        ivr_time int,
        wait_time int,
        talk_time int,
        wrap_time int
    );



    insert into last_month
        select
            cast(c.start as date) as date_called,
            c.list_id,
            c.causes_id,
            c.type_id,
            c.user_id,
            ifnull(d.disposition_code, 0) as disposition_code,
            ifnull(d.description, '') as disposition_description,
            count(distinct c.id) as calls,
            sum(c.dial_time) as dial_time,
            sum(c.ivr_time) as ivr_time,
            sum(c.call_wait_time) as wait_time,
            sum(c.talk) as talk_time,
            sum(c.wrap) as wrap_time
        from ultra_integration_uncle.call_attempts c 
            left join ultra_integration_uncle.dispositions d on c.transaction_id = d.transaction_id
        where
            c.start >= start_last_month
            and
            c.start < start_this_month
        group by 
            cast(c.start as date),
            c.list_id,
            c.causes_id,
            c.type_id,
            c.user_id,
            ifnull(d.disposition_code, 0),
            ifnull(d.description, '')
            ;



    insert ignore into call_attempts_last_month
    (
        created_at,
        updated_at,
        date_called,
        list_id,
        causes_id,
        type_id,
        user_id,
        disposition_code,
        disposition_description,
        calls,
        dial_time,
        ivr_time,
        wait_time,
        talk_time,
        wrap_time
    )
    select now(), now(), lm.* from last_month lm;



    update call_attempts_last_month ca 
        join last_month lm on ca.date_called = lm.date_called
                            and ca.list_id = lm.list_id
                            and ca.causes_id = lm.causes_id
                            and ca.type_id = lm.type_id
                            and ca.user_id = lm.user_id
                            and ca.disposition_code = lm.disposition_code
                            and ca.disposition_description = lm.disposition_description
    set
        ca.updated_at = now(),
        ca.calls = lm.calls,
        ca.dial_time = lm.dial_time,
        ca.ivr_time = lm.ivr_time,
        ca.wait_time = lm.wait_time,
        ca.talk_time = lm.talk_time,
        ca.wrap_time = lm.wrap_time
    where ca.talk_time >0;


    select *
    -- delete
    from call_attempts 
    where 
        start >= start_last_month 
        and 
        start < start_this_month
        and 
        talk =0
    ;

    

    drop table if exists last_month;
END;
            