
DROP PROCEDURE IF EXISTS update_emails_sms;

CREATE PROCEDURE update_emails_sms(
	json_string json,
    max_count int
)
BEGIN
    declare counter int;
    declare path varchar(36);
    
    declare loanno varchar(50);
    declare emails varchar(50);
    declare sms varchar(50);

    drop table if exists json_data;
    
    create table json_data
    (
        loanno int,
        emails int,
		sms int
    );
    

    SET counter = 0;
    
    /* convert json data string to table */
    WHILE counter < max_count DO
        set path = concat('$.emails_sms[', counter, '].');
        
        set loanno = concat(path, 'loanno');
        set emails = concat(path, 'emails');
        set sms = concat(path, 'sms');

        
        insert into json_data
            select
                replace(json_extract(json_string, loanno), '"', '') as loanno,
                replace(json_extract(json_string, emails), '"', '') as emails,
                replace(json_extract(json_string, sms), '"', '') as sms;
        
        SET  counter = counter + 1; 
    END WHILE;

--     select * from json_data;


    
    insert into email_sms
    (
        created_at,
        updated_at,
        loanno,
        emails,
        sms
    )
    select
        now() as created_at,
        now() as updated_at,
        jd.loanno,
        jd.emails,
        jd.sms
    from json_data jd
        left join ultra_integration_uncle.email_sms es on jd.loanno = es.loanno and es.created_at >= CURRENT_DATE
    where es.loanno is null;



    update ultra_integration_uncle.email_sms es
        join json_data jd on es.loanno = jd.loanno
    set
        es.updated_at = now(),
        es.emails = jd.emails,
        es.sms = jd.sms
    where es.created_at >= CURRENT_DATE;

--     select * from ultra_integration_uncle.email_sms where created_at >= CURRENT_DATE;



    /* get all the loanno where contact was made today, i.e. call attempts or email & SMS */
    drop table if exists loans;

    create table loans ( loanno int );

    insert into loans
        select distinct jd.loanno from json_data jd;


    insert into loans
        select distinct ca.loanno from ultra_integration_uncle.call_attempts ca 
            left join loans ln on ca.loanno = ln.loanno
        where ln.loanno is null and ca.loanno >0;

--     select * from loans;



    /* calculate all the contact attempts for today */
    drop table if exists contacts;

    create table contacts
    (
        loanno int,
        emails int,
        sms int,
        calls int
    );


    insert into contacts
        select
            ln.loanno,
            sum(es.emails) as emails,
            sum(es.sms) as sms,
            count(ca.transaction_id) as calls
        from loans ln
            left join ultra_integration_uncle.email_sms es on ln.loanno = es.loanno and es.created_at >= CURRENT_DATE
            left join ultra_integration_uncle.call_attempts ca on ln.loanno = ca.loanno and ca.created_at >= CURRENT_DATE
        group by ln.loanno
        order by ln.loanno;

    select * from contacts;



    insert into ultra_integration_uncle.contact_attempts
    (
        created_at,
        updated_at,
        loanno,
        emails,
        sms,
        calls
    )
    select 
        now() as created_at,
        now() as updated_at,
        c.loanno,
        c.emails,
        c.sms,
        c.calls
    from contacts c 
        left join ultra_integration_uncle.contact_attempts ca on c.loanno = ca.loanno and ca.created_at >= CURRENT_DATE
    where ca.loanno is null
    order by c.loanno;



    update ultra_integration_uncle.contact_attempts ca 
        join contacts c on ca.loanno = c.loanno
    set 
        ca.updated_at = now(),
        ca.emails = c.emails,
        ca.sms = c.sms,
        ca.calls = c.calls
    where
        ca.created_at >= CURRENT_DATE
        and
        ca.emails + ca.sms + ca.calls <> c.emails + c.sms + c.calls;

    select * from ultra_integration_uncle.contact_attempts where created_at >= CURRENT_DATE;



    call max_attempts();


    call suppress_dispositions();

    
    
    drop table if exists json_data;

    drop table if exists loans;

    drop table if exists contacts;
END;
            