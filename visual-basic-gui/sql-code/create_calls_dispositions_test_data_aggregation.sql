﻿



select count(*) as call_attempts from call_attempts;

-- select *  from call_attempts;

select count(*) as dispositions from dispositions;

-- select * from dispositions;

select count(*) as agent_states from agent_states;

-- select * from agent_states;




set @start_this_month = concat(extract(year from CURRENT_DATE), '/', extract(month from CURRENT_DATE), '/01');
set @start_last_month = date_add(@start_this_month, interval -1 month);



drop table if exists transaction_ids_to_keep;
    

-- create records for last month
create table transaction_ids_to_keep ( transaction_id bigint );

insert into transaction_ids_to_keep
    select distinct c.transaction_id from call_attempts c where (c.transaction_id %4) =0 and c.start >= @start_this_month;

-- select * from transaction_ids_to_keep;


update call_attempts c 
    join transaction_ids_to_keep t on c.transaction_id = t.transaction_id
set
    c.created_at = DATE_ADD(c.created_at, interval -1 month),
    c.updated_at = DATE_ADD(c.updated_at, interval -1 month),
    c.start = DATE_ADD(c.start, interval -1 month),
    c.end = DATE_ADD(c.end, interval -1 month);

-- select * from call_attempts c join transaction_ids_to_keep t on c.transaction_id = t.transaction_id;


update dispositions c 
    join transaction_ids_to_keep t on c.transaction_id = t.transaction_id
set
    c.created_at = DATE_ADD(c.created_at, interval -1 month),
    c.updated_at = DATE_ADD(c.updated_at, interval -1 month),
    c.date_created = DATE_ADD(c.date_created, interval -1 month)
    ;

-- select * from dispositions c join transaction_ids_to_keep t on c.transaction_id = t.transaction_id;



-- create records for last year
delete from transaction_ids_to_keep;

insert into transaction_ids_to_keep
    select distinct c.transaction_id from call_attempts c where (c.transaction_id %3) =0 and c.start >= @start_this_month;

-- select * from transaction_ids_to_keep;


update call_attempts c 
    join transaction_ids_to_keep t on c.transaction_id = t.transaction_id
set
    c.created_at = DATE_ADD(c.created_at, interval -3 month),
    c.updated_at = DATE_ADD(c.updated_at, interval -3 month),
    c.start = DATE_ADD(c.start, interval -3 month),
    c.end = DATE_ADD(c.end, interval -3 month);

-- select * from call_attempts c join transaction_ids_to_keep t on c.transaction_id = t.transaction_id;


update dispositions c 
    join transaction_ids_to_keep t on c.transaction_id = t.transaction_id
set
    c.created_at = DATE_ADD(c.created_at, interval -3 month),
    c.updated_at = DATE_ADD(c.updated_at, interval -3 month),
    c.date_created = DATE_ADD(c.date_created, interval -3 month)
    ;

-- select * from dispositions c join transaction_ids_to_keep t on c.transaction_id = t.transaction_id;



-- create older records for agent_states
delete from transaction_ids_to_keep;

-- last month
insert into transaction_ids_to_keep
    select distinct id from agent_states where start >= @start_this_month and (id %4) =0;


update agent_states c 
    join transaction_ids_to_keep t on c.id = t.transaction_id
set
    c.created_at = DATE_ADD(c.created_at, interval -1 month),
    c.updated_at = DATE_ADD(c.updated_at, interval -1 month),
    c.start = DATE_ADD(c.start, interval -1 month),
    c.end = DATE_ADD(c.end, interval -1 month);


-- last year
delete from transaction_ids_to_keep;

insert into transaction_ids_to_keep
    select distinct id from agent_states where start >= @start_this_month and (id %3) =0;


update agent_states c 
    join transaction_ids_to_keep t on c.id = t.transaction_id
set
    c.created_at = DATE_ADD(c.created_at, interval -3 month),
    c.updated_at = DATE_ADD(c.updated_at, interval -3 month),
    c.start = DATE_ADD(c.start, interval -3 month),
    c.end = DATE_ADD(c.end, interval -3 month);



drop table if exists transaction_ids_to_keep;


    

select
    count(*) as calls,
    count(distinct case when start >= CURRENT_DATE then id end) as today,
    count(distinct case when start >= @start_this_month then id end) as this_month,
    count(distinct case when start < @start_this_month and start >= @start_last_month then id end) as last_month,
    count(distinct case when start < @start_last_month then id end) as last_year
from call_attempts;
    

select
    count(*) as dispositions,
    count(distinct case when date_created >= CURRENT_DATE then id end) as today,
    count(distinct case when date_created >= @start_this_month then id end) as this_month,
    count(distinct case when date_created < @start_this_month and date_created >= @start_last_month then id end) as last_month,
    count(distinct case when date_created < @start_last_month then id end) as last_year
from dispositions;
    

select
    count(*) as agent_states,
    count(distinct case when start >= CURRENT_DATE then id end) as today,
    count(distinct case when start >= @start_this_month then id end) as this_month,
    count(distinct case when start < @start_this_month and start >= @start_last_month then id end) as last_month,
    count(distinct case when start < @start_last_month then id end) as last_year
from agent_states;





select * from call_attempts;

select * from call_attempts_last_month;

select * from call_attempts_last_year;



select * from dispositions;

select * from dispositions_last_month;

select * from dispositions_last_year;



select * from agent_states;

select * from agent_states_last_month;

select * from agent_states_last_year;




/*

call ultra_integration_uncle.aggregate_last_month_calls;

-- call ultra_integration_uncle.aggregate_last_year_calls;



call ultra_integration_uncle.aggregate_last_month_dispositions;

-- call ultra_integration_uncle.aggregate_last_year_dispositions;



call ultra_integration_uncle.aggregate_last_month_agent_states;

-- call ultra_integration_uncle.aggregate_last_year_agent_states;

*/


