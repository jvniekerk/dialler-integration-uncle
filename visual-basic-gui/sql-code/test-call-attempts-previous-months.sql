

declare
	@start_this_month date = '1 feb 19',
	@start_last_month date = '1 jan 19';


--select top 200 * from (
--	select
--		created_as = getdate(),
--		updated_as = getdate(),
--		date_called = convert(date, ca.DateStart),
--		list_id = ca.ListID,
--		causes_id = ca.CauseID,
--		[type_id] = ca.TypeID,
--		[user_id] = ca.UserID,
--		disposition_code = d.DispositionCode,
--		disposition_description = d.[Description],
--		calls = count(distinct ca.AttemptID),
--		dial_time = sum(ca.DialTime),
--		ivr_time = sum(ca.IVRTime),
--		wait_time = sum(ca.WaitTime),
--		talk_time = sum(ca.TalkTime),
--		wrap_time = sum(ca.WrapTime)
--	from DiallerStagingArea.dbo.CallAttempts ca
--		left join DiallerStagingArea.dbo.Dispositions d on ca.TransactionID = d.TransactionID
--	where
--		DateStart < @start_this_month
--		and
--		DateStart >= @start_last_month
--	group by
--		convert(date, ca.DateStart),
--		ca.ListID,
--		ca.CauseID,
--		ca.TypeID,
--		ca.UserID,
--		d.DispositionCode,
--		d.[Description]
--	) c
--order by NEWID();


select top 10 * from DiallerStagingArea.dbo.CallAttempts;


select
	created_as = getdate(),
	updated_as = getdate(),
	ultra_id = c.UltraID,
	customer_number = c.Number,
	cli_ddi = 'some cli number',
	[start] = c.DateStart,
	[end] = c.DateEnd


	--list_id = c.ListID,
	--causes_id = c.CauseID,
	--[type_id] = c.TypeID,
	--[user_id] = c.UserID,
	--disposition_code = d.DispositionCode,
	--disposition_description = d.[Description],
	--calls = count(distinct c.AttemptID),
	--dial_time = sum(c.DialTime),
	--ivr_time = sum(c.IVRTime),
	--wait_time = sum(c.WaitTime),
	--talk_time = sum(c.TalkTime),
	--wrap_time = sum(c.WrapTime)
from DiallerStagingArea.dbo.CallAttempts c
	left join DiallerStagingArea.dbo.Dispositions d on c.TransactionID = d.TransactionID
where
	DateStart < @start_this_month
	and
	DateStart >= @start_last_month;

