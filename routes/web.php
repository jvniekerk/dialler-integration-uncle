<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/ultra.callbar.popscreen', function () {
    return view('call_bar_pop_screen');
});


Route::get('/callbar', function () {
    return view('ultra_callbar');
});


Route::get('/ultra.callbar', function () {
    return view('callbar/Script Template/ultra_callbar');
});


Route::post('/postajax', 'AjaxController@post');

