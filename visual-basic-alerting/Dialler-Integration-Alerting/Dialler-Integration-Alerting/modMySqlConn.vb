Imports MySql.Data.MySqlClient
Imports System.Data

Module modMySQLConn
    Function mySqlGetConnString() As String
        Dim mySQLconnDetails As Dictionary(Of String, String) = Get_mySql_connection_details()

        Return "server=" & mySQLconnDetails.Item("server") & "; user id=" & mySQLconnDetails.Item("user") & "; password=" & mySQLconnDetails.Item("pw") & "; database=" & mySQLconnDetails.Item("database")
    End Function


    Function mySqlGetDataTable(sql As String) As DataTable
        Dim reader As MySqlDataReader
        Dim mySQLconn As MySqlConnection = New MySqlConnection()
        Dim table As DataTable = New DataTable

        With mySQLconn
            .ConnectionString = mySqlGetConnString()

            .Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql, mySQLconn)

            reader = cmd.ExecuteReader()

            table.Load(reader)

            .Close()

            .Dispose()
        End With


        Return table
    End Function


    Function mySqlGetScalar(sql As String) As String
        Dim id As String
        Dim mySQLconn As MySqlConnection = New MySqlConnection()

        With mySQLconn
            .ConnectionString = mySqlGetConnString()

            .Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql, mySQLconn)

            id = cmd.ExecuteScalar().ToString

            .Close()

            .Dispose()
        End With

        Return id
    End Function


    Sub mySqlPutNoQuery(sql As String)
        Dim mySQLconn As MySqlConnection = New MySqlConnection()

        With mySQLconn
            .ConnectionString = mySqlGetConnString()

            .Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql, mySQLconn)

            cmd.ExecuteNonQuery()

            .Close()

            .Dispose()
        End With
    End Sub
End Module
