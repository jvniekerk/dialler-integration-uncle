Class MainWindow
    Private Sub BtnTest_Click(sender As Object, e As RoutedEventArgs) Handles btnTest.Click
        MsgBox("test start")


        Call Check_integration_app_processes()


        MsgBox("test completed...")
    End Sub


    Private Sub BtnExit_Click(sender As Object, e As RoutedEventArgs) Handles btnExit.Click
        Close()
    End Sub


    Private Sub MainWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Call Check_integration_app_processes()
        'MsgBox("complete")

        Close()
    End Sub


    Private Sub Check_integration_app_processes()
        '' Check the database to see if the integration processes are running
        Try
            Dim run As New csIntegrationProcessRun

            If run.Get_should_run() Then
                If Not run.Is_process_running() Then
                    Dim body As String = "The dialler integration processes do not seem to be running!!" & Environment.NewLine &
                        "Please check the integration database to check process are running as expected."

                    Try
                        Throw New Exception("Dialler integration processes not running!!")
                    Catch ex As Exception
                        writeErrorToDB(ex, ex.Message, 0, body, ex.ToString)
                    End Try
                End If
            End If
        Catch ex As Exception
            writeErrorToDB(ex, "Checking dialler integration processes running failed!! => " & ex.Message, 0)
        End Try


        '' Check if the integration application is running
        Try
            Dim app As New csDiallerIntegrationApp

            If app.Should_run() Then
                If Not app.Get_app_status() Then
                    Dim body As String = "The dialler integration app does not seem to be running!!" & Environment.NewLine &
                        "Please check the server to check the app is running as expected."

                    Try
                        Throw New Exception("Dialler integration application not running!!")
                    Catch ex As Exception
                        writeErrorToDB(ex, ex.Message, 0, body, ex.ToString)
                    End Try
                End If
            End If
        Catch ex As Exception
            writeErrorToDB(ex, "Checking dialler integration app open failed!! => " & ex.Message, 0)
        End Try
    End Sub
End Class
