Public Class csIntegrationProcessRun
    Private process_type As String
    Private process As String
    Private interval As String
    Private intervals As Integer


    '' constructor
    Public Sub New(Optional process_type As String = "data_sync", Optional process As String = "Data sync procedure", Optional interval As String = "minute", Optional intervals As Integer = -15)
        Me.process_type = process_type
        Me.process = process
        Me.interval = interval
        Me.intervals = intervals
    End Sub


    Private Function Get_SQL_query() As String
        Return "
            select count(*) as running from " & Get_mySql_connection_details.Item("database") & ".process_run p 
            where
                p.process_type = '" & process_type & "'
                and
                p.process = '" & process & "'
                and 
                p.completed =1
                and
                p.updated_at >= DATE_ADD(now(), interval " & intervals & " " & interval & ");
                "
    End Function


    Public Function Get_should_run() As Boolean
        Dim times As Dictionary(Of String, DateTime) = getOpenCloseTime()

        times.Item("open") = DateAdd(DateInterval.Minute, -45, times.Item("open"))
        'MsgBox("processes => open: " & times.Item("open").ToString & " close: " & times.Item("close").ToString)
        'MsgBox(Not (Now < times.Item("open") Or Now > times.Item("close")))

        If Now < times.Item("open") Or Now > times.Item("close") Then Return False

        Return True
    End Function


    Public Function Is_process_running() As Boolean
        If mySqlGetScalar(removeDuplicateSpaces(Get_SQL_query)) > 0 Then Return True

        Return False
    End Function
End Class
