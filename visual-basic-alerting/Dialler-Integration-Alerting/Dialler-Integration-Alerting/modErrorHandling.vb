Module modErrorHandling
    Sub writeErrorToDB(ex As Exception, subject As String, batch_id As Integer, Optional request As String = "", Optional response As String = "", Optional sendEmail As Boolean = True)
        Dim sql As String = ""

        Try
            Dim message As String = ex.Message & " => " & ex.StackTrace & " => " & ex.ToString

            subject = subject.Replace("\", "/")
            message = message.Replace("\", "/")
            request = request.Replace("\", "/")
            response = response.Replace("\", "/")


            'Dim timeStamp As String = DateTime.Now.ToString("yyyyMMdd-HHmmss")
            'Dim path As String = "C:\Users\jvniekerk.UNCLEBUCK\Desktop\test-files\test\dialler-integration\error-"

            'saveTextFile(path & timeStamp & "-details.txt",
            '             "SUBJECT => " & Environment.NewLine & subject & Environment.NewLine & Environment.NewLine &
            '             "SEND EMAIL => " & sendEmail & Environment.NewLine & Environment.NewLine &
            '             "MESSAGE => " & Environment.NewLine & message & Environment.NewLine & Environment.NewLine &
            '             "SENT => " & Environment.NewLine & request & Environment.NewLine & Environment.NewLine &
            '             "RESPONSE => " & Environment.NewLine & response)


            subject = removeDuplicateSpaces(subject.Replace("'", "''"))
            message = removeDuplicateSpaces(message.Replace("'", "''"))
            request = removeDuplicateSpaces(request.Replace("'", "''"))
            response = removeDuplicateSpaces(response.Replace("'", "''"))


            sql = "
                insert into " & Get_mySql_connection_details.Item("database") & ".error_log
                (
                    created_at,
                    updated_at,
                    subject,
                    message,
                    sent,
                    response
                ) values
                (
                    now(),
                    now(),
                    '" & subject & "',
                    '" & message & "',
                    '" & request & "',
                    '" & response & "'
                )"

            'saveTextFile(path & timeStamp & "-sql.txt", removeDuplicateSpaces(sql))
            'MsgBox(subject & " => Message: " & ex.Message)


            Try
                mySqlPutNoQuery(sql)

                'MsgBox("sendEmail: " & sendEmail)
                If sendEmail Then sendErrorEmail(request, response, subject, ex)
            Catch exep As Exception
                Dim text As String = "sql => " & sql & Environment.NewLine & "request => " & request

                writeErrorToDB(ex, "Write error to database failed!! => " & exep.Message, batch_id, text, response)
            End Try
        Catch exp As Exception
            writeErrorToDB(exp, "writeErrorToDB failed!!" & exp.Message & " => original exception message => " & subject, 0, request, response)
        End Try
    End Sub


    Sub sendErrorEmail(request As String, response As String, ex_subject As String, ex As Exception)
        Dim subject As String = "Dialler Integration UNCLE error"

        Dim body As String = "Exception subject => " & ex_subject & Environment.NewLine & Environment.NewLine &
            "request => " & request & Environment.NewLine & Environment.NewLine &
            "response => " & response & Environment.NewLine & Environment.NewLine &
            "exception => " & ex.ToString

        Call sendAlertEmail(subject, body, 0)
    End Sub
End Module
