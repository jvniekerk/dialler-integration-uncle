Module modFunctions
    Public Sub saveTextFile(ByVal fileName As String, ByVal data As String)
        If IO.File.Exists(fileName) Then IO.File.Delete(fileName)

        My.Computer.FileSystem.WriteAllText(fileName, data, True)
    End Sub


    Function removeDuplicateSpaces(text As String) As String
        While text.IndexOf("  ") >= 0
            text = text.Replace("  ", " ")
        End While

        Return text
    End Function


    Function getOpenCloseTime() As Dictionary(Of String, DateTime)
        Dim times As New Dictionary(Of String, DateTime)
        Dim open As New DateTime
        Dim close As DateTime

        If Is_weekend() Then
            With Today
                open = New DateTime(.Year, .Month, .Day, 9, 0, 0)
                close = New DateTime(.Year, .Month, .Day, 18, 0, 0)
            End With
        Else
            With Today
                open = New DateTime(.Year, .Month, .Day, 8, 0, 0)
                close = New DateTime(.Year, .Month, .Day, 21, 30, 0)
            End With
        End If

        times.Add("open", open)
        times.Add("close", close)

        Return times
    End Function


    Function Is_weekend() As Boolean
        Dim day_name As DayOfWeek = Date.Today.DayOfWeek

        If day_name = DayOfWeek.Saturday Or day_name = DayOfWeek.Sunday Then Return True

        Return False
    End Function
End Module
