Public Class csDiallerIntegrationApp
    Private app() As Process
    Private app_name As String = "DiallerStagingArea"   '' app app_name


    '' constructor
    Public Sub New(Optional cname As String = "DiallerStagingArea")
        If Len(cname) > 0 Then app_name = cname

        app = Process.GetProcessesByName(app_name)
    End Sub


    Public Function Get_app_status()
        If app.Count() > 0 Then Return True

        Return False
    End Function


    Public Function Should_run() As Boolean
        Dim times As Dictionary(Of String, DateTime) = getOpenCloseTime()

        times.Item("open") = DateAdd(DateInterval.Minute, -30, DateAdd(DateInterval.Hour, -1, times.Item("open")))
        'MsgBox("app => open: " & times.Item("open").ToString & " close: " & times.Item("close").ToString)
        'MsgBox(Not (Now < times.Item("open") Or Now > times.Item("close")))

        If Now < times.Item("open") Or Now > times.Item("close") Then Return False

        Return True
    End Function
End Class



'' https://social.msdn.microsoft.com/Forums/vstudio/en-US/77153d31-a23f-48d0-a4c4-2a3867370af6/vbnet-check-to-see-if-a-process-is-running?forum=vbgeneral
'' https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.process?redirectedfrom=MSDN&view=netframework-4.7.2
