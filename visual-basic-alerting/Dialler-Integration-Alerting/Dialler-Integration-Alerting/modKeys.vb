Module modKeys
    Function Get_mySql_connection_details() As Dictionary(Of String, String)
        Dim connDetails As Dictionary(Of String, String) = New Dictionary(Of String, String) From {
            {"server", "192.168.10.10"},
            {"user", "homestead"},
            {"pw", "secret"},
            {"database", "ultra_integration_uncle"}
        }

        Return connDetails
    End Function


    Function Get_alert_email_details() As Dictionary(Of String, String)
        Dim email_details As Dictionary(Of String, String) = New Dictionary(Of String, String) From {
            {"account_email", "dialler.integration.unclebuck@gmail.com"},
            {"account_pw", "Unc13Buck"},
            {"account_host", "smtp.gmail.com"},
            {"account_port", "587"},
            {"account_EnableSsl", True},
            {"account_UseDefaultCredentials", False},
            {"default_alert_email", "jvniekerk@unclebuck.co.uk"}
        }
        '{"default_alert_email", "oncall@unclebuck.co.uk"},

        Return email_details
    End Function
End Module
