<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateDataChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_data_change', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedSmallInteger('list_id');
            $table->unsignedBigInteger('batch_id');
            $table->unsignedInteger('row_id')->nullable();
            $table->smallInteger('from_lms_status')->nullable();
            $table->smallInteger('to_lms_status')->nullable();
            $table->smallInteger('from_ultra_status')->nullable();
            $table->smallInteger('to_ultra_status')->nullable();
            $table->string('from_phone_number1')->nullable();
            $table->string('to_phone_number1')->nullable();
            $table->string('from_phone_number2')->nullable();
            $table->string('to_phone_number2')->nullable();
            $table->string('from_phone_number3')->nullable();
            $table->string('to_phone_number3')->nullable();
            $table->float('from_balance')->nullable();
            $table->float('to_balance')->nullable();
            $table->string('from_first_name')->nullable();
            $table->string('to_first_name')->nullable();
            $table->string('from_surname')->nullable();
            $table->string('to_surname')->nullable();
            $table->string('from_title')->nullable();
            $table->string('to_title')->nullable();
            $table->date('from_dob')->nullable();
            $table->date('to_dob')->nullable();
            $table->string('from_unit')->nullable();
            $table->string('to_unit')->nullable();
            $table->string('from_house_number')->nullable();
            $table->string('to_house_number')->nullable();
            $table->string('from_street')->nullable();
            $table->string('to_street')->nullable();
            $table->string('from_postcode')->nullable();
            $table->string('to_postcode')->nullable();
            $table->string('from_dial_purpose')->nullable();
            $table->string('to_dial_purpose')->nullable();
            $table->unsignedSmallInteger('from_priority')->nullable();
            $table->unsignedSmallInteger('to_priority')->nullable();
            $table->string('update_type');
            $table->unsignedInteger('loanno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_data_change');
    }
}
