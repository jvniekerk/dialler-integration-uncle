<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrentAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('current_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedSmallInteger('list_id');
            $table->unsignedInteger('row_id');
            $table->smallInteger('lms_status');
            $table->smallInteger('ultra_status');
            $table->string('cli')->nullable();
            $table->string('phone_number1');
            $table->string('phone_number2');
            $table->string('phone_number3');
            $table->unsignedInteger('loanno');
            $table->float('balance');
            $table->string('first_name');
            $table->string('surname');
            $table->string('title');
            $table->date('dob');
            $table->string('unit');
            $table->string('house_number');
            $table->string('street');
            $table->string('postcode');
            $table->string('dial_purpose');
            $table->unsignedSmallInteger('priority');
            $table->index('list_id');
            $table->index('row_id');
            $table->index('lms_status');
            $table->index('loanno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('current_accounts');
    }
}
