<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentStatesLastYearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_states_last_year', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('month');
            $table->unsignedSmallInteger('state');
            $table->boolean('is_call');
            $table->unsignedSmallInteger('user_id');
            $table->unsignedSmallInteger('team_id');
            $table->unsignedSmallInteger('chain_id');
            $table->unsignedSmallInteger('list_id');
            $table->unsignedInteger('changes');
            $table->unsignedBigInteger('duration');
            $table->unique(array(
                               'month',
                               'state',
                               'is_call',
                               'user_id',
                               'team_id',
                               'chain_id',
                               'list_id'
                           ), 'idx_unique_aggregate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_states_last_year');
    }
}
