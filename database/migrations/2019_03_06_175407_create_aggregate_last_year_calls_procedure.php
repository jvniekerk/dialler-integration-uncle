<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAggregateLastYearCallsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
drop procedure if exists ultra_integration_uncle.aggregate_last_year_calls;
        
CREATE PROCEDURE ultra_integration_uncle.aggregate_last_year_calls(
)
begin
    declare start_this_month date;
    declare start_last_month date;
    declare start_last_year date;

    declare calls_last_month int;
    declare calls_prior_last_month int;
    declare calls_this_month int;


    set start_this_month = concat(extract(year from CURRENT_DATE), '/', extract(month from CURRENT_DATE), '/01');
    set start_last_month = date_add(start_this_month, interval -1 month);
    set start_last_year = date_add(start_last_month, interval -1 year);


    set calls_last_month =  (select count(*) from call_attempts_last_month where date_called >= start_last_month);
    set calls_prior_last_month =  (select count(*) from call_attempts_last_month where date_called < start_last_month);
    set calls_this_month = (select count(*) from call_attempts where start >= start_this_month);


    delete from call_attempts_last_year where month_called_start < start_last_year;

    
    -- calls last month && calls prior last month 
    if (calls_last_month and calls_prior_last_month and calls_this_month) then
        /*
            if calls last month && calls prior last month 
            => aggregate zero talk calls prior last month
            => insert ignore into last year
            => delete calls prior last month
            => aggregate talk calls from call_attempts prior last month in last year
            => insert ignore into last year
            => update last year talk calls
        */

        -- => aggregate zero talk calls prior last month
        drop table if exists zero_talk_calls_prior_last_month;


        create table zero_talk_calls_prior_last_month
        (
            month_called_start date,
            list_id int,
            causes_id int,
            type_id int,
            user_id int,
            disposition_code int,
            disposition_description varchar(200),
            calls int,
            dial_time int,
            ivr_time int,
            wait_time int,
            talk_time int,
            wrap_time int
        );



        insert into zero_talk_calls_prior_last_month
            select 
                concat(extract(year from date_called), '/', extract(month from date_called), '/01'),
                list_id,
                causes_id,
                type_id,
                user_id,
                disposition_code,
                disposition_description,
                sum(calls),
                sum(dial_time),
                sum(ivr_time),
                sum(wait_time),
                sum(talk_time),
                sum(wrap_time)
            from call_attempts_last_month
            where 
                date_called < start_last_month
                and 
                talk_time =0
            group by 
                concat(extract(year from date_called), '/', extract(month from date_called), '/01'),
                list_id,
                causes_id,
                type_id,
                user_id,
                disposition_code,
                disposition_description
            ;



        -- => insert ignore into last year
        insert ignore into call_attempts_last_year
        (
            created_at,
            updated_at,
            month_called_start,
            list_id,
            causes_id,
            type_id,
            user_id,
            disposition_code,
            disposition_description,
            calls,
            dial_time,
            ivr_time,
            wait_time,
            talk_time,
            wrap_time
        )
        select now(), now(), z.* from zero_talk_calls_prior_last_month z;
        
        
        drop table if exists zero_talk_calls_prior_last_month;



        -- => delete calls prior last month
        delete from call_attempts_last_month where date_called < start_last_month;



        -- => aggregate talk calls from call_attempts prior last month
        drop table if exists talk_calls_last_year;


        create table talk_calls_last_year
        (
            month_called_start date,
            list_id int,
            causes_id int,
            type_id int,
            user_id int,
            disposition_code int,
            disposition_description varchar(200),
            calls int,
            dial_time int,
            ivr_time int,
            wait_time int,
            talk_time int,
            wrap_time int
        );


        insert into talk_calls_last_year
            select
                concat(extract(year from c.start), '/', extract(month from c.start), '/01') as month_called_start,
                c.list_id,
                c.causes_id,
                c.type_id,
                c.user_id,
                ifnull(d.disposition_code, 0) as disposition_code,
                ifnull(d.description, '') as disposition_description,
                count(distinct c.id) as calls,
                sum(c.dial_time) as dial_time,
                sum(c.ivr_time) as ivr_time,
                sum(c.call_wait_time) as wait_time,
                sum(c.talk) as talk_time,
                sum(c.wrap) as wrap_time
            from ultra_integration_uncle.call_attempts c 
                left join ultra_integration_uncle.dispositions d on c.transaction_id = d.transaction_id
            where
                c.start >= start_last_year
                and
                c.start < start_this_month
                and 
                c.talk >0
            group by 
                concat(extract(year from c.start), '/', extract(month from c.start), '/01'),
                c.list_id,
                c.causes_id,
                c.type_id,
                c.user_id,
                ifnull(d.disposition_code, 0),
                ifnull(d.description, '')
                ;


        -- => insert ignore into last year
        insert ignore into call_attempts_last_year
        (
            created_at,
            updated_at,
            month_called_start,
            list_id,
            causes_id,
            type_id,
            user_id,
            disposition_code,
            disposition_description,
            calls,
            dial_time,
            ivr_time,
            wait_time,
            talk_time,
            wrap_time
        )
        select now(), now(), z.* from talk_calls_last_year z;


        -- => update last year talk calls
        update call_attempts_last_year ca 
            join talk_calls_last_year ly on ca.month_called_start = ly.month_called_start
                and ca.list_id = ly.list_id
                and ca.causes_id = ly.causes_id
                and ca.type_id = ly.type_id
                and ca.user_id = ly.user_id
                and ca.disposition_code = ly.disposition_code
                and ca.disposition_description = ly.disposition_description
        set
            ca.updated_at = now(),
            ca.calls = ly.calls,
            ca.dial_time = ly.dial_time,
            ca.ivr_time = ly.ivr_time,
            ca.wait_time = ly.wait_time,
            ca.talk_time = ly.talk_time,
            ca.wrap_time = ly.wrap_time
        where 
            ca.calls != ly.calls or
            ca.dial_time != ly.dial_time or
            ca.ivr_time != ly.ivr_time or
            ca.wait_time != ly.wait_time or
            ca.talk_time != ly.talk_time or
            ca.wrap_time != ly.wrap_time
        ;
        
        
        drop table if exists talk_calls_last_year;
    end if;



    drop table if exists zero_talk_calls_prior_last_month;

    drop table if exists talk_calls_last_year;
end";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS aggregate_last_year_calls");
    }
}
