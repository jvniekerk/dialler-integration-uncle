<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateGetUpdateDataProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
DROP PROCEDURE IF EXISTS get_update_data;

CREATE PROCEDURE ultra_integration_uncle.get_update_data(
	json_string json,
    max_count int,
    listId int,
    list_name varchar(100),
    batch_id int
)
BEGIN
    declare counter int;
    declare path varchar(36);
    
    declare mobile_number varchar(50);
    declare home_number varchar(50);
    declare work_number varchar(50);
    declare balance varchar(50);
    declare first_name varchar(50);
    declare surname varchar(50);
    declare title varchar(50);
    declare dob varchar(50);
    declare unit varchar(50);
    declare house_number varchar(50);
    declare street varchar(50);
    declare postcode varchar(50);
    declare dial_purpose varchar(50);
    declare priority varchar(50);
    declare loanno varchar(50);
    
    
    drop table if exists json_data;
    
    create table json_data
    (
        mobile_number varchar(20),
        home_number varchar(20),
        work_number varchar(20),
        balance float,
        first_name varchar(100),
        surname varchar(100),
        title varchar(20),
        dob date,
        unit varchar(20),
        house_number varchar(20),
        street varchar(200),
        postcode varchar(20),
        dial_purpose varchar(100),
        priority int,
        loanno int
    );
    

    SET counter = 0;
    
    WHILE counter < max_count DO
        set path = concat('$.', list_name, '[', counter, '].');
        
        set mobile_number = concat(path, 'mobile_number');
        set home_number = concat(path, 'home_number');
        set work_number = concat(path, 'work_number');
        set loanno = concat(path, 'loanno');
        set balance = concat(path, 'balance');
        set first_name = concat(path, 'first_name');
        set surname = concat(path, 'surname');
        set title = concat(path, 'title');
        set dob = concat(path, 'dob');
        set unit = concat(path, 'unit');
        set house_number = concat(path, 'house_number');
        set street = concat(path, 'street');
        set postcode = concat(path, 'postcode');
        set dial_purpose = concat(path, 'dial_purpose');
        set priority = concat(path, 'priority');
        set loanno = concat(path, 'loanno');

        
        insert into json_data
            select
                replace(json_extract(json_string, mobile_number), '\"', '') as mobile_number,
                replace(json_extract(json_string, home_number), '\"', '') as home_number,
                replace(json_extract(json_string, work_number), '\"', '') as work_number,
                cast(round(replace(json_extract(json_string, balance), '\"', ''), 2) as decimal(10, 2)) as balance,
                replace(json_extract(json_string, first_name), '\"', '') as first_name,
                replace(json_extract(json_string, surname), '\"', '') as surname,
                replace(json_extract(json_string, title), '\"', '') as title,
                replace(json_extract(json_string, dob), '\"', '') as dob,
                replace(json_extract(json_string, unit), '\"', '') as unit,
                replace(json_extract(json_string, house_number), '\"', '') as house_number,
                replace(json_extract(json_string, street), '\"', '') as street,
                replace(json_extract(json_string, postcode), '\"', '') as postcode,
                replace(json_extract(json_string, dial_purpose), '\"', '') as dial_purpose,
                replace(json_extract(json_string, priority), '\"', '') as priority,
                replace(json_extract(json_string, loanno), '\"', '') as loanno;
        
        SET  counter = counter + 1; 
    END WHILE;
    
    
    
    drop table if exists return_table;
    
    create table return_table
    (
        row_id int,
        cli varchar(20),
        from_lms_status int,
        to_lms_status int,
        from_ultra_status int,
        to_ultra_status int,
        from_phone_number1 varchar(20),
        to_phone_number1 varchar(20),
        from_phone_number2 varchar(20),
        to_phone_number2 varchar(20),
        from_phone_number3 varchar(20),
        to_phone_number3 varchar(20),
        from_balance float,
        to_balance float,
        from_first_name varchar(100),
        to_first_name varchar(100),
        from_surname varchar(100),
        to_surname varchar(100),
        from_title varchar(20),
        to_title varchar(20),
        from_dob date,
        to_dob date,
        from_unit varchar(20),
        to_unit varchar(20),
        from_house_number varchar(20),
        to_house_number varchar(20),
        from_street varchar(200),
        to_street varchar(200),
        from_postcode varchar(20),
        to_postcode varchar(20),
        from_dial_purpose varchar(100),
        to_dial_purpose varchar(100),
        from_priority int,
        to_priority int,
        update_type varchar(100),
        loanno int
    );


    call max_attempts;

    call suppress_dispositions;



    if list_name like '%ivr%' then
        -- New Uploads
        insert into return_table
            select
                null as row_id,
                '' as cli,
                ca.lms_status as from_lms_status,
                2 as to_lms_status,
                ca.ultra_status as from_ultra_status,
                0 as to_ultra_status,
                ca.phone_number1,
                jt.mobile_number,
                ca.phone_number2,
                jt.home_number,
                ca.phone_number3,
                jt.work_number,
                ca.balance,
                jt.balance,
                ca.first_name,
                jt.first_name,
                ca.surname,
                jt.surname,
                ca.title,
                jt.title,
                ca.dob,
                jt.dob,
                ca.unit,
                jt.unit,
                ca.house_number,
                jt.house_number,
                ca.street,
                jt.street,
                ca.postcode,
                jt.postcode,
                ca.dial_purpose,
                jt.dial_purpose,
                ca.priority,
                jt.priority,
                'NewUpload' as update_type,
                jt.loanno
            from json_data jt
                left join ultra_integration_uncle.current_accounts ca on jt.loanno = ca.loanno and ca.list_id = listId
                left join return_table rt on jt.loanno = rt.loanno
            where
                ca.loanno is null
                and
                rt.loanno is null
            order by jt.loanno;
    
    
        
        -- Reactivate accounts
        insert into return_table
            select
                ca.row_id,
                ca.cli,
                ca.lms_status as from_lms_status,
                2 as to_lms_status,
                ca.ultra_status as from_ultra_status,
                0 as to_ultra_status,
                ca.phone_number1,
                jt.mobile_number,
                ca.phone_number2,
                jt.home_number,
                ca.phone_number3,
                jt.work_number,
                ca.balance,
                jt.balance,
                ca.first_name,
                jt.first_name,
                ca.surname,
                jt.surname,
                ca.title,
                jt.title,
                ca.dob,
                jt.dob,
                ca.unit,
                jt.unit,
                ca.house_number,
                jt.house_number,
                ca.street,
                jt.street,
                ca.postcode,
                jt.postcode,
                ca.dial_purpose,
                jt.dial_purpose,
                ca.priority,
                jt.priority,
                'Reactivate' as update_type,
                jt.loanno
            from ultra_integration_uncle.current_accounts ca
                join json_data jt on jt.loanno = ca.loanno and ca.list_id = listId and ca.ultra_status =4
                left join return_table rt on jt.loanno = rt.loanno
            where rt.loanno is null
            order by ca.loanno;
            
        
        
        -- Alter accounts
        insert into return_table
            select
                ca.row_id,
                ca.cli,
                ca.lms_status as from_lms_status,
                2 as to_lms_status,
                ca.ultra_status as from_ultra_status,
                0 as to_ultra_status,
                ca.phone_number1,
                jt.mobile_number,
                ca.phone_number2,
                jt.home_number,
                ca.phone_number3,
                jt.work_number,
                ca.balance,
                jt.balance,
                ca.first_name,
                jt.first_name,
                ca.surname,
                jt.surname,
                ca.title,
                jt.title,
                ca.dob,
                jt.dob,
                ca.unit,
                jt.unit,
                ca.house_number,
                jt.house_number,
                ca.street,
                jt.street,
                ca.postcode,
                jt.postcode,
                ca.dial_purpose,
                jt.dial_purpose,
                ca.priority,
                jt.priority,
                'Alter' as update_type,
                jt.loanno
            from json_data jt
                join ultra_integration_uncle.current_accounts ca on jt.loanno = ca.loanno and ca.list_id = listId
                    and ca.ultra_status <>4
                left join return_table rt on jt.loanno = rt.loanno
            where
                (
                    jt.mobile_number <> ca.phone_number1 or
                    jt.home_number <> ca.phone_number2 or
                    jt.work_number <> ca.phone_number3 or
                    jt.balance <> ca.balance or
                    jt.first_name <> ca.first_name or
                    jt.surname <> ca.surname or
                    jt.title <> ca.title or
                    jt.dob <> ca.dob or
                    jt.unit <> ca.unit or
                    jt.house_number <> ca.house_number or
                    jt.street <> ca.street or
                    jt.postcode <> ca.postcode or
                    jt.dial_purpose <> ca.dial_purpose or
                    jt.priority <> ca.priority
                )
                and
                rt.loanno is null
            order by ca.loanno;
    else    -- list_name not like '%ivr%'
        -- Suppress accounts
        insert into return_table
            select
                ca.row_id,
                ca.cli,
                ca.lms_status as from_lms_status,
                4 as to_lms_status,
                ca.ultra_status as from_ultra_status,
                4 as to_ultra_status,
                ca.phone_number1,
                ca.phone_number1,
                ca.phone_number2,
                ca.phone_number2,
                ca.phone_number3,
                ca.phone_number3,
                ca.balance,
                ca.balance,
                ca.first_name,
                ca.first_name,
                ca.surname,
                ca.surname,
                ca.title,
                ca.title,
                ca.dob,
                ca.dob,
                ca.unit,
                ca.unit,
                ca.house_number,
                ca.house_number,
                ca.street,
                ca.street,
                ca.postcode,
                ca.postcode,
                ca.dial_purpose,
                ca.dial_purpose,
                ca.priority,
                ca.priority,
                'Suppress' as update_type,
                ca.loanno
            from ultra_integration_uncle.current_accounts ca
                join ultra_integration_uncle.suppress s on s.loanno = ca.loanno and ca.list_id = listId and ca.ultra_status <>4
                    and s.created_at >= CURRENT_DATE
            where ca.list_id = listId
            order by
                ca.row_id
                ,
                ca.loanno;
        
        
        
        -- New Uploads
        insert into return_table
            select
                null as row_id,
                '' as cli,
                ca.lms_status as from_lms_status,
                2 as to_lms_status,
                ca.ultra_status as from_ultra_status,
                0 as to_ultra_status,
                ca.phone_number1,
                jt.mobile_number,
                ca.phone_number2,
                jt.home_number,
                ca.phone_number3,
                jt.work_number,
                ca.balance,
                jt.balance,
                ca.first_name,
                jt.first_name,
                ca.surname,
                jt.surname,
                ca.title,
                jt.title,
                ca.dob,
                jt.dob,
                ca.unit,
                jt.unit,
                ca.house_number,
                jt.house_number,
                ca.street,
                jt.street,
                ca.postcode,
                jt.postcode,
                ca.dial_purpose,
                jt.dial_purpose,
                ca.priority,
                jt.priority,
                'NewUpload' as update_type,
                jt.loanno
            from json_data jt
                left join ultra_integration_uncle.current_accounts ca on jt.loanno = ca.loanno and ca.list_id = listId
                left join ultra_integration_uncle.suppress s on jt.loanno = s.loanno and s.created_at >= CURRENT_DATE
                left join ultra_integration_uncle.max_attempts ma on jt.loanno = ma.loanno and ma.created_at >= CURRENT_DATE
                left join return_table rt on jt.loanno = rt.loanno
            where
                ca.loanno is null
                and
                ma.loanno is null
                and
                s.loanno is null
                and
                rt.loanno is null
            order by jt.loanno;
    
    
        
        -- Reactivate accounts
        insert into return_table
            select
                ca.row_id,
                ca.cli,
                ca.lms_status as from_lms_status,
                2 as to_lms_status,
                ca.ultra_status as from_ultra_status,
                0 as to_ultra_status,
                ca.phone_number1,
                jt.mobile_number,
                ca.phone_number2,
                jt.home_number,
                ca.phone_number3,
                jt.work_number,
                ca.balance,
                jt.balance,
                ca.first_name,
                jt.first_name,
                ca.surname,
                jt.surname,
                ca.title,
                jt.title,
                ca.dob,
                jt.dob,
                ca.unit,
                jt.unit,
                ca.house_number,
                jt.house_number,
                ca.street,
                jt.street,
                ca.postcode,
                jt.postcode,
                ca.dial_purpose,
                jt.dial_purpose,
                ca.priority,
                jt.priority,
                'Reactivate' as update_type,
                jt.loanno
            from ultra_integration_uncle.current_accounts ca
                join json_data jt on jt.loanno = ca.loanno and ca.list_id = listId and ca.ultra_status =4
                left join ultra_integration_uncle.suppress s on jt.loanno = s.loanno and s.created_at >= CURRENT_DATE
                left join ultra_integration_uncle.max_attempts ma on jt.loanno = ma.loanno and ma.created_at >= CURRENT_DATE
                left join return_table rt on jt.loanno = rt.loanno
            where
                ma.loanno is null
                and
                s.loanno is null
                and
                rt.loanno is null
            order by ca.loanno;
            
        
        
        -- Alter accounts
        insert into return_table
            select
                ca.row_id,
                ca.cli,
                ca.lms_status as from_lms_status,
                2 as to_lms_status,
                ca.ultra_status as from_ultra_status,
                0 as to_ultra_status,
                ca.phone_number1,
                jt.mobile_number,
                ca.phone_number2,
                jt.home_number,
                ca.phone_number3,
                jt.work_number,
                ca.balance,
                jt.balance,
                ca.first_name,
                jt.first_name,
                ca.surname,
                jt.surname,
                ca.title,
                jt.title,
                ca.dob,
                jt.dob,
                ca.unit,
                jt.unit,
                ca.house_number,
                jt.house_number,
                ca.street,
                jt.street,
                ca.postcode,
                jt.postcode,
                ca.dial_purpose,
                jt.dial_purpose,
                ca.priority,
                jt.priority,
                'Alter' as update_type,
                jt.loanno
            from json_data jt
                join ultra_integration_uncle.current_accounts ca on jt.loanno = ca.loanno and ca.list_id = listId
                    and ca.ultra_status <>4
                left join ultra_integration_uncle.suppress s on jt.loanno = s.loanno and s.created_at >= CURRENT_DATE
                left join ultra_integration_uncle.max_attempts ma on jt.loanno = ma.loanno and ma.created_at >= CURRENT_DATE
                left join return_table rt on jt.loanno = rt.loanno
            where
                (
                    jt.mobile_number <> ca.phone_number1 or
                    jt.home_number <> ca.phone_number2 or
                    jt.work_number <> ca.phone_number3 or
                    jt.balance <> ca.balance or
                    jt.first_name <> ca.first_name or
                    jt.surname <> ca.surname or
                    jt.title <> ca.title or
                    jt.dob <> ca.dob or
                    jt.unit <> ca.unit or
                    jt.house_number <> ca.house_number or
                    jt.street <> ca.street or
                    jt.postcode <> ca.postcode or
                    jt.dial_purpose <> ca.dial_purpose or
                    jt.priority <> ca.priority
                )
                and
                ma.loanno is null
                and
                s.loanno is null
                and
                rt.loanno is null
            order by ca.loanno;
    end if;
    


    -- Deactivate accounts
    insert into return_table
        select
            ca.row_id,
            ca.cli,
            ca.lms_status as from_lms_status,
            4 as to_lms_status,
            ca.ultra_status as from_ultra_status,
            4 as to_ultra_status,
            ca.phone_number1,
            ca.phone_number1,
            ca.phone_number2,
            ca.phone_number2,
            ca.phone_number3,
            ca.phone_number3,
            ca.balance,
            ca.balance,
            ca.first_name,
            ca.first_name,
            ca.surname,
            ca.surname,
            ca.title,
            ca.title,
            ca.dob,
            ca.dob,
            ca.unit,
            ca.unit,
            ca.house_number,
            ca.house_number,
            ca.street,
            ca.street,
            ca.postcode,
            ca.postcode,
            ca.dial_purpose,
            ca.dial_purpose,
            ca.priority,
            ca.priority,
            'Deactivate' as update_type,
            ca.loanno
        from ultra_integration_uncle.current_accounts ca
            left join json_data jt on jt.loanno = ca.loanno and ca.list_id = listId and ca.ultra_status <>4
        where 
            jt.loanno is null
            and
            ca.list_id = listId
        order by ca.loanno;
    
    
    drop table if exists json_data;



    insert into update_data_change
    (
        created_at,
        updated_at,
        list_id,
        batch_id,   
        row_id,
        from_lms_status,
        to_lms_status,
        from_ultra_status,
        to_ultra_status,
        from_phone_number1,
        to_phone_number1,
        from_phone_number2,
        to_phone_number2,
        from_phone_number3,
        to_phone_number3,
        from_balance,
        to_balance,
        from_first_name,
        to_first_name,
        from_surname,
        to_surname,
        from_title,
        to_title,
        from_dob,
        to_dob,
        from_unit,
        to_unit,
        from_house_number,
        to_house_number,
        from_street,
        to_street,
        from_postcode,
        to_postcode,
        from_dial_purpose,
        to_dial_purpose,
        from_priority,
        to_priority,
        update_type,
        loanno
    )
    select
        now() as created_at,
        now() as updated_at,
        listId as list_id,
        batch_id,
        rt.row_id,
        rt.from_lms_status,
        rt.to_lms_status,
        rt.from_ultra_status,
        rt.to_ultra_status,
        rt.from_phone_number1,
        rt.to_phone_number1,
        rt.from_phone_number2,
        rt.to_phone_number2,
        rt.from_phone_number3,
        rt.to_phone_number3,
        rt.from_balance,
        rt.to_balance,
        rt.from_first_name,
        rt.to_first_name,
        rt.from_surname,
        rt.to_surname,
        rt.from_title,
        rt.to_title,
        rt.from_dob,
        rt.to_dob,
        rt.from_unit,
        rt.to_unit,
        rt.from_house_number,
        rt.to_house_number,
        rt.from_street,
        rt.to_street,
        rt.from_postcode,
        rt.to_postcode,
        rt.from_dial_purpose,
        rt.to_dial_purpose,
        rt.from_priority,
        rt.to_priority,
        rt.update_type,
        rt.loanno
    from return_table rt
    order by rt.row_id, rt.loanno;

    
    select
        row_id,
        cli,
        left(ifnull(to_phone_number1, from_phone_number1), 20) as phone_number1,
        left(ifnull(to_phone_number2, from_phone_number2), 20) as phone_number2,
        left(ifnull(to_phone_number3, from_phone_number3), 20) as phone_number3,
        ifnull(to_balance, from_balance) as balance,
        left(ifnull(to_first_name, from_first_name), 100) as first_name,
        left(ifnull(to_surname, from_surname), 100) as surname,
        left(ifnull(to_title, from_title), 20) as title,
        ifnull(to_dob, from_dob) as dob,
        left(ifnull(to_unit, from_unit), 20) as unit,
        left(ifnull(to_house_number, from_house_number), 20) as house_number,
        left(ifnull(to_street, from_street), 200) as street,
        left(ifnull(to_postcode, from_postcode), 20) as postcode,
        left(ifnull(to_dial_purpose, from_dial_purpose), 100) as dial_purpose,
        ifnull(to_priority, from_priority) as priority,
        left(rt.update_type, 100) as update_type,
        rt.loanno    
    from return_table rt
    order by rt.row_id, rt.loanno;

    
    drop table if exists return_table;
END";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS get_update_data");
    }
}
