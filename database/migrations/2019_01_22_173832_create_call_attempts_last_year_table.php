<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallAttemptsLastYearTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('call_attempts_last_year', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('month_called_start');
            $table->unsignedSmallInteger('list_id');
            $table->SmallInteger('causes_id');
            $table->unsignedSmallInteger('type_id');
            $table->unsignedSmallInteger('user_id');
            $table->unsignedSmallInteger('disposition_code');
            $table->string('disposition_description');
            $table->unsignedSmallInteger('calls');
            $table->unsignedSmallInteger('dial_time');
            $table->unsignedSmallInteger('ivr_time');
            $table->unsignedSmallInteger('wait_time');
            $table->unsignedSmallInteger('talk_time');
            $table->unsignedSmallInteger('wrap_time');
            $table->unique(array(
                               'month_called_start',
                               'list_id',
                               'causes_id',
                               'type_id',
                               'user_id',
                               'disposition_code',
                               'disposition_description'
                           ), 'idx_unique_aggregate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('call_attempts_last_year');
    }
}
