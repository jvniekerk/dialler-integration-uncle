<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataDownloadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_download', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('download_type');
            $table->unsignedBigInteger('batch_id');
            $table->longtext('request');
            $table->longtext('response');
            $table->index('download_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_download');
    }
}
