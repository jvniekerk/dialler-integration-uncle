<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_states', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedBigInteger('ultra_id')->unique();
            $table->unsignedSmallInteger('state');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedSmallInteger('user_id');
            $table->unsignedSmallInteger('team_id');
            $table->unsignedSmallInteger('chain_id');
            $table->unsignedSmallInteger('list_id');
            $table->string('station');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_states');
    }
}
