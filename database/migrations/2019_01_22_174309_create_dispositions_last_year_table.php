<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositionsLastYearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositions_last_year', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('month_created');
            $table->unsignedSmallInteger('disposition_code');
            $table->string('description');
            $table->unsignedSmallInteger('list_id');
            $table->unsignedSmallInteger('campaign_id');
            $table->unsignedSmallInteger('user_id');
            $table->unsignedInteger('calls');
            $table->unique(array(
                               'month_created',
                               'disposition_code',
                               'description',
                               'list_id',
                               'campaign_id',
                               'user_id'
                           ), 'idx_unique_aggregate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositions_last_year');
    }
}
