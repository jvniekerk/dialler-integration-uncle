<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAggregateLastMonthDispositionsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
drop procedure if exists ultra_integration_uncle.aggregate_last_month_dispositions;
        
CREATE PROCEDURE ultra_integration_uncle.aggregate_last_month_dispositions(
)
begin
    declare dispositions_this_month int;
    declare dispositions_last_year_attempts int;
    
    declare start_this_month date;
    declare start_last_month date;   
    declare start_last_year date;    

    set start_this_month = concat(extract(year from CURRENT_DATE), '/', extract(month from CURRENT_DATE), '/01');
    set start_last_month = date_add(start_this_month, interval -1 month);
    set start_last_year = date_add(start_last_month, interval -1 month);


    set dispositions_this_month = (select count(*) from dispositions where date_created >= start_this_month);

    set dispositions_last_year_attempts = (select count(*) from dispositions d where d.date_created < start_this_month);



    if (dispositions_this_month and dispositions_last_year_attempts) then
        -- Aggregate and insert into dispositions_last_month all dispositions data for last month
        insert ignore into dispositions_last_month
        (
            created_at,
            updated_at,
            date_created,
            disposition_code,
            description,
            list_id,
            campaign_id,
            user_id,
            calls
        )
        select 
            now(), now(),
            cast(date_created as date),
            disposition_code,
            description,
            list_id,
            campaign_id,
            user_id,
            count(distinct d.ultra_id) as calls
        from dispositions d
        where 
            d.date_created >= start_last_year
            and
            d.date_created < start_this_month
        group by 
            cast(date_created as date),
            disposition_code,
            description,
            list_id,
            campaign_id,
            user_id
            ;


        -- Delete old disposition data except what is being kept on call_attempts table
        drop table if exists transaction_ids_to_keep;
    
        create table transaction_ids_to_keep ( transaction_id bigint );

        insert into transaction_ids_to_keep
            select distinct c.transaction_id from call_attempts c where c.start < start_this_month;


        delete d from dispositions d
            left join transaction_ids_to_keep t on d.transaction_id = t.transaction_id
        where t.transaction_id is null and d.date_created < start_this_month
            ;


        call aggregate_last_year_dispositions;
    end if;


    
    drop table if exists transaction_ids_to_keep;
end";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS aggregate_last_month_dispositions");
    }
}
