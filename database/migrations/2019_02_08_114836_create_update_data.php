<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_data', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedSmallInteger('list_id');
            $table->string('update_type');
            $table->unsignedBigInteger('batch_id');
            $table->unsignedBigInteger('sub_id');
            $table->longText('request');
            $table->longText('response');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_data');
    }
}
