<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallAttemptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedBigInteger('ultra_id')->unique();
            $table->string('customer_number');
            $table->string('cli_ddi');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->SmallInteger('causes_id');
            $table->unsignedSmallInteger('type_id');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedBigInteger('event_source');
            $table->string('client_ref');
            $table->unsignedSmallInteger('user_id');
            $table->unsignedSmallInteger('team_id');
            $table->unsignedInteger('row_id');
            $table->unsignedSmallInteger('list_id');
            $table->unsignedSmallInteger('campaign_id');
            $table->unsignedSmallInteger('initial_service_id');
            $table->unsignedSmallInteger('final_service_id');
            $table->unsignedSmallInteger('dial_time');
            $table->unsignedSmallInteger('ivr_time');
            $table->unsignedSmallInteger('call_wait_time');
            $table->unsignedSmallInteger('talk');
            $table->unsignedSmallInteger('wrap');
            $table->string('station');
            $table->unsignedInteger('loanno');
            $table->index('transaction_id');
            $table->index('list_id');
            $table->index('row_id');
            $table->index('loanno');
            $table->index('start');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_call_attempts');
    }
}
