<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('date_created');
            $table->unsignedSmallInteger('disposition_code');
            $table->string('description');
            $table->unsignedBigInteger('transaction_id');
            $table->unsignedSmallInteger('list_id');
            $table->unsignedInteger('row_id');
            $table->unsignedInteger('loanno');
            $table->unsignedSmallInteger('campaign_id');
            $table->unsignedSmallInteger('user_id');
            $table->unsignedBigInteger('ultra_id')->unique();
            $table->index('disposition_code');
            $table->index('date_created');
            $table->index('transaction_id');
            $table->index('list_id');
            $table->index('row_id');
            $table->index('loanno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositions');
    }
}
