<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAggregateLastMonthAgentStatesProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
drop procedure if exists ultra_integration_uncle.aggregate_last_month_agent_states;
        
CREATE PROCEDURE ultra_integration_uncle.aggregate_last_month_agent_states(
)
begin
    declare agent_states_this_month int;
    declare agent_states_changes_last_year int;
    
    declare start_this_month date;
    declare start_last_month date;    

    set start_this_month = concat(extract(year from CURRENT_DATE), '/', extract(month from CURRENT_DATE), '/01');
    set start_last_month = date_add(start_this_month, interval -1 month);


    set agent_states_this_month = (select count(*) from agent_states where start >= start_this_month);
    set agent_states_changes_last_year = (select count(*) from agent_states where start < start_this_month);


    if (agent_states_this_month >0 and agent_states_changes_last_year >0) then
        -- Aggregate and insert into agent_states_last_month all dispositions data for last month
        insert ignore into agent_states_last_month
        (
            created_at,
            updated_at,
            date,
            is_call,
            user_id,
            team_id,
            chain_id,
            list_id,
            changes,
            duration
        )
        select 
            now(), now(),
            cast(start as date),
            case when transaction_id =0 then 0 else 1 end as is_call,
            user_id,
            team_id,
            chain_id,
            list_id,
            count(distinct id) as changes,
            sum(TIME_TO_SEC(timediff(end, start))) as duration
        from agent_states
        where end < start_this_month
        group by 
            cast(start as date),
            case when transaction_id =0 then 0 else 1 end,
            user_id,
            team_id,
            chain_id,
            list_id
            ;


        -- Delete agent states before start this month
        delete d
        from agent_states d
        where d.end < start_this_month
            ;


        call ultra_integration_uncle.aggregate_last_year_agent_states;
    end if;
end";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS aggregate_last_month_agent_states");
    }
}
