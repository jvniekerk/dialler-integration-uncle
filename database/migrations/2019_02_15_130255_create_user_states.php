<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_states', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedSmallInteger('ultra_id')->unique();
            $table->unsignedSmallInteger('state_id')->unique();
            $table->unsignedSmallInteger('system_state');
            $table->string('system_description');
            $table->unsignedSmallInteger('state_red');
            $table->unsignedSmallInteger('state_green');
            $table->unsignedSmallInteger('state_blue');
            $table->string('category');
            $table->unsignedSmallInteger('duration');
            $table->string('productive');
            $table->string('scheduled');
            $table->string('working');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_states');
    }
}
