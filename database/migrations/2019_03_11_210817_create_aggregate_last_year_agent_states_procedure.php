<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAggregateLastYearAgentStatesProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
drop procedure if exists ultra_integration_uncle.aggregate_last_year_agent_states;
        
CREATE PROCEDURE ultra_integration_uncle.aggregate_last_year_agent_states(
)
begin
    declare agent_states_this_month int;
    declare agent_states_last_month int;
    declare agent_states_last_year int;
    
    declare start_this_month date;
    declare start_last_month date;   
    declare start_last_year date;    

    set start_this_month = concat(extract(year from CURRENT_DATE), '/', extract(month from CURRENT_DATE), '/01');
    set start_last_month = date_add(start_this_month, interval -1 month);
    set start_last_year = date_add(start_last_month, interval -1 year);


    set agent_states_this_month = (select count(*) from agent_states where start >= start_this_month);
    set agent_states_last_year = (select count(*) from agent_states_last_month d where d.date < @start_last_month);



    if (agent_states_this_month >0 and agent_states_last_year >0) then
        -- Aggregate and insert into agent_states_last_year all dispositions data for last year before last month
        insert ignore into agent_states_last_year
        (
            created_at,
            updated_at,
            month,
            is_call,
            user_id,
            team_id,
            chain_id,
            list_id,
            changes,
            duration
        )
        select 
            now(), now(),
            concat(extract(year from date), '/', extract(month from date), '/01'),
            is_call,
            user_id,
            team_id,
            chain_id,
            list_id,
            sum(changes),
            sum(duration)
        from agent_states_last_month a
        where a.date < start_last_month
        group by 
            concat(extract(year from date), '/', extract(month from date), '/01'),
            is_call,
            user_id,
            team_id,
            chain_id,
            list_id
            ;


        -- Delete agent states from agent_states_last_month before start last month
        delete d
        from agent_states_last_month d
        where d.date < start_last_month
            ;
    end if;

    

    -- Delete agent states from agent_states_last_year from before start last year
    delete from agent_states_last_year where month < start_last_year;
end";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS aggregate_last_year_agent_states");
    }
}
