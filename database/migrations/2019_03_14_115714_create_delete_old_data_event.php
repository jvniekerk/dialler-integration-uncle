<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateDeleteOldDataEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
drop event if exists delete_old_data_event;

CREATE EVENT delete_old_data_event
ON SCHEDULE EVERY 1 day
STARTS date_add(current_date, interval 4 hour)
DO
   call ultra_integration_uncle.delete_old_data;";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS delete_old_data_event");
    }
}
