<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateDeleteOldDataProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
drop procedure if exists delete_old_data;

create procedure delete_old_data(
)
begin
    declare this_month_start date;
    declare last_month_start date;
    declare last_year_start date;
    declare six_years_back date;
    declare two_days_back date;
    declare week_back date;
    declare last_week_start date;
    declare days int;


    set this_month_start = concat(extract(year from CURRENT_DATE), '/', extract(month from CURRENT_DATE), '/01');
    set last_month_start = date_add(this_month_start, interval -1 month);
    set last_year_start = date_add(last_month_start, interval -1 year);
    set six_years_back = date_add(current_date, interval -6 year);
    set two_days_back = date_add(current_date, interval -2 day);
    set week_back = date_add(current_date, interval -1 week);

    set days = 7 + WEEKDAY(CURRENT_DATE);
    set last_week_start = date_add(current_date, interval -days day);

--     select
--         this_month_start,
--         last_month_start,
--         last_year_start,
--         six_years_back,
--         week_back,
--         last_week_start,
--         days;


    call ultra_integration_uncle.aggregate_last_month_calls;
    call ultra_integration_uncle.aggregate_last_month_dispositions;
    call ultra_integration_uncle.aggregate_last_month_agent_states;



    delete from agent_states where created_at < last_year_start;

    delete from agent_states_last_month where created_at < last_year_start;
    
    delete from agent_states_last_year where created_at < last_year_start;
    
    delete from alert_emails where created_at < week_back;
    
    delete from call_attempts where created_at < six_years_back;
    
    delete from call_attempts_last_month where created_at < last_year_start;
    
    delete from call_attempts_last_year where created_at < last_year_start;
    
    delete from contact_attempts where created_at < two_days_back;
    
    delete from data_download where created_at < two_days_back;
    
    delete from dispositions where created_at < six_years_back;
    
    delete from dispositions_last_month where created_at < last_year_start;
    
    delete from dispositions_last_year where created_at < last_year_start;
    
    delete from email_sms where created_at < two_days_back;
    
    delete from error_log where created_at < week_back;
    
    delete l from incoming_calls_loanno l
        left join call_attempts c on l.transaction_id = c.transaction_id
    where l.created_at < six_years_back or c.transaction_id is null;
    
    delete from max_attempts where created_at < last_month_start;
    
    delete from process_run where created_at < two_days_back;
    
    delete from suppress where created_at < last_month_start;
    
    delete from update_data where created_at < two_days_back;
    
    delete from update_data_change where created_at < two_days_back;

    

    -- Tables not delete from:
        -- call_causes
        -- call_types
        -- current_accounts
        -- lists
        -- migrations
        -- user_states
        -- users



    /*
    
    call delete_old_data;
    
        
    SELECT concat('delete from ', table_name, ' where created_at < ;') as delete_rows
    FROM information_schema.tables where table_schema='ultra_integration_uncle';
    
    */
end";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS delete_old_data");
    }
}
