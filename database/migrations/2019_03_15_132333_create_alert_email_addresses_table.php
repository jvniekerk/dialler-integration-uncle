<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertEmailAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_email_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('email')->unique();
            $table->boolean('active');
        });


        $procedure = "
insert ignore into alert_email_addresses
(
    created_at,
    updated_at,
    email,
    active
)
values 
    (now(), now(), 'oncall@unclebuck.co.uk', false),
    (now(), now(), 'jvniekerk@unclebuck.co.uk', true),
    (now(), now(), 'menemuwe@unclebuck.co.uk', false),
    (now(), now(), 'fboahene@unclebuck.co.uk', false),
    (now(), now(), 'sbell@unclebuck.co.uk', false),
    (now(), now(), 'ooyedeji@unclebuck.co.uk', false)
    ";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alert_email_addresses');
    }
}
