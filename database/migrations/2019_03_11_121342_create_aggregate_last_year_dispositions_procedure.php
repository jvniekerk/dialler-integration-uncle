<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAggregateLastYearDispositionsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
drop procedure if exists ultra_integration_uncle.aggregate_last_year_dispositions;

create PROCEDURE ultra_integration_uncle.aggregate_last_year_dispositions(
)
begin
    declare start_this_month date;
    declare start_last_month date;
    declare start_last_year date;
    

    declare dispositions_this_month int;
    declare calls_without_dispositions_last_year int;
    

    set start_this_month = concat(extract(year from CURRENT_DATE), '/', extract(month from CURRENT_DATE), '/01');
    set start_last_month = date_add(start_this_month, interval -1 month);
    set start_last_year = date_add(start_last_month, interval -1 year);


    set dispositions_this_month = (select count(*) from dispositions where date_created >= start_this_month);

    set calls_without_dispositions_last_year = (select count(*) from call_attempts c 
                                                    left join dispositions d on c.transaction_id = d.transaction_id
                                                where
                                                    (
                                                        d.id is null
                                                        or 
                                                        c.talk =0
                                                    )
                                                    and
                                                    c.start < start_last_month
                                                    and
                                                    c.start >= start_last_year);


    if (dispositions_this_month and calls_without_dispositions_last_year) then
        -- Aggregate and insert into dispositions_last_year all dispositions data for last year
        insert ignore into dispositions_last_year
        (
            created_at,
            updated_at,
            month_created,
            disposition_code,
            description,
            list_id,
            campaign_id,
            user_id,
            calls
        )
        select 
            now(), now(),
            concat(extract(year from date_created), '/', extract(month from date_created), '/01'),
            disposition_code,
            description,
            list_id,
            campaign_id,
            user_id,
            count(distinct d.ultra_id) as calls
        from dispositions d
        where 
            d.date_created >= start_last_year
            and
            d.date_created < start_last_month
        group by 
            concat(extract(year from date_created), '/', extract(month from date_created), '/01'),
            disposition_code,
            description,
            list_id,
            campaign_id,
            user_id
            ;



        -- Delete old disposition data from dispositions_last_month table
        delete d
        from dispositions_last_month d
        where d.date_created < start_last_month
            ;


        -- Delete old dispositions data from dispositions_last_year table
        delete d
        from dispositions_last_year d
        where d.month_created < start_last_year
            ;
    end if;
end";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS aggregate_last_year_dispositions");
    }
}
