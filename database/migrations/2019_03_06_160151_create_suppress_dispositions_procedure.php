<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSuppressDispositionsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
DROP PROCEDURE IF EXISTS suppress_dispositions;

CREATE PROCEDURE suppress_dispositions()
BEGIN
-- 	select * from ultra_integration_uncle.dispositions order by id;


    /* suppress based on disposition codes matched on loanno */
    insert into ultra_integration_uncle.suppress
    (
        created_at,
        updated_at,
        loanno,
        reason
    )
    select 
        now() as created_at,
        now() as updated_at,
        d.loanno,
        concat('Disposition ', d.disposition_code, ' (', d.description, ') used on transaction: ', d.transaction_id) as reason
    from ultra_integration_uncle.dispositions d
        left join ultra_integration_uncle.suppress s on d.loanno = s.loanno and s.created_at >= CURRENT_DATE
    where
        d.date_created >= CURRENT_DATE
        and
        s.loanno is null
        and
        (
            d.description like '%complain%'
            or
            d.description like '%left message%'
            or
            d.description like '%no outcome%'
            or
            d.description like '%resolution%'
            or
            d.description like '%wrong%'
            or
            d.description like '%work%'
        )
        and
        d.loanno is not null
        and
        d.loanno >0
    ;




    /* suppress based on disposition codes matched on phone number */
    insert into ultra_integration_uncle.suppress
    (
        created_at,
        updated_at,
        loanno,
        reason
    )
    select
        now() as created_at,
        now() as updated_at,
        c.loanno,
        concat('Disposition ', c.disposition_code, ' (', c.description, ') used on transaction: ', c.transaction_id,
            ' and phone number: ', c.customer_number) as reason
    from (
            select 
                c.transaction_id,
                c.customer_number,
                c.disposition_code,
                c.description,
                ifnull(ifnull(cm.loanno, ch.loanno), c.loanno) as loanno
            from (
                    select
                        c.transaction_id,
                        c.customer_number,
                        d.disposition_code,
                        d.description,
                        c.loanno
                    from ultra_integration_uncle.call_attempts c 
                        join ultra_integration_uncle.dispositions d on c.transaction_id = d.transaction_id
                    where 
                        c.start >= CURRENT_DATE
                        and 
                        c.list_id >0
                        and 
                        (
                            d.description like '%complain%'
                            or
                            d.description like '%left message%'
                            or
                            d.description like '%no outcome%'
                            or
                            d.description like '%resolution%'
                            or
                            d.description like '%wrong%'
                            or
                            d.description like '%work%'
                        )
            ) c
                left join ultra_integration_uncle.current_accounts cm on c.customer_number = cm.phone_number1
                left join ultra_integration_uncle.current_accounts ch on c.customer_number = ch.phone_number1
    ) c
        left join ultra_integration_uncle.suppress s on c.loanno = s.loanno and s.created_at >= CURRENT_DATE
    where 
        s.loanno is null 
        and 
        c.loanno is not null 
        and 
        c.loanno >0
    ;
        


--     select * from ultra_integration_uncle.suppress;
END;
";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS suppress_dispositions");
    }
}
