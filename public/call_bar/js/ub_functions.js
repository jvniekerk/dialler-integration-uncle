/**
 * Copies the outcomes of the questions asked to the person on the call to create note in UNCLE
 * @param textarea
 */
function copy_notes(textarea) {
    textarea.select();

    document.execCommand('copy');

    alert('Note copied, please create a note in LMS and paste these details.');
}


/**
 * Copies the note data
 * @param textarea
 */
function make_note_to_copy(textarea) {
    textarea.val('');

    let newline = '';


    for (let i = 1; i <= 5; i++) {
        if (textarea.val() !== '') {
            newline = '\n';
        }

        textarea.val(textarea.val() + newline + text_to_add(i));
    }
}


/**
 * Concatenate the address from the address fields
 * @returns {string}
 */
function get_address() {
    return $('#house_number').text() + ' ' + $('#unit').text() + ' ' + $('#street').text() + ' ' + $('#postcode').text();
}


/**
 * Concatenates the customer name
 * @returns {string}
 */
function get_name() {
    return $('#title').text() + ' ' + $('#first_name').text() + ' ' + $('#surname').text();
}


/**
 * Text to add to the copied note
 */
function text_to_add(div_id) {
    let value = '';

    switch (div_id) {
        case 1:
            value = "Did the person spoken to know " + get_name() + "?";
            break;
        case 2:
            value = "Is " + get_address() + " the right address?";
            break;
        case 3:
            value = "Has the customer left the address?";
            break;
        case 4:
            value = "Called address:";
            break;
        case 5:
            value = "Forwarding address:"
    }


    let div_val = $('#wrongNo' + div_id).val();

    if (div_val !== '0' && div_val !== '') {
        return value + " " + div_val;
    } else {
        return value + " No answer given";
    }
}


/**
 * Makes the copy of the note
 * @param textarea
 */
function make_copy_note(textarea) {
    make_note_to_copy(textarea);

    copy_notes(textarea);
}


/**
 * Check the next screen to show to the agent
 * @param question_id
 */
function validate_next_question(question_id) {
    let disposition = 0;

    if (question_id === 1) { // No answer
        disposition = 500;
    } else {
        disposition = $('#Disposition' + question_id).val();
    }


    showQuestion(6);

    saveDisposition(disposition);

    // alert('question=' + question_id);

    // Change agent state if they selected a different state after call
    if (question_id > 1) {
        let agent_state = $('#agent_state' + question_id).val();

        // alert('agent_state=' + agent_state);

        if (agent_state !== 2 || agent_state !== '2') {
            mCallControl.settingSet("SuppressWrapDialog", "1");

            mCallControl.changeState(agent_state);
        }
    }
}


/**
 * Shows the help image modals
 * @param image_id
 */
function show_modal(image_id) {
    let modal = document.getElementById('help_image_modal');

    // Get the image and insert it inside the modal - use its 'alt' text as a caption
    let img = document.getElementById(image_id);
    let modalImg = document.getElementById('help_modal_image');
    let captionText = document.getElementById('modal_image_caption');


    img.onclick = function () {
        modal.style.display = 'block';

        modalImg.src = this.src;

        captionText.innerHTML = this.alt;
    };


    $('#close_image_modal').click(function () {
        modal.style.display = 'none';
    });
}


/**
 * Checks the number of DPA checkboxes that have been checked
 */
function count_checked() {
    if ($('input.dpa:checked').length === $('input:checkbox.dpa').length) {
        $('#dpa_confirm').prop('disabled', false);

        $('#dpaError').hide();
    } else {
        $('#dpa_confirm').prop('disabled', true);

        $('#dpaError').show();
    }
}


/**
 * Manages the display of the checkboxes depending on its check state
 * @param checkbox_name
 */
function check_toggle(checkbox_name) {
    let checkbox = $('#' + checkbox_name);

    let div = $('#' + checkbox_name + '_div');

    if (checkbox.prop('checked')) {
        checkbox.prop('checked', false);

        div.css('background-color', 'grey');
    } else {
        checkbox.prop('checked', true);

        div.css('background-color', '#66EE4A');
    }

    count_checked();
}


/**
 * Saves the link between the loan number and the call transaction ID
 */
function save_transactionId_loanno() {
    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


    $.ajax({
        /* the route pointing to the post function */
        url: '/dialler-integration-uncle/public/postajax',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {
            _token: CSRF_TOKEN,
            loanno: parseInt($('#incoming_loanno').val()),
            transaction_id: parseInt($('#transaction_id').text())
        },
        dataType: 'JSON',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) {
            let response = 'status=' + data.status + ' transaction_id=' + data.transaction_id + ' loanno=' + data.loanno;

            console.log(response);
        },
    });
}


/**
 * Validates the entered loan number
 */
function validate_loanno() {
    let error = $('#loanno_error');

    error.hide();

    let loanno = parseInt($('#incoming_loanno').val());


    if (check_loanno(loanno)) {
        save_transactionId_loanno();
    } else if (!confirm('A valid Loan number was not entered. Do you want to continue without entering a valid Loan number?')) {
        error.show();

        return;
    }

    validate_next_question(7);
}


/**
 * Returns whether the given loanno is valid
 * TODO: At some point call UNCLE to validate loanno
 *
 * @param loanno
 * @returns {boolean}
 */
function check_loanno(loanno) {
    return (+loanno === loanno && !(loanno % 1) && loanno > 9548);  // TODO: Change this minimum loanno value to real minimum number
}


/**
 * Adds the loanno into the LoanNo div if valid
 *
 * @param loanno
 */
function allocate_loanno(loanno, show_alert) {
    if (check_loanno(loanno)) {
        $('#loanno_value').text(loanno);

        $('#loan_link_div').show();
    } else {
        if (show_alert) {
            alert('Invalid Loan Number / ID');
        }

        $('#loan_link_div').hide();
    }
}


/**
 * Validates the disposition code selected
 * @param id
 */
function validate_disposition(id) {
    let error = $('#DispositionError' + id);

    error.hide();


    if (parseInt($('#Disposition' + id).val()) === 0) {
        error.show();
    } else if (id === 7) {
        validate_loanno();
    } else {
        validate_next_question(id);
    }
}


/**
 * Shows the reschedule screen
 * @param id
 */
function show_reschedule(id) {
    $('#DispositionError' + id).hide();


    let dispo = $('#Disposition' + id).val();

    if ((dispo === 102 || dispo === '102' || id === 23) && id !== 7) {
        $('#divReschedule').show();
    } else {
        $('#divReschedule').hide();
    }
}


/**
 * Enlarges the reschedule screen
 */
function enlarge_reschedule() {
    let reschedule = $('#divReschedule');

    reschedule.css('height', '100%');

    reschedule.css('padding', '3em');
}


/**
 * Puts the agent in wrap state
 */
function goto_wrap() {
    if (mCallControl.LineState_Customer_String != "idle") {
        mCallControl.LineDisconnect_Customer();
    }

    mCallControl.settingSet("SuppressWrapDialog", "1");
}


