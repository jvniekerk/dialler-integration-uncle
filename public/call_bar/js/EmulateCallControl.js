function CallControl() {
    this.Version = "Emulator";

    this.TestCustomerFields = {};

    this.BaseRecordFieldCount = 0;

    this.BaseRecordSaveRequired = false;

    this.BaseRecordLoaded = false;

    this.CallDetails_Count = 0;

    this.AgentState = "connected";
}


CallControl.prototype.LoadDetails = function () {
    this.BaseRecordFieldCount = this.TestCustomerFields.length;

    this.CallDetails_Count = this.TestCustomerFields.length;

    this.BaseRecordLoaded = true;
}


CallControl.prototype.LogDisplay = function (string) {
    try {
        console.log(string);
    } catch (Error) {
    }
}


CallControl.prototype.DispositionGroups_MemberCount = function () {
    return 0;
}


CallControl.prototype.Data_RowCount = function (datacollection) {
    return 0;
}


CallControl.prototype.Data_BeginLoad = function () {
    return 0;
}


CallControl.prototype.Data_Clear = function (datacollection) {
    return 0;
}


CallControl.prototype.CallDetails_Value_ByKey = function (string) {
    for (var x = 0; x < this.TestCustomerFields.length; x++) {
        if (this.TestCustomerFields[x].Name == string) {
            return this.TestCustomerFields[x].Value;
        }
    }

    return "The field [" + string + "] has not been set in the emulator";
}


CallControl.prototype.CallDetails_Key = function (index) {
    return this.TestCustomerFields[index].Name;
}


CallControl.prototype.CallDetails_ContainsKey = function (field) {
    for (x = 0; x < this.TestCustomerFields.length; x++) {
        if (this.TestCustomerFields[x].Name == field) {
            return true;
        }
    }
    return false;
}


CallControl.prototype.BaseRecordFieldExists = function (string) {
    return this.CallDetails_ContainsKey(string);
}


CallControl.prototype.BaseRecordValueGet = function (string) {
    return this.CallDetails_Value_ByKey(string);
}


CallControl.prototype.BaseRecordValueSet = function (field, value) {
    this.LogDisplay("Setting " + field + " to " + value);
    for (var x = 0; x < this.TestCustomerFields.length; x++) {
        if (this.TestCustomerFields[x].Name == field) {
            this.TestCustomerFields[x].Value = value;

            this.BaseRecordSaveRequired = true;
        }
    }

    return true;
}


CallControl.prototype.BaseRecordField = function (index) {
    return this.TestCustomerFields[index].Name;
}


CallControl.prototype.BaseRecordFieldDataType = function (field) {
    for (var x = 0; x < this.TestCustomerFields.length; x++) {
        if (this.TestCustomerFields[x].Name == field) {
            return this.TestCustomerFields[x].DataType;
        }
    }

    return "";
}


CallControl.prototype.BaseRecordBeginReload = function () {
    SetTimeout('BaseRecordLoadComplete', 1000);
}


CallControl.prototype.BaseRecordSave = function () {
    return true;
}


CallControl.prototype.LineStartRecording = function () {
    return true;
}


CallControl.prototype.LineStopRecording = function () {
    return true;
}


CallControl.prototype.comDataUpload = function () {
    return true;
}