// Allow the script to edit the record where the fields are configured
var AllowFieldEdit = false;

// Set to false to hide the notes panel
var ShowNotes = false;

// Set to false to fix the notes panel open. (Otherwise it hides when the mouse moves out of it)
var AutoHideNotesPane = false;

// If set, this field will be updated with the most recent note when one is added.
var NotesField = undefined;

// Set to the id of the group, or undefined to disable the top bar
// var TopDispositionGroup = undefined;
// var TopDispositionGroup = $('#divOutcomesTop');
var TopDispositionGroup = 'divOutcomesTop';

// Set to the id of the disposition group, or undefined to disable the bottom bar of dispositions
var BottomDispositionGroup = undefined;

// Show the customer edit pane on the left hand side
var DisplayCustomerEditPane = false;

// Set to false to fix the field edit pane in place. (Otherwise it shows when the mouse moves into it, and hides when it moves out)
var AutoHideCustomerEditPane = true;

// Set to true to log a record for each page transition. These can be used to annotate the call recording playback.
var LogPageTransitions = true;

// Set to true to show the agent the previous outcomes for this record
var ShowDispositionHistory = true;

// Set the below to change which fields are visible on the left edit panel, and in which order
// Name is the display name which will be shown.
// Field is the actual field name from the data.
var CustomerEditFieldOrder = [
    {"Name": "First Name", "Field": "first_name"},
    {"Name": "Title", "Field": "title", "AllowedValues": ["", "Mr", "Miss", "Mrs", "Dr"]},
    {"Name": "Surname", "Field": "surname"},
    {"Name": "-", "Field": ""},
    {"Name": "DOB", "Field": "dob"},
    {"Name": "Mobile Number", "Field": "PhoneNumber1"},
    {"Name": "Home Number", "Field": "PhoneNumber2"},
    {"Name": "Work Number", "Field": "PhoneNumber3"},
    {"Name": "-", "Field": ""},
    {"Name": "Address:", "Field": "house_number"},
    {"Name": "Address:", "Field": "unit"},
    {"Name": "Address:", "Field": "street"},
    {"Name": "PostCode:", "Field": "postcode"}
];

// Set to true to automatically save the base record if the call ends (for any reason). (Useful for survey type calls)
var SaveRecordOnEndCall = true;


// If you want to emulate the script while you write it, set these values to appropriate test customer fields.
// Name is the field name.
// Value is the value of the field.
// DataType will determine what data validation is applied.
// These will NOT have any effect once the script is being used for live calls. They are for test only.
var TestCustomerFields = [
    {"Name": "title", "Value": "Ms.", "DataType": "Text"},
    {"Name": "first_name", "Value": "Betty", "DataType": "Text"},
    {"Name": "surname", "Value": "Cunningham", "DataType": "Text"},
    {"Name": "dob", "Value": "16/09/1957", "DataType": "Text"},
    {"Name": "dial_purpose", "Value": "Underwriting", "DataType": "Text"},
    {"Name": "balance", "Value": "284.77", "DataType": "Text"},
    {"Name": "CLI", "Value": "01959543400", "DataType": "PhoneNumber"},
    {"Name": "house_number", "Value": "23", "DataType": "Text"},
    {"Name": "postcode", "Value": "M38 9PN", "DataType": "Text"},
    {"Name": "RowId", "Value": "1166", "DataType": "Text"},
    {"Name": "street", "Value": "Some Street", "DataType": "Text"},
    {"Name": "unit", "Value": "B", "DataType": "Text"},
    // {"Name": "loanno", "Value": "723737", "DataType": "Int"},
    {"Name": "loanno", "Value": "9549", "DataType": "Int"},
    {"Name": "PhoneNumber1", "Value": "02031892512", "DataType": "PhoneNumber"},
    {"Name": "PhoneNumber2", "Value": "02031892512", "DataType": "PhoneNumber"},
    {"Name": "PhoneNumber3", "Value": "", "DataType": "PhoneNumber"},
    {"Name": "AgentFirstName", "Value": "Josef", "DataType": "Text"},
    {"Name": "TransactionId", "Value": (Math.floor(Math.random() * 1000000) + 100000), "DataType": "Text"},
    {"Name": "PhoneNumber", "Value": "07853038174", "DataType": "PhoneNumber"},
    {"Name": "CallType", "Value": "1", "DataType": "Int"}
    // {"Name": "CallType", "Value": "5", "DataType": "Int"}


    // Ultra Music test line -> 02031892512     // Ultra Incoming test line -> 02030569215
];


// Allow clients to have custom format options when their fields are displayed.
// Do not abuse this, it is not intended for data-type conversion.
// Not supported by editable fields
var CustomDisplayOptions = {
    "DataTypes":
        {
            "TrueFalse":
                {
                    // These are the values, and how we will display them...
                    "True": "Y",
                    "False": "N"
                }
        },
    // For field specific (not data type) rules
    "Custom":
        {
            "SomeFieldName":	// Name of a field
                {
                    "If the value is this": "Display this"	// Value to find and replace with
                }
        }
};


function Custom_OnLoad() {
    // Show incoming call question screen on incoming call
    let type = mCallControl.CallDetails_Value_ByKey("CallType");


    // Make rescheduling div bigger to be seen by agent
    let reschedule = $("#divReschedule");

    reschedule.css('height', '50%');
    reschedule.css('padding', '3em');


    // Call type 5 is inbound
    if (mCallControl.CallDetails_ContainsKey("CallType") && (type === 5 || type === "5")) {
        showQuestion(7);

        $('#reschedule_toggle').hide();

        $('.outgoing_call').hide();

        $('#outgoing_buttons').hide();

        $('#loan_link_div').hide();
    } else {
        showQuestion("1");

        $('#reschedule_toggle').show();

        $('#loan_link_div').show();
    }


    // Populate disposition codes lists with disposition code & agent states options from Ultra
    let dispos = [];

    let states = [];


    dispos.push({
        DispositionReason: "0",
        Description: "Please select an outcome"
    });


    states.push({
        AgentStateKey: "2",
        AgentStateValue: "Active"
    });


    // Check if script in emulation mode then add test disposition options
    if ($('#divEmulation').is(':visible')) {
        dispos.push({
            DispositionReason: "1",
            Description: "test disposition"
        });

        dispos.push({
            DispositionReason: "102",
            Description: "test callback"
        });

        dispos.push({
            DispositionReason: "504",
            Description: "test wrong number"
        });


        states.push({
            AgentStateKey: "1",
            AgentStateValue: "test state"
        });

        states.push({
            AgentStateKey: "3",
            AgentStateValue: "test state 3"
        });

        states.push({
            AgentStateKey: "4",
            AgentStateValue: "test state 4"
        });
    }


    // Get the disposition options from Ultra
    for (let d = 0, k = mCallControl.Data_RowCount("System.Disposition.Reason.Reschedule.Rules"); d < k; d++) {
        let descr = mCallControl.Data_Value_ByName("System.Disposition.Reason.Reschedule.Rules", "Description", d);

        if (descr.search(/test/i) < 0) {
            dispos.push({
                DispositionReason: mCallControl.Data_Value_ByName("System.Disposition.Reason.Reschedule.Rules", "DispositionReason", d),
                Description: descr
            });
        }


    }


    // Get the agent states from Ultra
    for (let d=0, k=mCallControl.AgentStates_User_Count; d<k; d++) {
        states.push({
            AgentStateKey: mCallControl.AgentStates_User_Key(d),
            AgentStateValue: mCallControl.AgentStates_User_Value(d)
        });
    }


    // Each dispositions select dropdown
    let ids = [4, 5, 7];

    // Add the dispositions options into each dropdown
    for (let j = 0, l = ids.length; j < l; j++) {
        for (let d = 0, k = dispos.length; d < k; d++) {
            $('#Disposition' + ids[j]).append($("<option>", {
                value: dispos[d].DispositionReason,
                text: dispos[d].Description
            }));
        }


        // Populate each agent state select dropdown
        let agent_state = $("#agent_state" + ids[j]);

        for (let d = 0, k = states.length; d < k; d++) {
            agent_state.append($("<option>", {
                value: states[d].AgentStateKey,
                text: states[d].AgentStateValue
            }));
        }


        // Set default state value
        agent_state.val('2');
    }


    // Hide callback option in incoming call disposition codes
    $("#Disposition7 option[value=102]").hide();


    // Put default "Wrong Number" disposition on Wrong Number/Not Available question screen
    $('#Disposition5').val('504');


    // Suppress Ultra wrap screen
    mCallControl.settingSet("SuppressWrapDialog", "1");
}


function Custom_LineStateChange_Customer(stateid, statename, reason, red, green, blue) {

}


function Custom_AgentStateChange(statename, red, green, blue) {

}
