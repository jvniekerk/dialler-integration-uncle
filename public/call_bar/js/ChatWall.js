var chatJson = {
	"Since":"9 December 2014 17:11", 
	"MessageIDs":[23,16,15,14,13,12],
	"Messages":{
		"23":		
		{
			"Posted":"8 December 2014 16:32", 
			"PostedBy":"James Sumner", 
			"Message":"Hey everyone, this is a test"
		},
		"16":		
		{
			"Posted":"7 December 2014 16:32", 
			"PostedBy":"James Sumner", 
			"Message":"Earlier message"
		},
		"15":		
		{
			"Posted":"7 December 2014 16:32", 
			"PostedBy":"James Sumner", 
			"Message":"Earlier message"
		},
		"14":		
		{
			"Posted":"7 December 2014 16:32", 
			"PostedBy":"James Sumner", 
			"Message":"Earlier message"
		},
		"13":		
		{
			"Posted":"7 December 2014 16:32", 
			"PostedBy":"James Sumner", 
			"Message":"Earlier message"
		},
		"12":		
		{
			"Posted":"7 December 2014 16:32", 
			"PostedBy":"James Sumner", 
			"Message":"Earlier message"
		}
	}
};

var displayedMessageIds = [];

var fakeId = 24;

function UpdateChat(){
	mCallControl.LogDisplay("Update Chat");
	
	chatJson.MessageIDs.splice(0,0,fakeId);
	chatJson.Messages[fakeId] = {"Posted":"Now!", "PostedBy":"James Sumner", "Message":"This isn't real"};
	
	fakeId++;
	
	RenderChat();
}

function RenderChat(){
	var html = "";
	
	mCallControl.logDisplay(chatJson);	
	
	for(var x=0;x<chatJson.MessageIDs.length;x++){
		if(displayedMessageIds.indexOf(chatJson.MessageIDs[x]) == -1){
			html += getHtmlForMessage(chatJson.Messages[chatJson.MessageIDs[x]], true);
			displayedMessageIds.push(chatJson.MessageIDs[x]);
		}
		else{
			html += getHtmlForMessage(chatJson.Messages[chatJson.MessageIDs[x]], false);
		}
	}
	
	mCallControl.logDisplay(html);
	
	$("#divChatMessages").html(html);	
	
	$(".NewMessage").show("fade",{},1000).removeClass("NewMessage");
}

function getHtmlForMessage(message, isNew){
	var html = "<div class=\"Message" + (isNew?" NewMessage":"") + "\">";
	
	html += "<div class=\"MessagePoster\">" + message.PostedBy + " says:</div>";
	//html += "<div>Posted " + message.Posted + " by " + message.PostedBy + "</div>";
	html += "<div>" + message.Message + "</div>";
	html += "<div class=\"PostedDate\">" + message.Posted + "</div>";
	html += "</div>";
	return html;
}

function PageLoad(){
	if(GetCallControl()){
		document.getElementById("divLoading").style.display = "none";
		document.getElementById("divNoCallControl").style.display = "none";
		document.getElementById("divCallControl").style.display = "block";
		
		RenderChat();
		setInterval('UpdateChat()', 10000);
	}
	else{
		document.getElementById("divLoading").style.display = "none";
		document.getElementById("divNoCallControl").style.display = "block";
		document.getElementById("divCallControl").style.display = "none";
	}
}

function GetCallControl() {	

	try{
		Array.prototype.indexOf = function(obj, start) {
			for (var i = (start || 0), j = this.length; i < j; i++) { 
				 if (this[i] === obj) { return i; } 
			} 
			return -1; 
		} 
	}
	catch(Error){}
	
	
    if (window.external.Version == undefined) {        
		mCallControl = new CallControl();
		document.getElementById('divEmulation').style.display = "block";
		haveCallControl = true;
    }
    else {
        mCallControl = window.external;
		haveCallControl = true;		
    }
	
    if (mCallControl != undefined) {        

    }
	
	return haveCallControl;
	
}
