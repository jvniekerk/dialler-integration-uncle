var mCallControl = undefined;

var haveCallControl = false;

var historyStack = new Array();

// Set true if we don't have Call Control available
var emulationMode = false;

// Download token
var mDispositionHistoryId = undefined;
var mUsersId = undefined;
var mNotesId = undefined;

var dispCodes = {};
var users = {};

// Set true when the disposition button is used to prevent double logging
var dispositionLogged = false;

var editDisplayed = false;
var notesDisplayed = false;

// We track the ID of the last note we saved to allow it to be over-written.
var noteRowId = undefined;

// We store the disposition selected while the user confirms
var mSelectedDisposition = undefined;

// We store the selected priority in the same way.
var mSelectedPriority = undefined;

var mRescheduleUserId = undefined;
var mRescheduleTime = undefined;
var mRescheduleDate = undefined;


function enterEditPanel() {
    if (AutoHideCustomerEditPane && !editDisplayed) {
        $("#editPanel").animate({left: "0px"});

        $("#mainPanel").animate({left: "370px"});
    }
    editDisplayed = true;
}


function leaveEditPanel() {
    if (AutoHideCustomerEditPane && editDisplayed) {
        $("#editPanel").animate({left: "-330px"});

        $("#mainPanel").animate({left: "40px"});
    }
    editDisplayed = false;
}


function hideEditPanel() {
    document.getElementById("editPanel").style.display = "none";
    document.getElementById("mainPanel").style.left = "0px";

    editDisplayed = false;
}


function showEditPanel() {
    document.getElementById("editPanel").style.display = "block";
    document.getElementById("editPanel").style.left = "0px";
    document.getElementById("mainPanel").style.left = "380px";

    editDisplayed = true;
}


function hideTopDispositionPanel() {
    document.getElementById("divOutcomesTop").style.display = "none";
    document.getElementById("editPanel").style.top = "0px";
    document.getElementById("mainPanel").style.top = "0px";
    document.getElementById("notesPanel").style.top = "0px";
}


function showTopDispositionPanel() {
    document.getElementById("divOutcomesTop").style.display = "block";
    document.getElementById("editPanel").style.top = "40px";
    document.getElementById("mainPanel").style.top = "40px";
    document.getElementById("notesPanel").style.top = "40px";
}


function hideBottomDispositionPanel() {
    document.getElementById("divOutcomesBottom").style.display = "none";
    document.getElementById("editPanel").style.bottom = "0px";

    try {
        document.getElementById("mainPanel").style.bottom = document.getElementById("editPanel").bottom;
    }	// This fails in IE 8
    catch (Error) {
    }

    document.getElementById("notesPanel").style.bottom = "0px";
}


function showBottomDispositionPanel() {
    document.getElementById("divOutcomesBottom").style.display = "block";
    document.getElementById("editPanel").style.bottom = "40px";
    document.getElementById("mainPanel").style.bottom = document.getElementById("editPanel").bottom;
    document.getElementById("notesPanel").style.bottom = "40px";
}


function enterNotesPanel() {
    if (AutoHideNotesPane && !notesDisplayed) {
        $("#notesPanel").animate({width: "350px"});

        $("#mainPanel").animate({right: "370px"});
    }

    notesDisplayed = true;
}


function leaveNotesPanel() {
    if (AutoHideNotesPane && notesDisplayed) {
        $("#notesPanel").animate({width: "20px"});

        $("#mainPanel").animate({right: "40px"});
    }

    notesDisplayed = false;
}


function hideNotesPanel() {
    $("#notesPanel").css("display", "none");
    $("#mainPanel").css("right", "0px");

    notesDisplayed = false;
}


function showNotesPanel() {
    $("#notesPanel").css("display", "block");
    $("#notesPanel").css("right", "0px");
    $("#notesPanel").css("width", "350px");
    $("#mainPanel").css("right", "370px");

    notesDisplayed = true;
}


//
// Called when the page first loads into the browser
//
function GetCallControl() {
    try {
        Array.prototype.indexOf = function (obj, start) {
            for (var i = (start || 0), j = this.length; i < j; i++) {
                if (this[i] === obj) {
                    return i;
                }
            }

            return -1;
        }
    } catch (Error) {
    }


    if (window.external.Version == undefined) {
        mCallControl = new CallControl();

        mCallControl.TestCustomerFields = TestCustomerFields;

        // Populates any test data
        mCallControl.LoadDetails();

        document.getElementById('divEmulation').style.display = "block";

        haveCallControl = true;
    } else {
        mCallControl = window.external;

        haveCallControl = true;
    }


    if (mCallControl != undefined) {
    }

    return haveCallControl;

}


function PageLoad() {
    if (GetCallControl()) {
        mCallControl.LogDisplay("Script Page Running PageLoad()");

        document.getElementById("divLoading").style.display = "none";
        document.getElementById("divNoCallControl").style.display = "none";
        document.getElementById("divCallControl").style.display = "block";


        mCallControl.LogDisplay("Data_RowCount=" + mCallControl.Data_RowCount("System.Disposition.Reason.Reschedule.Rules"));

        // Store the disposition codes so we don't need to loop through them multiple times
        for (var d = 0; d < mCallControl.Data_RowCount("System.Disposition.Reason.Reschedule.Rules"); d++) {
            dispCodes[mCallControl.Data_Value_ByName("System.Disposition.Reason.Reschedule.Rules", "DispositionReason", d)]
                = {
                Description: mCallControl.Data_Value_ByName("System.Disposition.Reason.Reschedule.Rules", "Description", d),
                Category: mCallControl.Data_Value_ByName("System.Disposition.Reason.Reschedule.Rules", "Category", d)
            };

            // mCallControl.LogDisplay(d);
        }


        $("#selectRescheduleUser").append($("<option>", {
            value: "",
            text: ""
        }));

        $("#selectRescheduleUser").append($("<option>", {
            value: mCallControl.UserId,
            text: "Me"
        }));

        $("#selectRescheduleUser").append($("<option>", {
            value: "",
            text: ""
        }));


        try {
            if (mCallControl.Data_Loaded("Framework.User")) {
                UsersLoaded();
            } else {
                mUsersId = mCallControl.Data_Load("<data clientid=\"" + mCallControl.ClientId + "\"><Framework.User /></data>");

                UsersLoaded();
            }
        } catch (Error) {
        }


        ReplaceFields();


        try {
            if (TopDispositionGroup != undefined) {
                mCallControl.LogDisplay(TopDispositionGroup);

                PopulateDispositionBar($("#divOutcomesTop"), TopDispositionGroup);
            } else {
                hideTopDispositionPanel();
            }
        } catch (Error) {
        }


        try {
            if (BottomDispositionGroup != undefined) {
                PopulateDispositionBar($("#divOutcomesBottom"), BottomDispositionGroup);
            } else {
                hideBottomDispositionPanel();
            }
        } catch (Error) {
            alert("Error showing bottom disposition buttons: " + Error);
        }


        $(".dispositionButton").hover(
            function () {
                $(this).addClass("dispositionButtonOver", 500);
            },
            function () {
                $(this).removeClass("dispositionButtonOver", 500);
            });


        $(".button").hover(
            function () {
                $(this).addClass("buttonOver", 500);
            },
            function () {
                $(this).removeClass("buttonOver", 500);
            }
        );


        mCallControl.LogDisplay("Allow Field Edit: " + AllowFieldEdit + " BaseRecordLoaded: " + mCallControl.BaseRecordLoaded);


        PopulateEditPane();


        if (AllowFieldEdit && !mCallControl.BaseRecordLoaded) {
            mCallControl.BaseRecordBeginReload();
        } else {
            BaseRecordLoadComplete();
        }


        if (DisplayCustomerEditPane) {
            if (!AutoHideCustomerEditPane) {
                showEditPanel();
            }
        } else {
            hideEditPanel();
        }


        if (ShowNotes) {
            if (!AutoHideNotesPane) {
                showNotesPanel();
            }

            mCallControl.Data_Clear("System.ListRecordNotes");

            mNotesId = mCallControl.Data_BeginLoad("<data clientid=\"" + mCallControl.ClientId + "\"><System.ListRecordNotes BaseIndexId='" + mCallControl.CallDetails_Value_ByKey('BaseId') + "' RowId='" + mCallControl.CallDetails_Value_ByKey('RowId') + "' /></data>");
        } else {
            hideNotesPanel();
        }


        if (ShowDispositionHistory) {
            if (mCallControl.CallDetails_ContainsKey("BaseId") && mCallControl.CallDetails_ContainsKey("RowId")) {
                mCallControl.Data_Clear("System.DispositionHistory");

                mDispositionHistoryId = mCallControl.Data_BeginLoad("<data clientid=\"" + mCallControl.ClientId + "\"><System.DispositionHistory baseId='" + mCallControl.CallDetails_Value_ByKey('BaseId') + "' rowId='" + mCallControl.CallDetails_Value_ByKey('RowId') + "' /></data>");
            }
        }


        // Hookup the onchange event for the customer DDI dropdown
        $("#selectCustomerDDI").change(function () {
            if ($("#selectCustomerDDI").val() == "Other") {
                $("#txtPreviewDDI").show();
            } else {
                $("#txtPreviewDDI").hide();

                $("#txtPreviewDDI").val($("#selectCustomerDDI").val());
            }
        });


        for (var x = 0; x < mCallControl.AgentStates_User_Count; x++) {
            $("#selectState").append($("<option>", {
                value: mCallControl.AgentStates_User_Key(x),
                text: mCallControl.AgentStates_User_Value(x)
            }));
        }


        mCallControl.LogDisplay("System.BaseIndex=" + mCallControl.Data_RowCount("System.BaseIndex"));

        for (var x = 0; x < mCallControl.Data_RowCount("System.BaseIndex"); x++) {
            if (mCallControl.Data_Value_ByName("System.BaseIndex", "SchemaCode", x) == mCallControl.CurrentSchema && mCallControl.Data_Value_ByName("System.BaseIndex", "IsEnabled", x) == "True") {
                mCallControl.LogDisplay("Adding list " + mCallControl.Data_Value_ByName("System.BaseIndex", "BaseIndexId", x) + " -" + mCallControl.Data_Value_ByName("System.BaseIndex", "BaseName", x) + " to options");

                $("#selectReassignList").append($("<option>", {
                    value: mCallControl.Data_Value_ByName("System.BaseIndex", "BaseIndexId", x),
                    text: mCallControl.Data_Value_ByName("System.BaseIndex", "BaseName", x)
                }));
            }
        }


        try {
            PopulateDispositionDropdown();
        } catch (Error) {
        }


        $("#selectRescheduleReason").change(function () {
            // Do we need to show the agent set controls?
            mCallControl.LogDisplay("Selected Reschedule Outcome Changed");

            var index = $("#selectRescheduleReason").val();

            let reschedule = $("#divReschedule");


            if ($("#selectRescheduleReason").val() == -1)	// The "- SELECT -" option...
            {
            } else {
                mCallControl.LogDisplay("Minutes: " + mCallControl.DispositionCodes_Minutes(index) + " Terminal: " + mCallControl.DispositionCodes_Terminal(index));
                if (mCallControl.DispositionCodes_Minutes(index) == "0" && mCallControl.DispositionCodes_Terminal(index) == "0") {
                    // We need to agent set callback
                    $("#divReschedule_Time").show();

                    // $("#divReschedule").css('height', '200px');
                    reschedule.css('height', '50%');
                    reschedule.css('padding', '3em');

                    $("#spanRescheduleTip").text("The record will be rescheduled according to your selection");
                } else {
                    $("#divReschedule_Time").hide();

                    // $("#divReschedule").css('height', '150px');
                    reschedule.css('height', '50%');
                    reschedule.css('padding', '3em');

                    // Not agent set
                    if (mCallControl.DispositionCodes_Terminal(index) == "1") {
                        // Call will be completed
                        $("#spanRescheduleTip").text("The record will be completed");
                    } else {
                        // Call will be rescheduled
                        var minutes = mCallControl.DispositionCodes_Minutes(index);

                        $("#spanRescheduleTip").text("The record will be rescheduled for " + minutes + " minutes");
                    }
                }
            }
        });


        // Setup the reschedule/reassign controls
        for (var hour = 0; hour < 24; hour++) {
            var h = "0" + hour;

            if (h.length > 2) {
                h = h.substring(1, 3);
            }

            $("#selectRescheduleHour").append($("<option>", {value: h, text: h}));
        }


        for (var min = 0; min < 60; min++) {
            var h = "0" + min;

            if (h.length > 2) {
                h = h.substring(1, 3);
            }

            $("#selectRescheduleMinute").append($("<option>", {value: h, text: h}));
        }


        $("#textRescheduleDate").datepicker({
            changeYear: true,
            duration: "normal",
            yearRange: "2016:2020",
            dateFormat: "dd/mm/yy"
        });


        if (mCallControl.AgentState.toLowerCase() == "preview") {
            if (mCallControl.LineState_Customer_String == "connecting") {
                $("#spanCustomerDDI").text("Customer"); //(mCallControl.SettingGet("DiallingDDI"));

                $(".preview").hide();

                //$(".dialling").show();
            } else {
                $("#txtPreviewDDI").hide();


                for (var x = 0; x < 6; x++) {
                    if (mCallControl.CallDetails_ContainsKey("PhoneNumber" + x)) {
                        var number = mCallControl.CallDetails_Value_ByKey("PhoneNumber" + x);

                        $("#selectCustomerDDI").append(
                            $("<option>",
                                {
                                    value: number,
                                    text: number
                                }
                            )
                        );
                    }
                }


                try {
                    $("#selectCustomerDDI").val(mCallControl.CallDetails_Value_ByKey("PhoneNumber1"));
                } catch (Error) {
                }


                $("#txtPreviewDDI").val(mCallControl.CallDetails_Value_ByKey("PhoneNumber1"));


                $("#divPreviewMode").show();


                // Previous BP Number for Danielle...
                if (mCallControl.SettingGet("PreviousBP") != "" && mCallControl.SettingGet("PreviousBP") != mCallControl.CallDetails_Value_ByKey("BCMSCustomernumber")) {
                    $(".PreviousBP").text("Previous BP Number: " + mCallControl.SettingGet("PreviousBP"));
                }


                // Store this BP number for the next call
                try {
                    mCallControl.SettingSet("PreviousBP", mCallControl.CallDetails_Value_ByKey("BCMSCustomernumber"))
                } catch (Error) {
                }
            }
        } else if (mCallControl.AgentState.toLowerCase() == "connecting") {
            $("#spanCustomerDDI").text("Agent");

            $(".preview").hide();

            //$(".dialling").show();
        }


        showQuestion("1");


        // Execute any user-specific startup script
        try {
            Custom_OnLoad();
        } catch (error) {
            // alert(error.message);
        }
    } else {
        document.getElementById("divLoading").style.display = "none";
        document.getElementById("divNoCallControl").style.display = "block";
        document.getElementById("divCallControl").style.display = "none";
    }
}


function BaseRecordLoadComplete() {
    ReplaceFields();

    ReplaceEdit();

    if (DisplayCustomerEditPane) {
        PopulateEditPane();
    } else {
        hideEditPanel();
    }
}


function ReplaceFields() {

    var items = document.querySelectorAll(".Static_Greeting");

    for (var y = 0; y < items.length; y++) {
        items[y].innerText = getGreeting();
    }

    for (var x = 0; x < mCallControl.CallDetails_Count; x++) {
        var items = document.querySelectorAll('.Bind_' + mCallControl.CallDetails_Key(x));

        for (var y = 0; y < items.length; y++) {
            items[y].innerText = mCallControl.CallDetails_Value_ByKey(mCallControl.CallDetails_Key(x));
        }
    }


    mCallControl.LogDisplay("Base Record Loaded: " + mCallControl.BaseRecordLoaded + " (" + mCallControl.BaseRecordFieldCount + " records)");

    if (mCallControl.BaseRecordLoaded) {
        for (var x = 0; x < mCallControl.BaseRecordFieldCount; x++) {
            var items = document.querySelectorAll(".Bind_" + mCallControl.BaseRecordField(x));

            for (var y = 0; y < items.length; y++) {
                //alert(mCallControl.BaseRecordField(x) + ": " + mCallControl.BaseRecordFieldDataType(mCallControl.BaseRecordField(x)));

                // Apply custom formatting to the fields/data types
                var dt = mCallControl.BaseRecordFieldDataType(mCallControl.BaseRecordField(x));

                try {
                    if (CustomDisplayOptions["Custom"][mCallControl.BaseRecordField(x)]) {
                        mCallControl.LogDisplay("Using custom format for field " + mCallControl.BaseRecordField(x));

                        var value = mCallControl.BaseRecordValueGet(mCallControl.BaseRecordField(x));

                        if (CustomDisplayOptions["Custom"][mCallControl.BaseRecordField(x)][value]) {
                            var newValue = CustomDisplayOptions["Custom"][mCallControl.BaseRecordField(x)][value];

                            mCallControl.LogDisplay("Applying format rule for \"" + value + "\"; Formatted " + value + " => " + newValue);

                            items[y].innerText = newValue;
                        } else {
                            mCallControl.LogDisplay("No format rule defined for \"" + value + "\"");

                            items[y].innerText = mCallControl.BaseRecordValueGet(mCallControl.BaseRecordField(x));
                        }
                    } else if (CustomDisplayOptions["DataTypes"][dt]) {
                        mCallControl.LogDisplay("Using custom format for field " + mCallControl.BaseRecordField(x) + " by data type " + dt);

                        var value = mCallControl.BaseRecordValueGet(mCallControl.BaseRecordField(x));

                        var formattedValue = CustomDisplayOptions["DataTypes"][dt][value];

                        mCallControl.LogDisplay("Formatted " + value + " => " + formattedValue);

                        items[y].innerText = formattedValue;
                    } else {
                        items[y].innerText = mCallControl.BaseRecordValueGet(mCallControl.BaseRecordField(x));
                    }
                } catch (error) {
                    // Worst case, display raw
                    items[y].innerText = mCallControl.BaseRecordValueGet(mCallControl.BaseRecordField(x));
                }
            }
        }
    }
}


function ReplaceEdit() {
    if (mCallControl.BaseRecordLoaded) {
        $.datepicker.setDefaults({
            showOn: "focus",
        });

        for (var x = 0; x < mCallControl.BaseRecordFieldCount; x++) {
            var items = document.querySelectorAll(".Edit_" + mCallControl.BaseRecordField(x));

            for (var y = 0; y < items.length; y++) {
                if (items[y].tagName == "INPUT") {
                    // We've already replaced this...
                    items[y].value = mCallControl.BaseRecordValueGet(mCallControl.BaseRecordField(x));
                } else if (items[y].tagName == "SPAN") {
                    var html = "";

                    var field = mCallControl.BaseRecordField(x);


                    if (mCallControl.BaseRecordFieldDataType(field).toLowerCase() == "datetime") {
                        html = "<input type=\"text\" class=\"dataEdit DateEdit Edit_" + field + "\" value=\"" + mCallControl.BaseRecordValueGet(field) + "\" onchange=\"ValidateField('" + field + "', this.value)\"></input>";

                        $(".dataEdit.DateEdit.Edit_" + field).datepicker({
                            changeYear: true,
                            duration: "normal",
                            yearRange: "1900:2100",
                            dateFormat: "dd/mm/yy",
                        });
                    } else if (mCallControl.BaseRecordFieldDataType(field).toLowerCase() == "truefalse") {
                        html = "<input type=\"checkbox\" class=\dataEdit Edit_" + field + "\" " + (mCallControl.BaseRecordValueGet(field) == "True" ? "checked" : "") + " onchange=\"ValidateField('" + field + "', this.checked)\"></input>";
                    } else {
                        html = "<input type=\"text\" onblur=\"ValidateField('" + mCallControl.BaseRecordField(x) + "',this.value);\" class=\" dataEdit Edit_" + mCallControl.BaseRecordField(x) + "\" value=\"" + mCallControl.BaseRecordValueGet(mCallControl.BaseRecordField(x)) + "\"></input>";
                    }

                    items[y].innerHTML = html;
                }
            }
        }
    }
}


// Get the value of a given field from the base record, unless it hasn't yet loaded. In that case, get it from the call details
function GetValue(fieldName) {
    if (mCallControl.BaseRecordLoaded) {
        return (mCallControl.BaseRecordFieldExists(fieldName) ? mCallControl.BaseRecordValueGet(fieldName) : "");
    } else {
        return (mCallControl.CallDetails_ContainsKey(fieldName) ? mCallControl.CallDetails_Value_ByKey(fieldName) : "");
    }
}


// This will usually be hit twice. Once to pre-populate from the datapop, then again once the base record has loaded.
function PopulateEditPane() {
    var html = "<table width=100%>";

    mCallControl.LogDisplay("Number of Sidebar fields: " + CustomerEditFieldOrder.length);

    for (var x = 0; x < CustomerEditFieldOrder.length; x++) {
        try {
            var name = CustomerEditFieldOrder[x].Name;

            var field = CustomerEditFieldOrder[x].Field;


            if (name == "-") {
                html += "<tr><td colspan=\"2\"><hr></td></tr>";
            } else if (field == "")	// Blank field represents a label
            {
                html += "<tr><td><div class=\"sideBarLabel\">" + name + "</div></td></tr>";
            } else {
                if (mCallControl.BaseRecordLoaded) {
                    if (mCallControl.BaseRecordFieldExists(field)) {
                        html += "<tr><td>" + name + "</td><td>";

                        if (CustomerEditFieldOrder[x].ReadOnly == "true") {
                            html += "<span class=\"Bind_" + field + "\">" + GetValue(field) + "</span>";
                        } else if (mCallControl.BaseRecordFieldDataType(field).toLowerCase() == "datetime") {
                            html += "<input type=\"text\" class=\"dataEditSideBar DateEdit Edit_" + field + "\" value=\"" + mCallControl.BaseRecordValueGet(field) + "\" onchange=\"ValidateField('" + field + "', this.value);\"></input>";
                        } else if (CustomerEditFieldOrder[x].AllowedValues) {
                            html += "<select onchange=\"ValidateField('" + field + "', this.value);\" class=\"dataEditSideBar Edit_" + field + "\">"
                            for (var y = 0; y < CustomerEditFieldOrder[x].AllowedValues.length; y++) {
                                html += "<option>" + CustomerEditFieldOrder[x].AllowedValues[y] + "</option>";
                            }

                            html += "</select>";
                        } else {
                            html += "<input type=\"text\" onblur=\"ValidateField('" + field + "',this.value);\" class=\"dataEditSideBar Edit_" + field + "\" value=\"" + mCallControl.BaseRecordValueGet(field) + "\"></input>";
                        }

                        html += "</td></tr>";
                    }
                } else {
                    if (mCallControl.CallDetails_ContainsKey(field)) {
                        html += "<tr><td>" + name + "</td><td>";

                        if (CustomerEditFieldOrder[x].ReadOnly == "true") {
                            html += "<span class=\"Bind_" + field + "\">" + GetValue(field) + "</span>";
                        } else {
                            html += "<input type=\"text\" onblur=\"ValidateField('" + field + "',this.value);\" class=\"dataEditSideBar Edit_" + field + "\" value=\"" + GetValue(field) + "\"></input>";
                        }

                        html += "</td></tr>";
                    }
                }
            }
        } catch (Error) {
            mCallControl.LogDisplay("Error displaying side bar fields: " + Error);

            mCallControl.LogDisplay("Index: " + x + " of " + CustomerEditFieldOrder.length + " fields.");
        }
    }

    html += "</table>";


    document.getElementById("editFields").innerHTML = html;

    $.datepicker.setDefaults({
        showOn: "focus",
    });

    mCallControl.LogDisplay("BaseRecordFieldCount=" + mCallControl.BaseRecordFieldCount);

    for (var x = 0; x < mCallControl.BaseRecordFieldCount; x++) {
        var field = mCallControl.BaseRecordField(x);

        //mCallControl.LogDisplay(field + ": " + mCallControl.BaseRecordFieldDataType(field));
        if (mCallControl.BaseRecordFieldDataType(field).toLowerCase() == "datetime") {
            $(".dataEditSideBar.Edit_" + field).datepicker({
                changeYear: true,
                duration: "normal",
                yearRange: "1900:2100",
                dateFormat: "dd/mm/yy",
            });
        }
    }
}


function NotesTextChanged() {
    var text = document.getElementById("txtNotes").value;
    var chars = 4000 - text.length;

    if (chars < 0) {
        text = text.substring(0, 4000);
    }

    document.getElementById("spanCharCount").innerText = chars;

    //$("#txtNotes").addClass("noteUnsaved");

    $("#notesSaveMessage").text("You have unsaved Changes");

    $("#notesSaveMessage").css("color", "#FF0F0F");
}


function btnSaveNotes_Click() {

    $("#notesSaveMessage").text("Saving...");

    $("#notesSaveMessage").css("color", "");

    if (SaveNoteText()) {
        $("#txtNotes").removeClass("noteUnsaved");

        $("#notesSaveMessage").text("Saved!");

        $("#notesSaveMessage").css("color", "");
    } else {
        $("#notesSaveMessage").text("Error Saving!");

        $("#notesSaveMessage").css("color", "#FF0000");
    }

}


function SaveNoteText() {
    var noteText = $("#txtNotes").text();

    noteText = mCallControl.XMLEscape(noteText);

    var d = new Date();

    var timeStamp = d.getUTCHours() + ":" + d.getUTCMinutes() + ":" + d.getUTCSeconds();

    var xml = "<upload clientid=\"" + mCallControl.ClientId + "\"><System.ListRecordNotes BaseIndexId=\"" + mCallControl.CallDetails_Value_ByKey("BaseId") + "\"><rows>";
    xml += (noteRowId != undefined ? "<updateOnly " : "<insertOnly ");
    xml += (noteRowId != undefined ? "Id=\"" + noteRowId + "\" " : "");
    xml += "AgentId=\"" + mCallControl.UserId + "\" ";
    xml += "RowId=\"" + (mCallControl.CallDetails_ContainsKey("RowId") ? mCallControl.CallDetails_Value_ByKey("RowId") : "0") + "\" ";
    xml += "TransactionId=\"" + mCallControl.CallDetails_Value_ByKey("TransactionId") + "\" ";
    xml += "Note=\"" + noteText + "\" ";
    xml += "TimeStampUTC=\"" + timeStamp + "\" ";
    xml += "/>";
    xml += "</rows></System.ListRecordNotes></upload>";

    mCallControl.LogDisplay("Uploading: " + xml);

    var result = mCallControl.comDataUpload(xml);
    //var result = "<?xml version=\"1.0\" encoding=\"utf-16\"?><results><Notes2><rows count=\"1\"><validate rowindex=\"0\" outcome=\"7\" /></rows><save rows=\"1\" outcome=\"ok\" /></Notes2></results>";

    mCallControl.LogDisplay("Result: " + result);

    var $result = $(result);

    if ($result.find("save").attr("outcome") == "ok") {		// Saved row ok
        if (noteRowId == undefined) {
            noteRowId = $result.find("validate").attr("outcome");

            if (NotesField != undefined) {
                try {
                    mCallControl.BaseRecordValueSet(NotesField, noteText);
                } catch (Error) {
                }
            }
        }
        return true;

    } else {												// Error condition
        return false;
    }
}


function ValidateField(field, value) {
    var ok = mCallControl.BaseRecordValueSet(field, value);

    var fields = document.querySelectorAll(".Edit_" + field);

    $(".Bind_" + field).text(value);

    for (var x = 0; x < fields.length; x++) {
        if (fields[x].tagName == "INPUT") {
            fields[x].value = value;

            var className = fields[x].className;

            var sidebar = className.indexOf("dataEditSideBar") > -1;

            var date = className.indexOf("dataEditSideBarDate") > -1;

            // Rebuld the appropriate class string
            fields[x].className = "dataEdit" + (sidebar ? "SideBar" : "") + (date ? "Date" : "") + " Edit_" + field + (ok ? "" : " dataError");

        }
    }
}


function PopulateDispositionBar(barDiv, dispositionGroup) {
    var html = "";

    for (var x = 0; x < mCallControl.DispositionGroups_MemberCount(dispositionGroup); x++) {
        var index = mCallControl.DispositionCodes_Position(mCallControl.DispositionGroups_Member(dispositionGroup, x));

        if (index > -1) {
            var DispositionReason = mCallControl.DispositionCodes_Key(index);

            var DispositionValue = mCallControl.DispositionCodes_Value(index);

            //html += "<input type=\"button\" onclick=\"saveDisposition(" + DispositionReason + ");\" value=\"" + DispositionValue + "\"></button>&nbsp;";
            html += "<span class=\"dispositionButton " + (mCallControl.DispositionCodes_Terminal(index) == 1 ? "dispositionButton_Terminal" : "dispositionButton_Recycle") + "\" onclick=\"saveDisposition(" + DispositionReason + ");\">" + DispositionValue + "</span>&nbsp;";
        }
    }

    var s = document.createElement("SPAN");

    barDiv.appendChild(s);

    s.innerHTML = html;
}


function btnSave_Click() {
    mCallControl.LogDisplay("Agent selected save customer record");

    document.getElementById("btnSave").disabled = true;

    document.getElementById("saveFeedback").innerText = "Saving...";

    try {
        if (mCallControl.BaseRecordSave()) {
            document.getElementById("saveFeedback").innerText = "Done!";

            setTimeout("document.getElementById(\"saveFeedback\").innerText=\"\";", 4000);
        } else {
            document.getElementById("saveFeedback").innerText = "Error!";

            mCallControl.LogDisplay("Error saving values: " + mCallControl.GetLastError);
        }
    } catch (Error) {
    }

    document.getElementById("btnSave").disabled = false;
}


function toggleOutcomeButtons(disable) {
    var topButtons = document.getElementById("divOutcomesTop").getElementsByTagName("input");

    var bottomButtons = document.getElementById("divOutcomesBottom").getElementsByTagName("input");

    for (var x = 0; x < topButtons.length; x++) {
        topButtons[x].disabled = disable;
    }

    for (var x = 0; x < bottomButtons.length; x++) {
        bottomButtons[x].disabled = disable;
    }
}


function saveDisposition(dispositionReason) {
    mCallControl.LogDisplay('dispositionLogged=' + dispositionLogged);


    // window.open("https://www.w3schools.com");

    if (!dispositionLogged) {
        dispositionLogged = true;

        mCallControl.LogDisplay("Agent selected outcome " + dispositionReason + " from the script button.");

        toggleOutcomeButtons(true);

        mCallControl.settingSet("SuppressWrapDialog", "1");


        if (mCallControl.LineState_Customer_String != "idle") {
            mCallControl.LineDisconnect_Customer();
        }


        if (mCallControl.comSaveDisposition(dispositionReason, "")) {
            mCallControl.comActivate("", false);

            mCallControl.settingSet("SuppressWrapDialog", "0");
        } else {
            // Handle error...
            mCallControl.settingSet("SuppressWrapDialog", "0");

            toggleOutcomeButtons(false);
        }
    } else {
        mCallControl.LogDisplay("Agent double clicked the disposition button. 2nd attempt ignored.");
    }
}


//
// Goto the next question...
//
function showQuestion(divName) {
    mCallControl.LogDisplay("Show question/div " + divName);

    if (divName === 2) {
        count_checked();
    }


    if (historyStack.length > 0) {
        var d = historyStack.pop();

        historyStack.push(d);

        $("#" + d).fadeOut(250, function () {
            $("#" + divName).fadeIn(250);
        });
    } else {
        $("#" + divName).fadeIn(250);
    }


    $(".CallGuide").fadeOut(250);

    historyStack.push(divName);

    // Perform comDataUpload into ScriptProgress data collection
    if (LogPageTransitions) {
        try {
            // This will fail on Call Control releases prior to 2.1.71.0
            var payload = "<upload clientid='" + mCallControl.ClientId + "'><ScriptProgress><rows><insertOnly TransactionId=\"" + mCallControl.CallDetails_Value_ByKey("TransactionId") + "\" Annotation=\"Start Question " + divName + "\"  /></rows></ScriptProgress></upload>";

            var response = mCallControl.comDataUpload(payload);

            mCallControl.LogDisplay("payload => " + payload);
            mCallControl.LogDisplay("mCallControl.comDataUpload(payload) response => " + response);
        } catch (error) {
            mCallControl.LogDisplay("Failed to Upload page transition annotation: " + error)
        }
    }
}


function showQuestionCondition(divName, field, val) {
    if (mCallControl.BaseRecordValueGet(field) != val) {
        showQuestion(divName);
    } else {
        alert("Please make a selection");
    }
}


/**
 * Returns to the last question asked
 */
function PreviousQuestion() {
    historyStack.pop();

    var divName = historyStack.pop();

    showQuestion(divName);
}


//
// Returns morning, afternoon or evening
//
function getGreeting() {
    var returnString = "";

    var todaysDate = new Date()

    var theHour = todaysDate.getHours()

    if (theHour < 12) {
        returnString = 'morning';
    }

    if (theHour > 11) {
        returnString = 'afternoon';
    }

    if (theHour > 17) {
        returnString = 'evening';
    }

    return returnString;
}


//
// Update the display for customer line state changes
//
function OnLineStateChange_Customer(stateid, statename, reason, red, green, blue) {
    if (!haveCallControl) {
        try {
            setTimeout("OnLineStateChange_Customer(" + stateid + ", '" + statename + "', '" + reason + "', " + red + "," + green + "," + blue + ")", 100);
        } catch (Error) {
        }
        ;
        return;
    } else {

        if (statename.toLowerCase() == "connecting" && mCallControl.LineState_Customer_String == "connecting") {
            $(".preview").hide();
            $("#spanCustomerDDI").text("Customer");
            mCallControl.LogDisplay("DIALLING: SHOWING AS LINE STATE CONNECTING");
            $(".dialling").show();

        } else if (statename.toLowerCase() == "connected" || statename.toLowerCase() == "joined") {
            $(".preview").hide();
            mCallControl.LogDisplay("DIALLING: HIDING AS LINE STATE CONNECTED");
            $(".dialling").hide();
        } else if (statename.toLowerCase() == "idle") {
            try {
                if (SaveRecordOnEndCall) {
                    if (mCallControl.BaseRecordSaveRequired) {
                        mCallControl.LogDisplay("Page is configured to save on end call: Saving Base Record");
                        mCallControl.BaseRecordSave();
                    } else {
                        mCallControl.LogDisplay("Page is configured to save on end call: NO CHANGES TO SAVE");
                    }
                }
            } catch (Error) {
            }
        }

        try {
            // The config can implement this function if it needs to perform any client specific actions
            mCallControl.LogDisplay("Ultra.js => Execute Custom_LineStateChange_Customer");
            Custom_LineStateChange_Customer(stateid, statename, reason, red, green, blue);
        } catch (Error) {
            mCallControl.LogDisplay("ERROR: " + Error);
        }
    }
}


function OnLineStateChange_Transfer(stateid, statename, reason, red, green, blue) {
    try {
        Custom_LineStateChange_Transfer(stateid, statename, reason, red, green, blue);
    } catch (Error) {
    }
}


function OnAgentStateChange(statename, red, green, blue) {
    if (!haveCallControl) {
        try {
            setTimeout(OnAgentStateChange(statename, red, green, blue), 500);
        } catch (Error) {
        }
        return;
    }

    $(".dialling").hide();
    $(".preview").hide();

    var sn = statename.toLowerCase();
    if (
        sn == "active" ||
        sn == "predialled" ||
        sn == "disconnected" ||
        sn == "wrap" ||
        sn == "connecting" ||
        sn == "connected" ||
        sn == "unavailable"
    ) {
        // System States
        if (sn == "connecting") {
            hideBottomDispositionPanel();
            hideTopDispositionPanel();
            $(".preview").hide();
            $("#spanCustomerDDI").text("Agent");
            mCallControl.LogDisplay("DIALLING: SHOWING AS AGENT STATE CONNECTING");
            $(".dialling").show();
        } else if (statename.toLowerCase() == "preview") {
            if (mCallControl.LineState_Customer_String.toLowerCase() == "connecting") {
                mCallControl.LogDisplay("DIALLING: SHOWING AS AGENT STATE PREVIEW AND CUSTOMER LINE CONNECTING");
                $(".dialling").show();
                $(".preview").hide();
            } else {
                mCallControl.LogDisplay("DIALLING: SHOWING AS AGENT STATE PREVIEW AND CUSTOMER LINE NOT CONNECTING");
                $(".dialling").hide();
                $(".preview").show();
            }
        }
    } else {
        // custom states
        if (mCallControl.settingGet("HomePageURL") != "") {
            window.location = mCallControl.settingGet("HomePageURL");
        }
    }

    try {
        // The config can implement this function if it needs to perform any client specific actions
        mCallControl.LogDisplay("Ultra.js => Executing Custom_AgentStateChange(" + statename + ", " + red + "," + green + "," + blue + ")");
        Custom_AgentStateChange(statename, red, green, blue);
    } catch (Error) {
        mCallControl.LogDisplay("ERROR: " + Error);
    }
}


function OnDataReady(id) {
    if (id == mDispositionHistoryId) {
        DispositionHistoryLoaded();
    } else if (id == mNotesId) {
        NotesLoaded();
    } else if (id == mUsersId) {
        UsersLoaded();
    }
}


function UsersLoaded() {
    for (var d = 0; d < mCallControl.Data_RowCount("Framework.User"); d++) {
        users[mCallControl.Data_Value_ByName("Framework.User", "UserId", d)] =
            {
                Username: mCallControl.Data_Value_ByName("Framework.User", "Username", d),
                FirstName: mCallControl.Data_Value_ByName("Framework.User", "FirstName", d),
                LastName: mCallControl.Data_Value_ByName("Framework.User", "LastName", d)
            };


        if (mCallControl.Data_Value_ByName("Framework.User", "UserId", d) != mCallControl.UserId
            &&
            mCallControl.Data_Value_ByName("Framework.User", "DomainId", d) != "6")	// Ignore ultra users
        {
            $("#selectRescheduleUser").append($('<option>', {
                value: mCallControl.Data_Value_ByName("Framework.User", "UserId", d),

                text: mCallControl.Data_Value_ByName("Framework.User", "FirstName", d) + " " + mCallControl.Data_Value_ByName("Framework.User", "LastName", d)
            }));
        }
    }
}


function DispositionHistoryLoaded() {

    var html = "<div>There are no previous outcomes</div>";

    if (mCallControl.Data_RowCount("System.DispositionHistory") > 0) {
        html = "<div>Previous Outcomes:</div>";
        for (var x = mCallControl.Data_RowCount("System.DispositionHistory") - 1; x >= 0 && x >= mCallControl.Data_RowCount("System.DispositionHistory") - 4; x--) {
            html += "<div class=\"callHistoryRow\">";
            try {
                html += "<span class=\"disposition\">" + dispCodes[mCallControl.Data_Value_ByName("System.DispositionHistory", "DispositionReason", x)].Description + "</span> -";
            } catch (error) {
                html += "<span class=\"disposition\">" + mCallControl.Data_Value_ByName("System.DispositionHistory", "DispositionReason", x) + "</span> -";
            }
            html += "<span class=\"timeStamp\">" + mCallControl.Data_Value_ByName("System.DispositionHistory", "TimeStamp", x) + "</span>";
            try {
                var user = mCallControl.Data_Value_ByName("System.DispositionHistory", "CreatedUserId", x);
                html += "<span class=\"userName\"> by " + users[user].FirstName + " " + users[user].LastName + "</span>"
            } catch (Error) {
                html += "<span class=\"userName\"> by an unknown user</span>";
            }
            html += "</div>";
        }

        if (mCallControl.Data_RowCount("System.DispositionHistory") > 4) {
            var remaining = mCallControl.Data_RowCount("System.DispositionHistory") - 4;
            html += "<div class=\"additional\">There " + (remaining == 1 ? "is " : "are ") + remaining + " additional older outcome" + (remaining == 1 ? "" : "s") + "</div>";
        }
    }

    document.getElementById("spanHistory").innerHTML = html;
}


function NotesLoaded() {
    //var html = "<div class=\"note\"><div class=\"noteHeading\">2 August 2015 at 21:35 by James Sumner</div><div class=\"noteBody\">This is a note. I spoke to the customer and we discussed some things. It was all nice and good.</div></div>";
    //html = html + html + html + html + html;
    var html = "";
    for (var x = 0; x < mCallControl.Data_RowCount("System.ListRecordNotes"); x++) {

        var username = "";
        try {
            var user = mCallControl.Data_Value_ByName("System.ListRecordNotes", "AgentId", x);
            username = users[user].FirstName + " " + users[user].LastName;
        } catch (Error) {
            username = "an unknown user";
        }

        html += "<div class=\"note\">";
        html += "<div class=\"noteHeading\">" + mCallControl.Data_Value_ByName("System.ListRecordNotes", "TimeStampUTC", x) + " by " + username + "</div>";
        html += "<div class=\"noteBody\">" + mCallControl.XMLUnescape(mCallControl.Data_Value_ByName("System.ListRecordNotes", "Note", x)) + "</div>";
        html += "</div>";
    }

    $("#notesHistory").append(html);
}


function ReplaceURLPlaceHolders(url) {
    for (var x = 0; x < mCallControl.CallDetails_Count; x++) {
        var placeholder = "<#" + mCallControl.CallDetails_Key(x) + ">";
        url = url.replace(placeholder, mCallControl.CallDetails_Value_ByKey(mCallControl.CallDetails_Key(x)));
    }

    return url;
}


function MessageBox(heading, message) {
    $("#messageBoxTitle").text(heading);

    $("#messageBoxMessage").text(message);

    $("#divMaskMessageBox").fadeIn(250);

    $("#btnMessageBoxOk").click(function () {
        $("#divMaskMessageBox").fadeOut(250);
    });
}


function PopulateDispositionDropdown() {
    var dispositionGroup = "0";	// 0 Means no group in this instance

    dispositionGroup = mCallControl.SettingGet("DispositionGroup_Preview_Base" + mCallControl.CallDetails_Value_ByKey("BaseId"));

    if (dispositionGroup == "") {
        dispositionGroup = mCallControl.SettingGet("DispositionGroup_Preview");
    }


    if (dispositionGroup == "") {
        dispositionGroup = mCallControl.SettingGet("DispositionGroup_Base" + mCallControl.CallDetails_Value_ByKey("BaseId"));
    }


    if (dispositionGroup == "") {
        dispositionGroup = mCallControl.SettingGet("DispositionGroupOutbound");
    }


    if (dispositionGroup == "") {
        dispositionGroup = "0";
    }


    mCallControl.LogDisplay("Bound to disposition group " + dispositionGroup);

    if (dispositionGroup == "0") {
        $("#selectRescheduleReason").append($("<option>", {value: -1, text: "- SELECT -"}));

        for (x = 0; x < mCallControl.DispositionCodes_Count; x++) {
            // Populate the complete dispositions...
            $("#selectRescheduleReason").append($("<option>", {
                value: x,
                text: mCallControl.DispositionCodes_Value(x)
            }));
        }
    } else {
        for (x = 0; x < mCallControl.DispositionGroups_MemberCount(dispositionGroup); x++) {
            // The ID of the code we require
            var dispId;

            // CallControl stores the codes by their position, not their ID
            var dispPosition;

            // The category of this code
            var dispCategory;


            try {
                dispId = mCallControl.DispositionGroups_Member(dispositionGroup, x);

                // But it DOES resolve it for us at least!
                dispPosition = mCallControl.DispositionCodes_Position(dispId);

                dispCategory = mCallControl.DispositionCodes_Category(dispPosition);
            } catch (Error) {
                mCallControl.LogDisplay("PopulateDispositionDropDowns:" + Error);
            }

            $("#selectRescheduleReason").append($("<option>", {
                value: dispPosition,
                text: mCallControl.DispositionCodes_Value(dispPosition)
            }));
        }
    }
}


function btnPreviewDial_Click() {

    var ddi = mCallControl.ParseNumber($("#txtPreviewDDI").val());
    mCallControl.LogDisplay("Preview Dialling " + ddi);
    mCallControl.SettingSet("DiallingDDI", ddi);
    mCallControl.Preview_Dial_Manual(ddi);
}


function logout_Click() {
    $("#divPreviewMode").hide();

    $("#divRelease").show();
}


function dial_Click() {
    $("#divPreviewMode").show();

    $("#divRelease").hide();

    $("#divReassign").hide();

    $("#divReschedule").hide();
}


function reassign_Click() {
    $("#divPreviewMode").hide();

    $("#divReassign").show();
}


function reschedule_Click() {
    $("#divPreviewMode").hide();

    $("#divReschedule").show();
}


function btnRelease_Click() {
    mCallControl.Preview_Release();

    mCallControl.ChangeState($("#selectState").val());
}


function btnReassign_Click() {
    // Take this from the current schema. Only those lists which match will be displayed
    var schema = mCallControl.CurrentSchema;

    // Take this from the dropdown list which will contain only matching lists
    var baseid = $("#selectReassignList").val();

    var xml = "<upload clientid=\"" + mCallControl.ClientId + "\"><" + schema + " BaseIndexId=\"" + baseid + "\"><rows><insertOnly ";


    for (var x = 0; x < mCallControl.BaseRecordFieldCount(); x++) {
        if (mCallControl.BaseRecordField(x) != "RowId") {
            xml += mCallControl.BaseRecordField(x) + "=\"" + mCallControl.BaseRecordValue(x) + "\" ";
        }
    }

    xml += "/></rows></" + schema + "></upload>";


    //MessageBox("XML", xml);
    var response = mCallControl.comDataUpload(xml);

    var $response = $(response);

    if ($response.find('save').attr('rows') == "1")	// If the row was uploaded correctly...
    {
        // Log an outcome
        if (mCallControl.SettingGet("ReassignDisposition") != "") {
            mCallControl.comSaveDisposition(mCallControl.SettingGet("ReassignDisposition"), "");
        }

        mCallControl.Preview_Complete();

        // And activate
        mCallControl.comActivate("", false);
    } else {
        MessageBox("Error", $response.find('validate').attr('error'));
    }
}


function btnSubmitReschedule_Click() {
    mRescheduleDate = "";

    mRescheduleTime = "";

    mRescheduleUserId = "";

    // Validate the selections...
    mRescheduleUserId = 0;

    mRescheduleTime = $("#selectRescheduleHour").val() + ":" + $("#selectRescheduleMinute").val();

    mRescheduleDate = $("#textRescheduleDate").val();

    var x = $("#selectRescheduleReason").val();


    if (x < 0) {
        MessageBox("Please Select a Reason", "You must select a reason code to reschedule the task");

        return;
    }


    var selectedDisposition = {};

    selectedDisposition.DispositionReason = mCallControl.DispositionCodes_Key(x);

    selectedDisposition.Terminal = mCallControl.DispositionCodes_Terminal(x);

    selectedDisposition.Minutes = mCallControl.DispositionCodes_Minutes(x);

    selectedDisposition.Priority = mCallControl.DispositionCodes_Priority(x);


    if (selectedDisposition.Terminal == "0" && selectedDisposition.Minutes == "0") {
        if (mRescheduleDate == "") {
            MessageBox("Please Select a Date", "You have not selected a valid date for the task.");

            return;
        }
    }

    $("#divMaskSaving").fadeIn(250);


    if (dispositionLogged || mCallControl.comSaveDisposition(selectedDisposition.DispositionReason, "")) {
        dispositionLogged = true;
        if (selectedDisposition.Terminal == "0" && selectedDisposition.Minutes == "0") {
            mCallControl.LogDisplay("Logging Callback");

            // Set the callback (so it over-writes anything the disposition did)
            var sXML = "<Command>CallBack</Command>";
            sXML = sXML + "<Priority>" + selectedDisposition.Priority + "</Priority>";
            sXML = sXML + "<Schedule>" + mRescheduleDate + " " + mRescheduleTime + ":00</Schedule>";	// Always :00 seconds
            sXML = sXML + "<TimeZoneOffset>0</TimeZoneOffset>";
            sXML = sXML + "<DateFormat>DMY</DateFormat>";
            sXML = sXML + "<PhoneNumber></PhoneNumber>";
            sXML = sXML + "<ContactID>0</ContactID>";
            sXML = sXML + "<CallBackUserID>0</CallBackUserID>";
            sXML = sXML + "<BaseID>" + mCallControl.CallDetails_Value_ByKey("BaseId") + "</BaseID>";
            sXML = sXML + "<RowID>" + mCallControl.CallDetails_Value_ByKey("RowId") + "</RowID>";
            sXML = sXML + "<ScheduleType>2</ScheduleType>";

            if (mCallControl.sendClientFunction(sXML) > 0) {
                $("#divMaskSaving").fadeOut(250);

                mCallControl.LogDisplay("Error occurred setting callback");

                MessageBox("Error Rescheduling", "The task couldn't be rescheduled. Please ensure that the time you selected is in the future.");

                return;
            }
        }

        if (selectedDisposition.Terminal == "1") {
            // Call Preview.Release
            mCallControl.Preview_Complete();

        } else {
            // Call Preview.Release
            mCallControl.Preview_Release();
        }

        // And re-activate
        mCallControl.comActivate("", false);

        window.location = mCallControl.SettingGet("WaitingURL");
    } else {
        MessageBox("Error", "An error occured saving the disposition. Please try again.");

        $("#divMaskSaving").fadeOut(250);
    }
}


function btnCancelReschedule_Click() {
    $("#divPreviewMode").show();

    $("#divReschedule").hide();
}


/**
 * Card Payments Module
 */
var mAccountId = 1;					// This is the account to submit the payment to. It will normally be constant.

// Some variables to track what we've collected so far.
var
    validName,
    validAmount,
    validCard,
    validExpiry,
    validCVV,
    validIssue
        = false;

var mCapturing = "";

function Capture(item){
    if(mCapturing == ""){
        mCapturing = item;
        $(".CaptureButton").attr("disabled",true);

        $("#btnCapture" + item).html("Cancel");
        $("#btnCapture" + item).attr("disabled", false);

        $("#txt" + item).val("Capturing...");
        $("#txt" + item).removeClass("Capture_Valid, Capture_Invalid");
        $("#txt" + item).addClass("Capture_Capture",500);

        switch(item){
            case "CardNumber":
                mCallControl.LineCaptureDigits("Card Number", "", true, "Card Number", 0);
                break;
            case "Expiry":
                mCallControl.LineCaptureDigits("Expiry Date", "", true, "Expiry Date", 0);
                break;
            case "CVV":
                mCallControl.LineCaptureDigits("CVV", "", true, "CVV", 0);
                break;
        }

    }
    else if(mCapturing == item){
        mCallControl.LineCaptureCancel();
        $("#btnCapture" + item).html("Capture");
        $("#txt" + item).val("CANCELLED");
        $("#txt" + item).removeClass("Capture_Valid, Capture_Invalid, Capture_Capture");
        $("#txt" + item).addClass("Capture_Invalid",500);
        $(".CaptureButton").attr("disabled",false);
        mCapturing = "";
    }
}

function OnLineCapture(XML)
{
    mCallControl.LogDisplay("I saw: " + XML + " arrive from the capture");

    var xmlDoc = $.parseXML("<Response>" + XML + "</Response>");
    var $xml = $(xmlDoc);

    // On Each capture, the entire payload is passed through. For this reason we can update each item on each call safely.
    var $nodes = $xml.find("Data");

    $.each($nodes, function(index, item){

        var $item = $(item);

        var dataType = $item.find("DataType").text(),
            item = "",									// Which field are we dealing with
            valid = $item.find("Valid").text()=="0",	// 0 is valid, 1 is invalid
            value = $item.find("Value").text(),			// This can be configured to show the last 4 digits of the card number, but will normally be ****************
            cardType = "",								// Text description of card issuer
            paymentType = undefined;					// 0 is credit, 1 is debit

        switch(dataType){
            case "Card Number":
                item = "CardNumber";
                validCard = valid;
                cardType = $item.find("CardType").text();
                paymentType = $item.find("PaymentType").text();

                $("#CardType").html(cardType);

                break;
            case "Expiry Date":
                item = "Expiry";
                validExpiry = valid;
                break;
            case "CVV":
                item = "CVV";
                validCVV = valid;
                break;
        }

        $("#btnCapture" + item).html("Capture");
        $("#txt" + item).val((valid?"VALID":"INVALID"));
        $("#txt" + item).removeClass("Capture_Valid, Capture_Invalid, Capture_Capture");
        $("#txt" + item).addClass("Capture_" + (valid?"Valid":"Invalid"),500);
        $(".CaptureButton").attr("disabled",false);
    });

    mCapturing = false;
    ReadyToSubmit();
}

function Test(){

    var XML = "<Data><Valid>0</Valid><Terminator>#</Terminator><DataLength>0</DataLength><DataType>CVV</DataType><Hide>0</Hide><Name>CVV</Name><Value>***</Value></Data><Data><Valid>0</Valid><Terminator>#</Terminator><DataLength>0</DataLength><DataType>Expiry Date</DataType><Hide>0</Hide><Name>Expiry Date</Name><Value>****</Value></Data><Data><PaymentType>1</PaymentType><CardType>SWITCH</CardType><IssueNumber>0</IssueNumber><Valid>0</Valid><Terminator>#</Terminator><DataLength>0</DataLength><DataType>Card Number</DataType><Hide>0</Hide><Name>Card Number</Name><Value>************6542</Value></Data>"

    OnLineCapture(XML);
}

function ReadyToSubmit(){

    var ready = validAmount && validName && validCard && validExpiry && validCVV;
    mCallControl.logDisplay((ready?"":"Not ") + "ready to submit.");

    if(!ready){
        mCallControl.logDisplay(validAmount);
        mCallControl.logDisplay(validName);
        mCallControl.logDisplay(validCard);
        mCallControl.logDisplay(validExpiry);
        mCallControl.logDisplay(validCVV);
    }

    $("#btnSubmit").attr("disabled", !ready);
}

function btnSubmit_Click()
{
    mCallControl.LogDisplay("btnSubmit clicked");

    $(".CaptureButton").attr("disabled", true);
    $("#spanResponse").html("Please Wait...");
    $("#divRetry").css("display", "none");
    $(".masked").css("display", "block");

    var mTotal = txtAmount.value;
    mTotal = mTotal.replace(".", "");
    if (!(mCallControl.LineCaptureDigits("Amount", mTotal, false, "Amount", 0)
        && mCallControl.LineCaptureDigits("Name", txtName.value, false, "Name", 0)))
    {
        mCallControl.LogDisplay("WARNING: Couldn't set the amount or name");
        $("#spanResponse").html("Couldn't set the amount or name.");
        $("#divRetry").css("display", "block");
        return;
    }

    // Submit the payment to the correct account id
    mCallControl.LogDisplay("mAccountId: " + mAccountId);
    var response = mCallControl.ComSubmitPaymentWithNotes(mAccountId, "", "", "");

    mCallControl.LogDisplay("Payment Response: " + response);

    handleResponse(response);

}


function handleResponse(response)
{
    mCallControl.LogDisplay("handleResponse: " + response);
    switch (response)
    {
        case 0:   //PAYMENT_AUTHORISED
            $("#spanResponse").html("Payment Outcome: Payment Successful\n\nMessage: " + mCallControl.PaymentMessage);
            break;
        case -1:   // PAYMENT_DECLINED
            $("spanResponse").html("Payment Outcome: Payment Declined\n\nMessage: " + mCallControl.PaymentMessage);
            $("#divRetry").css("display", "block");
            break;
        case -2:  //PAYMENT_ERROR
            $("spanResponse").html("Payment Outcome: Error\n\nMessage: " + mCallControl.PaymentMessage);
            $("#divRetry").css("display", "block");
            break;
        case 1:   //PAYMENT_COMMFAILURE
            $("spanResponse").html("We're sorry but there has been a communication error with the payment provider.\n\nPlease inform your supervisor who should be able to check if the current transaction was authorised.");
            break;
        case 2:   //PAYMENT_NOORDERID
            break;
        default:
            $("spanResponse").html("Payment Outcome: Error\n\nMessage: " + mCallControl.PaymentMessage);
            $("#divRetry").css("display", "block");
            break;
    }
}

$(document).ready(function(){
    if(GetCallControl()){

        mCallControl.LogDisplay("Script Page Running PageLoad()");

        document.getElementById("divLoading").style.display = "none";
        document.getElementById("divNoCallControl").style.display = "none";
        document.getElementById("divCallControl").style.display = "block";

        $("#btnSubmit").attr("disabled", true);
        $("#CaptureField").val("Pending...");

        // Hookup events

        $("#txtName").change(function(){
            validName = $(this).val()!="";

            $(this).removeClass("Capture_Invalid Capture_Valid");
            $(this).addClass((validName?"Capture_Valid":"Capture_Invalid"), 500);
            ReadyToSubmit();
        });

        $("#txtAmount").change(function(){
            $(this).removeClass("Capture_Invalid Capture_Valid");

            var n = $(this).val();
            if(
                n!="" &&
                (!isNaN(parseFloat(n)) && isFinite(n)) &&
                n > 0
            )
            {
                validAmount=true;
            }
            else
            {
                validAmount=false;
            }

            $(this).addClass((validAmount?"Capture_Valid":"Capture_Invalid"),500);
            $("#errorAmount").text((validAmount?"":"Amount must be a number greater than zero."));
            ReadyToSubmit();
        });

        $("#btnCaptureCardNumber").click(function(){
            Capture("CardNumber");
        })
        $("#btnCaptureExpiry").click(function(){
            Capture("Expiry");
        })
        $("#btnCaptureCVV").click(function(){
            Capture("CVV");
        })
        $("#btnSubmit").click(function(){
            btnSubmit_Click();
        })

    }
    else{
        document.getElementById("divLoading").style.display = "none";
        document.getElementById("divNoCallControl").style.display = "block";
        document.getElementById("divCallControl").style.display = "none";
    }
});