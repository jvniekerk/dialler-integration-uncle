<?php
$page_title = 'Uncle Buck Finance Dialler'
?>


{{-- TODO: change full url links to url() --}}


        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <!-- load jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- provide the csrf token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>


    <title><?= $page_title; ?></title>


    <link rel="stylesheet" href="{{env("APP_URL")}}/data_view/data_view.css"/>


    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/css/Styles.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/css/modal.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/css/ub_styles.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/bootstrap-4.0.0/css/bootstrap.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/bootstrap-4.0.0/css/bootstrap-flex.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/bootstrap-4.0.0/css/bootstrap-grid.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/bootstrap-4.0.0/css/bootstrap-reboot.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/bootstrap-4.0.0/css/bootstrap-select.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/font-awesome-4.7.0/css/font-awesome.min.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/font-awesome-4.7.0/css/font-awesome.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/font-awesome-4.7.0/fonts/fontawesome-webfont.svg"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/css/waiting.css"/>

    <link rel="stylesheet"
          href="{{env("APP_URL")}}/call_bar/Externals/jquery-ui-1.11.2.custom/jquery-ui.css"/>
</head>


<div class="content">

    @yield('content')

</div>


</html>



