@extends('layout.layout')

<?php

use App\Services\reports\data_view_report;


//include_once "C:/Users/jvniekerk.UNCLEBUCK/Desktop/repos/dialler-integration-uncle/resources/reports/data_view.class.php";
//include_once "C:/Users/jvniekerk.UNCLEBUCK/Desktop/repos/dialler-integration-uncle/resources/reports/data_view_report.class.php";
//include_once "reports/data_view.class.php";
//include_once "reports/data_view_report.class.php";
//include_once "../reports/


$report_mins = (!empty($_GET['report_mins'])) ? htmlentities($_GET['report_mins']) : 120;


$download_data = new data_view_report('data_download', $report_mins);

$data_sync = new data_view_report('data_sync', $report_mins);

$data_cleanup = new data_view_report('data_cleanup', $report_mins);

$data_check = new data_view_report('data_check', $report_mins);

$error_log = new data_view_report('error_log', $report_mins);

$lists_accounts = new data_view_report('lists_accounts', $report_mins);

?>



@section('content')
    <body>

    <div class="container">
        <a href="#top"></a>


        <hr>


        <div style="text-align: center;">
            <img src="{{env("APP_URL")}}/call_bar/images/ub-logo.png"
                 alt="--Uncle Buck logo--"/>
        </div>


        <hr>
        <hr>


        <h1>Dialler Integration Processes Run Data</h1>


        <hr>
        <hr>


        <div>
            <?= $lists_accounts->get_report(); ?>
        </div>


        <hr>


        <div>
            <?= $data_sync->get_report(); ?>
        </div>


        <hr>


        <div>
            <?= $download_data->get_report(); ?>
        </div>


        <hr>


        <div>
            <?= $data_cleanup->get_report(); ?>
        </div>


        <hr>


        <div>
            <?= $data_check->get_report(); ?>
        </div>


        <hr>


        <div>
            <?= $error_log->get_report(); ?>
        </div>


        <hr>
    </div>

    </body>
@endsection