@extends('layout.layout')



@section('content')


    <?php

    //    function save_file($text, $filename, $file_type) {
    //        $text = is_array($text) ? json_encode($text) : $text;
    //        $filename = "C:/Users/jvniekerk.UNCLEBUCK/Desktop/test-files/test/dialler-integration/$filename.$file_type";
    //        $new_file = fopen($filename, "w");
    //        fwrite($new_file, $text);
    //        fclose($new_file);
    //        return $filename;
    //    }

    ?>


    <body onload="PageLoad()">


    <div class="container">
        <div id="divLoading" style="text-align: center;">
            <div style="text-align: center;">
                <img style="width:100px !important; margin:auto !important" alt="--Please wait gif--"
                     class="text-center"
                     src="{{env("APP_URL")}}/call_bar/images/Take-Control.gif">
                <p class="text-center">Please wait a moment...</p>
            </div>
        </div>


        <div id="divNoCallControl">
            This page doesn't work outside the Ultra Call Bar.
        </div>


        <div id="divCallControl">
            <div id="divOutcomesTop"></div>


            <div id="editPanel" onmouseover="enterEditPanel();">
                <h2>Edit Customer</h2>

                <hr>

                <span id="editFields"></span>

                <div id="editSave">
                    <span id="saveFeedback"></span>&nbsp;<span class="btn no" id="btnSave" onclick="btnSave_Click()">Save</span>
                </div>
            </div>


            {{-- Main Panel START --}}
            <div id="mainPanel" onmouseover="leaveEditPanel(); leaveNotesPanel();">
                <div id="heading">
                    <div style="text-align: center;">
                        <img src="{{env("APP_URL")}}/call_bar/images/ub-logo.png"
                             alt="--Uncle Buck logo--"/>
                    </div>


                    <div id="divHistory">
                        <span id="spanHistory"></span>
                    </div>
                </div>


                <hr>


                <h2 style="text-align: center;">Uncle Buck Finance Ultracomms Pop Screen</h2>


                <hr>


                <div class="col-xs-12 col-sm-12 col-md-12" style="margin-bottom: 1em;">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-3 col-sm-3 col-md-3"></div>


                        <div class="caller-details col-xs-3 col-sm-3 col-md-3">
                            <strong>Phone Number: </strong><span id="inbound_number" class="Bind_PhoneNumber"></span>
                        </div>


                        <div class="caller-details col-xs-3 col-sm-3 col-md-3">
                            <strong>Transaction ID: </strong><span class="Bind_TransactionId"
                                                                   id="transaction_id"></span>
                        </div>


                        <div class="col-xs-3 col-sm-3 col-md-3"></div>
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-3 col-sm-3 col-md-3"></div>


                        <div class="caller-details col-xs-3 col-sm-3 col-md-3 outgoing_call">
                            <strong>Loan Number: </strong><span id="LoanNo"><span id="loanno_value"
                                                                                  class="Bind_loanno"></span></span>
                        </div>


                        <div class="caller-details col-xs-3 col-sm-3 col-md-3 outgoing_call">
                            <strong>Dial Purpose: </strong><span id="ListID" class="Bind_dial_purpose"></span>
                        </div>


                        <div class="col-xs-3 col-sm-3 col-md-3"></div>
                    </div>
                </div>


                <div>
                    <span id="call_type" class="Bind_CallType"
                          style="display: none;"></span>
                </div>


                <div id="questions">
                    {{-- Question 1 => Greeting START --}}
                    <div id="1" class="col-xs-12 col-sm-12 text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12 greet">
                            <h4>Greeting</h4>


                            <p>
                                Good <span class="Static_Greeting"></span>, my name is
                                <span class="dataItem Bind_AgentFirstName"></span>
                            </p>


                            <p>
                                Can I please speak to
                                <strong>
                                    <span id="customer">
                                        <span class="Bind_title"></span>
                                        <span class="Bind_first_name"></span>
                                        <span class="Bind_surname"></span>
                                    </span>
                                </strong>?
                            </p>


                            <div class="col-xs-12 col-sm-12" style="margin-top: 2em;">
                                <div class="grid-container col-xs-12 col-sm-12" style="margin: 0.5em;">
                                    <div class="grid-item"></div>

                                    <div class="grid-item grid-middle grid-top">
                                        <button name="Yes" class="yes btn btn-middle" onclick="showQuestion(2)">
                                            Confirm Correct Person
                                        </button>
                                    </div>

                                    <div class="grid-item"></div>
                                    <div class="grid-item"></div>

                                    <div class="grid-item grid-middle">
                                        <button id="VMclose" name="VMclose" onclick="validate_next_question(1)"
                                                class="no btn btn-middle">
                                            No Answer / Voicemail
                                        </button>
                                    </div>

                                    <div class="grid-item"></div>
                                    <div class="grid-item"></div>

                                    <div class="grid-item grid-middle grid-bottom">
                                        <button name="No" onclick="showQuestion(5)" class="no btn btn-middle">
                                            Not Available / Wrong Number
                                        </button>
                                    </div>

                                    <div class="grid-item"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Question 1 => Greeting END --}}


                    {{-- Question 2 => DPA START --}}
                    <div id="2" class="question col-xs-12 col-sm-12 text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12 greet">
                            <h4>DPA</h4>


                            <p>
                                Hello <strong id="thedpa"><span class="Bind_title"></span> <span
                                            class="Bind_surname"></span></strong>, I am calling from Uncle Buck Finance.
                            </p>


                            <p>For security please confirm the following questions:</p>


                            {{-- Fullname START --}}
                            <div class="col-xs-12 col-sm-12 text-center">
                                <div class="col-xs-4 col-sm-4 col-md-4"></div>


                                <div class="col-xs-4 col-sm-4 col-md-4 btn yes" id="Fullname_div"
                                     style="background-color: grey;" onclick="check_toggle('Fullname');">
                                    <input type="checkbox" class="dpa" name="Fullname" id="Fullname"
                                           style="display: none;" onclick="check_toggle('Fullname');">

                                    <label for="Fullname" onclick="check_toggle('Fullname');">
                                        <strong>Full Name: </strong>
                                    </label>

                                    <span id="datapro1">
                                        <span class="Bind_title"></span>
                                        <span class="Bind_first_name"></span>
                                        <span class="Bind_surname"></span>
                                    </span>
                                </div>


                                <div class="col-xs-4 col-sm-4 col-md-4"></div>
                            </div>
                            {{-- Fullname END --}}


                            {{-- DOB START --}}
                            <div class="col-xs-12 col-sm-12 text-center">
                                <div class="col-xs-4 col-sm-4 col-md-4"></div>


                                <div class="col-xs-4 col-sm-4 col-md-4 btn yes" id="DOB_div"
                                     style="background-color: grey;" onclick="check_toggle('DOB');">
                                    <input type="checkbox" class="dpa" name="DOB" id="DOB" style="display: none;"
                                           onclick="check_toggle('DOB');">

                                    <label for="DOB" onclick="check_toggle('DOB');"><strong>DOB: </strong></label>

                                    <span id="datapro2">
                                        <span class="Bind_dob"></span>
                                    </span>
                                </div>


                                <div class="col-xs-4 col-sm-4 col-md-4"></div>
                            </div>
                            {{-- DOB END --}}


                            {{-- Address START --}}
                            <div class="col-xs-12 col-sm-12 text-center">
                                <div class="col-xs-4 col-sm-4 col-md-4"></div>


                                <div class="col-xs-4 col-sm-4 col-md-4 btn yes" id="Addr1_div"
                                     style="background-color: grey;" onclick="check_toggle('Addr1');">
                                    <input type="checkbox" class="dpa" name="Addr1" id="Addr1" style="display: none;"
                                           onclick="check_toggle('Addr1');">

                                    <label for="Addr1"
                                           onclick="check_toggle('Addr1');"><strong>Address: </strong></label>

                                    <span id="datapro3">
                                        <span class="Bind_house_number"></span>
                                        <span class="Bind_unit"></span>
                                        <span class="Bind_street"></span>
                                    </span>
                                </div>


                                <div class="col-xs-4 col-sm-4 col-md-4"></div>
                            </div>
                            {{-- Address END --}}


                            {{-- Postcode START --}}
                            <div class="col-xs-12 col-sm-12 text-center">
                                <div class="col-xs-4 col-sm-4 col-md-4"></div>


                                <div class="col-xs-4 col-sm-4 col-md-4 btn yes" id="PostCode_div"
                                     style="background-color: grey;" onclick="check_toggle('PostCode');">
                                    <input type="checkbox" class="dpa" name="PostCode" id="PostCode"
                                           style="display: none;" onclick="check_toggle('PostCode');">

                                    <label for="PostCode"
                                           onclick="check_toggle('PostCode');"><strong>Postcode: </strong>
                                    </label>

                                    <span id="datapro4"><span class="Bind_postcode"></span></span>
                                </div>


                                <div class="col-xs-4 col-sm-4 col-md-4"></div>
                            </div>
                            {{-- Postcode START --}}


                            {{-- Confirm DPA START --}}
                            <div class="col-xs-12 col-sm-12 col-md-12" id="dpaError">
                                <div class="col-xs-4 col-sm-4 col-md-4"></div>

                                <div class="col-xs-4 col-sm-4 col-md-4 error" style="font-style: italic;">
                                    Please confirm DPA to continue
                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4"></div>
                            </div>
                            {{-- Confirm DPA END --}}


                            <div id="ticked"></div>


                            <div>
                                <button class="back" onclick="showQuestion(1)" name="back">Back</button>

                                <button class="continue" onclick="showQuestion(3)" name="dpa_confirm" id="dpa_confirm"
                                        disabled>
                                    DPA Confirmed
                                </button>

                                <button class="not_continue" onclick="showQuestion(5)" name="dpa_not_confirmed"
                                        id="dpa_not_confirmed">
                                    DPA Not Confirmed
                                </button>
                            </div>
                        </div>
                    </div>
                    {{-- Question 2 => DPA END --}}


                    {{-- Question 3 => Memoranda START --}}
                    <div id="3" class="question col-xs-12 col-sm-12 text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12 greet">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <h4><strong>Collections Memoranda</strong></h4>


                                    <p>
                                        This call is being recorded for training and monitoring purposes and is an
                                        attempt
                                        to
                                        collect a debt.
                                    </p>


                                    <p>
                                        Any information obtained will be used strictly for this purpose.
                                    </p>
                                </div>


                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <h4><strong>Underwriting Memoranda</strong></h4>


                                    <p>
                                        Calls may be recorded for training and monitoring purposes.
                                    </p>
                                </div>
                            </div>


                            <div>
                                <button class="back" onclick="showQuestion(2)" name="back">
                                    Back
                                </button>


                                <button class="continue" onclick="showQuestion(4)" name="Memoranda">
                                    Memoranda Completed
                                </button>
                            </div>
                        </div>
                    </div>
                    {{-- Question 3 => Memoranda END --}}


                    {{-- Question 4 => Dispositions START --}}
                    <div id="4" class="question col-xs-12 col-sm-12 text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12 greet">
                            <h3>
                        <span id="FullName">
                            <span class="Bind_title"></span>
                            <span class="Bind_first_name"></span>
                            <span class="Bind_surname"></span>
                        </span>
                            </h3>


                            <p>
                                <span class="bold">Balance: </span>
                                <span id="BalanceCurrent">&pound;
                                    <span class="Bind_balance"></span>
                                </span>
                                <br>


                                <span class="bold">DOB: </span>
                                <span id="DOB">
                                    <span class="Bind_dob"></span>
                                </span>
                                <br>


                                <span class="bold">Address: </span>
                                <span id="Address">
                                    <span class="Bind_house_number"></span>
                                    <span class="Bind_unit"></span>
                                    <span class="Bind_street"></span>
                                </span>
                                <br>


                                <span class="bold">Postcode: </span>
                                <span id="Postcode">
                                    <span class="Bind_postcode"></span>
                                </span>
                                <br><br>


                                <span class="bold">Mobile number: </span>
                                <span id="PhoneMobile">
                                    <span class="Bind_PhoneNumber1"></span>
                                </span>
                                <br>


                                <span class="bold">Home phone number: </span>
                                <span id="PhoneHome">
                                    <span class="Bind_PhoneNumber2"></span>
                                </span>
                                <br>


                                <span class="bold">Work number: </span>
                                <span id="PhoneWork">
                                    <span class="Bind_PhoneNumber3"></span>
                                </span>
                            </p>


                            <div class="ptitle  col-md-12 col-xs-12 col-sm-12">
                                <div class="dropdown-container">
                                    <div class="button-container">
                                        <label for="Disposition4">Select a disposition</label>
                                        <select id="Disposition4" data-width="100%" onchange="show_reschedule(4);">
                                        </select>
                                    </div>


                                    <div class="button-container">
                                        <label for="agent_state4">When Done Go</label>
                                        <select id="agent_state4" data-width="100%">
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-4 col-xs-4 col-sm-4"></div>

                                <div id="DispositionError4" style="display: none;"
                                     class="error col-md-4 col-xs-4 col-sm-4">
                                    Please select a disposition
                                </div>

                                <div class="col-md-4 col-xs-4 col-sm-4"></div>
                            </div>
                            <br>


                            <div class="col-xs-12 col-sm-12">
                                <button class="back" onclick="showQuestion(3)" name="back">Back</button>

                                <button class="continue" onclick="validate_disposition(4)" name="Memoranda">
                                    End call
                                </button>
                            </div>
                        </div>
                    </div>
                    {{-- Question 4 => Dispositions END --}}


                    {{-- Question 5 => Wrong number/Not available START --}}
                    <div id="5" class="question col-xs-12 col-sm-12 text-center question">
                        <div class="col-md-12 col-sm-12 col-xs-12 greet">
                            <div class="" id="wdetail">
                                <p>
                                    <strong>Full Name: </strong>
                                    <span class="Bind_title" id="title"></span>
                                    <span class="Bind_first_name" id="first_name"></span>
                                    <span class="Bind_surname" id="surname"></span>
                                    <br><br>

                                    <strong>Address: </strong>
                                    <span class="Bind_house_number"></span>
                                    <span class="Bind_unit"></span>
                                    <span class="Bind_street"></span>
                                    <span class="Bind_postcode"></span>
                                </p>
                            </div>


                            <hr>


                            <div class="col-xs-12 col-sm-12" id="wrongNo">
                                <div class="row margin-in">
                                    <br>
                                    <div class="col-xs-6 col-sm-6 button-container">
                                        <label style="">Do they know the customer </label>

                                        <strong>
                                        <span id="wname">
                                            <span class="Bind_title"></span>
                                            <span class="Bind_first_name"></span>&nbsp;
                                            <span class="Bind_surname"></span>?
                                        </span>
                                        </strong>
                                    </div>
                                    <br>


                                    <div class="col-xs-6 col-sm-6 text-center  button-container">
                                        <select class="select-dropdown" id="wrongNo1">
                                            <option value="0">Please Select</option>
                                            <option value="Yes - Spouse or Family member">Yes - Spouse or Family member
                                            </option>
                                            <option value="Yes - Partner">Yes - Partner</option>
                                            <option value="Yes - Friend or Aquintance">Yes - Friend or Aquintance
                                            </option>
                                            <option value="Yes - Tenant">Yes - Tenant</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                    <br>
                                </div>


                                <hr>


                                <div class="row margin-in">
                                    <br>
                                    <div class="col-xs-6 col-sm-6 button-container">
                                        <label style="">Is this the right address?</label>

                                        <strong>
                                        <span id="waddress">
                                            <span class="Bind_house_number" id="house_number"></span>
                                            <span class="Bind_unit" id="unit"></span>
                                            <span class="Bind_street" id="street"></span>
                                            <span class="Bind_postcode" id="postcode"></span>
                                        </span>
                                        </strong>
                                    </div>
                                    <br>


                                    <div class="col-xs-6 col-sm-6 text-center button-container">
                                        <select class="select-dropdown" id="wrongNo2">
                                            <option value="0">Please Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="N/A - Details not given">N/A - Details not given</option>
                                            <option value="No - Address does not Match">No - Address does not Match
                                            </option>
                                        </select>
                                    </div>
                                    <br>
                                </div>


                                <hr>


                                <div class="row margin-in">
                                    <br>
                                    <div class="col-xs-6 col-sm-6  button-container">
                                        <label style="">Has the customer left the address?</label>
                                    </div>
                                    <br>


                                    <div class="col-xs-6 col-sm-6 text-center  button-container">
                                        <select class="select-dropdown" id="wrongNo3">
                                            <option value="0">Please Select</option>
                                            <option value="Yes - They left 1 week ago">Yes - They left 1 week ago
                                            </option>
                                            <option value="Yes - They left 2 weeks ago">Yes - They left 2 weeks ago
                                            </option>
                                            <option value="Yes - They left 1 month ago">Yes - They left 1 month ago
                                            </option>
                                            <option value="Yes - They left 3 months ago">Yes - They left 3 months ago
                                            </option>
                                            <option value="Yes - They left 6 months ago">Yes - They left 6 months ago
                                            </option>
                                            <option value="Yes - They left Over a Year ago">Yes - They left Over a Year
                                                ago
                                            </option>
                                            <option value="No - They still live there">No - They still live there
                                            </option>
                                            <option value="N/A - Wrong Address">N/A - Wrong Address</option>
                                            <option value="N/A - Customer Never lived there">N/A - Customer Never lived
                                                there
                                            </option>
                                        </select>
                                    </div>
                                    <br>
                                </div>


                                <hr>


                                <div class="col-xs-12 col-sm-12">
                                    <p>
                                        What was the actual address called and do they have any
                                        forwarding details?
                                    </p>


                                    <div>
                                        <label for="wrongNo4">Called address:</label>
                                    </div>

                                    <div>
                                        <input id="wrongNo4" type="text" name="wrongNo4" size="80"/>
                                    </div>
                                    <br>


                                    <div>
                                        <label for="wrongNo5">Forwarding address:</label>
                                    </div>

                                    <div>
                                        <input id="wrongNo5" type="text" name="wrongNo5" size="80"/>
                                    </div>
                                </div>


                                <div class="col-xs-12 col-sm-12 button-container">
                                    <hr>
                                    <div class="col-xs-4 col-sm-4 button-container"></div>

                                    <div class="col-xs-4 col-sm-4 button-container">
                                        <button id="traceTrace" class="btn no"
                                                onclick="make_copy_note($('#txtNotes2'))">
                                            Generate & copy note text to clipboard
                                        </button>
                                    </div>

                                    <div class="col-xs-4 col-sm-4 button-container"></div>

                                    {{-- TODO: add a link to create a new note in UNCLE here --}}
                                </div>


                                <div class="col-xs-12 col-sm-12 greet">
                                    <label for="txtNotes2">Note to copy to LMS</label>
                                    <br>

                                    <textarea spellcheck="true" id="txtNotes2" style="text-align: center;" readonly>
                                        Once the note text has been generated, copy the details from here by clicking the button above...
                                    </textarea>
                                    <br><br>


                                    {{--<button class="hist" onclick="toggleHistory(this.name)" name="History" disabled>Loading Notes...</button>--}}
                                    {{--<br>--}}


                                    {{--<div id="HistoryBox" name="History2Box" onkeyup="NotesTextChanged();"--}}
                                    {{--onblur="NotesTextChanged()" onpropertychange="NotesTextChanged()"></div>--}}
                                    {{--<br><br>--}}


                                    <div class="col-xs-12 col-sm-12">
                                        <div class="col-xs-12 col-sm-12">
                                            <label for="Disposition5">Select a disposition</label>
                                            <select id="Disposition5" data-width="100%" onchange="show_reschedule(5);">
                                            </select>
                                        </div>


                                        <div class="button-container">
                                            <label for="agent_state5">When Done Go</label>
                                            <select id="agent_state5" data-width="100%">
                                            </select>
                                        </div>


                                        <div class="col-xs-4 col-sm-4"></div>

                                        <div id="DispositionError5" style="display: none;"
                                             class="col-xs-4 col-sm-4 error">
                                            Please select a disposition
                                        </div>

                                        <div class="col-xs-4 col-sm-4"></div>
                                    </div>
                                    <br>


                                    <div class="col-xs-12 col-sm-12 button-container">
                                        <button class="back" onclick="showQuestion(1)" name="back">
                                            Back
                                        </button>


                                        <button class="continue" onclick="validate_disposition(5)" name="Memoranda">
                                            Close call
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Question 5 => Wrong number/Not available END --}}


                    {{-- Question 6 => Call closing START --}}
                    <div id="6" class="question col-xs-12 col-sm-12 text-center">
                        <div style="text-align: center;">
                            <img style="width:100px !important; margin:auto !important" class="text-center"
                                 src="{{env("APP_URL")}}/call_bar/images/Take-Control.gif">
                            <p class="text-center">Closing call, please wait a moment...</p>
                        </div>
                    </div>
                    {{-- Question 6 => Call closing END --}}


                    {{-- Question 7 => Incoming call START --}}
                    <div id="7" class="question col-xs-12 col-sm-12 text-center">
                        <div class="col-xs-12 col-sm-12 greet">
                            <h2 style="text-align: center;">Inbound Call</h2>


                            <p>
                                Good <span class="Static_Greeting"></span>, my name is
                                <strong><span class="Bind_AgentFirstName"></span></strong>
                            </p>


                            <div class="index dropdown-container col-xs-12 col-sm-12">
                                <div class="col-xs-12 col-sm-12" style="margin: 1em;">
                                    <div class="col-xs-12 col-sm-12">
                                        <label for="Disposition7">Select a disposition</label>
                                        <select id="Disposition7" data-width="100%" onchange="show_reschedule(7);">
                                        </select>
                                    </div>
                                    <br>


                                    <div class="button-container">
                                        <label for="agent_state7">When Done Go</label>
                                        <select id="agent_state7" data-width="100%">
                                        </select>
                                    </div>


                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="col-md-4 col-xs-4 col-sm-4"></div>

                                        <div id="DispositionError7" style="display: none;"
                                             class="error col-md-4 col-xs-4 col-sm-4">
                                            Please select a disposition
                                        </div>

                                        <div class="col-md-4 col-xs-4 col-sm-4"></div>
                                    </div>
                                </div>


                                <div class="col-md-12 col-xs-12 col-sm-12" style="margin: 1em;">
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <strong>Save Loan Number: </strong>
                                        <label for="incoming_loanno">Enter Loan Number/ID </label>
                                        <input type="text" id="incoming_loanno" style="text-align: center;"
                                               onchange="allocate_loanno(parseInt($(this).val()), false)">
                                    </div>


                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="col-md-4 col-xs-4 col-sm-4"></div>

                                        <div id="loanno_error" style="display: none;"
                                             class="error col-md-4 col-xs-4 col-sm-4">
                                            Please enter a valid LoanNo
                                        </div>

                                        <div class="col-md-4 col-xs-4 col-sm-4"></div>
                                    </div>


                                    <button class="continue" onclick="validate_disposition(7)" name="inbound">
                                        Save disposition and close call
                                    </button>


                                    <button class="not_continue" name="inbound"
                                            onclick="allocate_loanno(parseInt($('#incoming_loanno').val()), true)">
                                        Validate Loan Number / ID
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Question 7 => Incoming call END --}}


                    {{-- Question 8 => Dial outbound START --}}   {{-- Not to be used at present --}}
                    <div id="8" class=" question  col-xs-12 col-sm-12 text-center">
                        <div class="row">
                            <div class="preview col-sm-12 col-md-12 col-xs-12" id="divPreviewMode">
                                <div>
                                    <p style="font-weight:bold">DIAL</p>
                                </div>


                                <div>
                                    <p style="font-size:16px">
                                        Customers Number:
                                        <select id="selectCustomerDDI"></select>&nbsp;
                                        <input id="txtPreviewDDI" type="text">
                                        <span class="button previewingButton"
                                              onclick="btnPreviewDial_Click();">Dial</span>&nbsp;
                                        <span class="button previewingButton" style="background-color:#40AEFF"
                                              onclick="Preview_Releases();">Accept Inbound</span>
                                    </p>
                                </div>


                                <div>
                                    <button class="button previewingButton" onclick="reschedule_Click();">Reschedule
                                        Task
                                    </button>
                                </div>
                            </div>
                        </div>


                        <div id="customerPreview" class="prevQ row ">
                            <h3>&nbsp;&nbsp;
                                <span id="pFullName"><span class="Bind_Title"></span>
                                <span class="Bind_first_name"></span>
                                <span class="Bind_surname"></span>
                            </span>


                                <button style="position: relative; margin-top:-40px !important;float:right !important;outline:none !important;text-decoration:none !important;border:none !important; background-color:#ffffff !important;"
                                        data-toggle="modal" data-target="#addPreviewNote">
                                    <i style="float:right !important; font-size:50px !important; font-weight:600 !important; color:#50b948 !important; margin-top:0px;margin-right:7px;"
                                       class="fa fa-pencil-square-o"></i>
                                </button>
                            </h3>


                            <hr>


                            <div style="padding-right:15px" class="col-xs-12 col-sm-6">
                                <span class="bold">Balance: </span>
                                <span id="BalanceCurrent1">&pound;
                                <span class="Bind_balance"></span>
                            </span>
                                <br>


                                <p>
                                    <span class="bold">DOB: </span>
                                    <span id="pDOB">
                                    <span class="Bind_dob"></span>
                                </span>
                                    <br>


                                    <span class="bold">Address: </span>
                                    <span id="pAddress">
                                    <span class="Bind_house_number"></span>
                                    <span class="Bind_unit"></span>
                                    <span class="Bind_street"></span>
                                    <span class="Bind_postcode"></span>
                                </span>
                                    <br>


                                    <span class="bold">Mobile Number: </span>
                                    <span id="pPhoneMobile">
                                    <span class="Bind_PhoneNumber1"></span>
                                </span>
                                    <br>


                                    <span class="bold">Home Number: </span>
                                    <span id="pPhoneHome">
                                    <span class="Bind_PhoneNumber2"></span>
                                </span>
                                    <br>


                                    <span class="bold">Work Number: </span>
                                    <span id="pPhoneMobile">
                                    <span class="Bind_PhoneNumber3"></span>
                                </span>
                                    <br>
                                </p>
                            </div>


                            {{--<div style="text-align:center;" class="row">--}}
                            {{--<button id="hnote" class="hist" onclick="toggleHistory(this.name)" name="History3" disabled>--}}
                            {{--Loading Notes...--}}
                            {{--</button>--}}
                            {{--<br>--}}
                            {{--<div id="History3Box" name="History3Box" onkeyup="NotesTextChanged();"--}}
                            {{--onblur="NotesTextChanged()"--}}
                            {{--onpropertychange="NotesTextChanged()"></div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    {{-- Question 8 => Dial outbound END --}}


                    {{-- Question 9 => Predial START --}}   {{-- Not to be used at present --}}
                    <div id="9" class="question col-xs-12 col-sm-12 text-center">
                        <p>
                            You are now in <strong style="color:green">Predialled</strong>.
                        </p>


                        <p>
                            Please click <strong style="color:green">Ready</strong> via the call control to connect to
                            an
                            <strong style="color:green">Inbound</strong> call.
                        </p>
                    </div>
                    {{-- Question 9 => Predial END --}}
                </div>


                {{-- UNLCE link START --}}
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-xs-4 col-sm-4 col-md-4 greet"></div>


                    <div id="loan_link_div" class="col-xs-4 col-sm-4 col-md-4"
                         style="display: none;">    {{-- $('#loan_link') --}}
                        <a id="loan_link" class="uncle-link"
                           onclick="$(this).prop('href', 'microsoft-edge:{{env("UNCLE_URL")}}/loans/' + $('#loanno_value').text());">
                            Go to Loan Screen in UNCLE LMS
                        </a>
                    </div>


                    <div class="col-xs-4 col-sm-4 col-md-4"></div>
                </div>
                {{-- UNLCE link END --}}


                <hr>


                {{-- Payment Capture START --}}
                <div id="divCaptureControls" class="col-xs-12 col-sm-12">
                    <div class="col-xs-12 col-sm-12">
                        <div class="col-xs-4 col-sm-4"></div>

                        <div class="col-xs-4 col-sm-4">
                            <button class=" btn no" onclick="$('#card_control').toggle();">
                                Toggle Payment Details
                            </button>
                        </div>

                        <div class="col-xs-4 col-sm-4"></div>
                    </div>


                    <div class="col-xs-12 col-sm-12">
                        <div class="col-xs-4 col-sm-4"></div>


                        <div id="card_control" class="col-xs-4 col-sm-4 card-container" style="display: none;">
                            <table>
                                <tr>
                                    <td><span class="Prompt">Name on Card: </span></td>
                                    <td><input type="text" id="txtName" class="EntryField"></td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><span class="Prompt">Amount: (GBP)</span></td>
                                    <td><input type="text" id="txtAmount" class="EntryField"></td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td colspan="3"><span id="errorAmount" class="errorMessage"></span></td>
                                </tr>

                                <tr>
                                    <td><span class="Prompt">Card Number: </span></td>
                                    <td><input type="text" id="txtCardNumber" class="CaptureField" readonly></td>
                                    <td>
                                        <button class="CaptureButton" id="btnCaptureCardNumber">Capture</button>
                                    </td>
                                </tr>

                                <tr>
                                    <td><span class="Prompt">Card Type:</span></td>
                                    <td colspan="2"><span class="Prompt" id="CardType">N/A</span></td>
                                </tr>

                                <tr>
                                    <td><span class="Prompt">Expiry Date: </span></td>
                                    <td><input type="text" id="txtExpiry" class="CaptureField" readonly></td>
                                    <td>
                                        <button class="CaptureButton" id="btnCaptureExpiry">Capture</button>
                                    </td>
                                </tr>

                                <tr>
                                    <td><span class="Prompt">CVV:</span></td>
                                    <td><input type="text" id="txtCVV" class="CaptureField" readonly></td>
                                    <td>
                                        <button class="CaptureButton" id="btnCaptureCVV">Capture</button>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                        <button id="btnSubmit" class="CaptureButton">Submit</button>
                                    </td>
                                </tr>
                            </table>


                            <div id="divMask" class="masked"></div>


                            <div id="divResponse" class="masked">
                                <div>
                                    <span id="spanResponse">Please Wait...</span>
                                </div>


                                <div id="divRetry">
                                    <button id="btnRetry">Continue</button>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-4 col-sm-4"></div>
                    </div>
                </div>

                {{-- Payment Capture END --}}


                {{-- Help START --}}
                <div id="help">
                    <div class="col-xs-12 col-sm-12">
                        <div class="col-xs-3 col-sm-3"></div>


                        <div class="col-xs-6 col-sm-6">
                            <div class="yes btn_div col-xs-12 col-sm-12" style="margin-bottom: 1em; margin-top: 1em;">
                                <div class="col-xs-6 col-sm-6 button-container">
                                    {{--<button class="fa fa-question-circle no btn" id="infoicon"--}}
                                    <button class="no btn" id="infoicon"
                                            style="font-size: 20px; text-align: center;"
                                            onclick="$('#HelpHome').toggle();">
                                        &#9072; Show Help Section
                                    </button>
                                </div>


                                <div class="col-xs-6 col-sm-6 button-container">
                                    <button class="btn no" onclick="goto_wrap();">
                                        Go To Wrap
                                    </button>
                                </div>
                            </div>


                            <div class="yes btn_div col-xs-12 col-sm-12" id="outgoing_buttons">
                                <div class="col-xs-6 col-sm-6 button-container">
                                    <button class="btn no" onclick="showQuestion(1);">
                                        Show Greeting Screen
                                    </button>
                                </div>


                                <div class="col-xs-6 col-sm-6 button-container">
                                    <button id="reschedule_toggle" class="btn no" onclick="show_reschedule(23);">
                                        Reschedule Call
                                    </button>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-3 col-sm-3"></div>
                    </div>


                    {{-- Question HelpHome => Help START --}}
                    <div id="HelpHome" class="question col-xs-12 col-sm-12 text-center" style="margin-top: 2em;">
                        <hr>


                        <div style="text-align: center;">
                            <h1>Help Section</h1>
                        </div>


                        <hr>


                        {{-- Help Images START --}}
                        <div id="help_images" class="col-xs-12 col-sm-12">
                            <div style="text-align: center;">
                                <h2>Help Images</h2>
                            </div>


                            <div class="col-xs-3 col-sm-3">
                                <div id="5th_in_12_call_flow" style="display: none;">
                                    <h3 style="text-align: center;">5th in 12 Call Flow</h3>

                                    <img id="img_5th_in_12_call_flow" class="modal_image"
                                         src="{{env("APP_URL")}}/call_bar/help/5th_in_12_call_flow.jpg"
                                         alt="5th in 12 Call Flow" style="width: 100%; max-width: 300px;"
                                         onclick="show_modal($(this).attr('id'));"
                                    >
                                </div>
                                <br>

                                <button class="btn" onclick="$('#5th_in_12_call_flow').toggle();">
                                    Toggle 5th in 12 Image
                                </button>
                                <br>
                            </div>


                            <div class="col-xs-3 col-sm-3">
                                <div id="call_centre_script" style="display: none;">
                                    <h3 style="text-align: center;">Call Centre Script</h3>


                                    <img id="img_call_centre_script" class="modal_image"
                                         src="{{env("APP_URL")}}/call_bar/help/call_centre_script.png"
                                         alt="Call Centre Script" style="width: 100%; max-width: 300px;"
                                         onclick="show_modal($(this).attr('id'));"
                                    >
                                </div>
                                <br>

                                <button class="btn" onclick="$('#call_centre_script').toggle();">
                                    Toggle Call Centre Script Image
                                </button>
                                <br>
                            </div>


                            <div class="col-xs-3 col-sm-3">
                                <div id="stages_guide" style="display: none;">
                                    <h3 style="text-align: center;">Stages Guide</h3>

                                    <img id="img_stages_guide" class="modal_image"
                                         src="{{env("APP_URL")}}/call_bar/help/stages_guide.png"
                                         alt="Stages Guide" style="width: 100%; max-width: 300px;"
                                         onclick="show_modal($(this).attr('id'));"
                                    >
                                </div>
                                <br>

                                <button class="btn" onclick="$('#stages_guide').toggle();">
                                    Toggle Stages Guide Image
                                </button>
                                <br>
                            </div>


                            <div class="col-xs-3 col-sm-3">
                                <div id="process_guide" style="display: none;">
                                    <h3 style="text-align: center;">Process Guide</h3>

                                    <img id="img_process_guide" class="modal_image"
                                         src="{{env("APP_URL")}}/call_bar/help/process_guide.png"
                                         alt="Process Guide" style="width: 100%; max-width: 300px;"
                                         onclick="show_modal($(this).attr('id'));"
                                    >
                                </div>
                                <br>

                                <button class="btn" onclick="$('#process_guide').toggle();">
                                    Toggle Process Guide Image
                                </button>
                            </div>


                            {{-- Image Modal --}}
                            <div id="help_image_modal" class="modal" onclick="$(this).hide();">
                                <span class="close_image_modal" id="close_image_modal">&times;</span>

                                <img class="modal_image_popped" id="help_modal_image"
                                     onclick="$('#help_image_modal').hide();">

                                <div id="modal_image_caption"></div>
                            </div>
                        </div>
                        {{-- Help Images END --}}


                        <br><br>


                        {{-- Help Videos START --}}
                        <div id="help_videos" class="col-xs-12 col-sm-12">
                            <div style="text-align: center;">
                                <h2>Help Videos</h2>
                            </div>


                            <div class="col-xs-3 col-sm-3">
                                <div id="preview_dialler_full_clip" style="display: none;">
                                    <h3 style="text-align: center;">Preview Dialler Full clip</h3>

                                    <video width="320" height="240" controls>
                                        <source src="{{env("APP_URL")}}/call_bar/help/preview_dialler_full_clip.mp4"
                                                type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                                <br>

                                <button class="btn" onclick="$('#preview_dialler_full_clip').toggle();">
                                    Toggle "Preview Dialler Full Clip"
                                </button>
                                <br>
                            </div>


                            <div class="col-xs-3 col-sm-3">
                                <div id="preview_dialler_not_available" style="display: none;">
                                    <h3 style="text-align: center;">Preview Dialler Not Available</h3>

                                    <video width="320" height="240" controls>
                                        <source src="{{env("APP_URL")}}/call_bar/help/preview_dialler_not_available.mp4"
                                                type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                                <br>

                                <button class="btn" onclick="$('#preview_dialler_not_available').toggle();">
                                    Toggle "Preview Dialler Not Available" Video
                                </button>
                                <br>
                            </div>


                            <div class="col-xs-3 col-sm-3">
                                <div id="preview_dialler_right_party_contact" style="display: none;">
                                    <h3 style="text-align: center;">Preview Dialler Right party connect</h3>

                                    <video width="320" height="240" controls>
                                        <source src="{{env("APP_URL")}}/call_bar/help/preview_dialler_right_party_contact.mp4"
                                                type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                                <br>

                                <button class="btn" onclick="$('#preview_dialler_right_party_contact').toggle();">
                                    Toggle "Preview Dialler Right Party Contact" Video
                                </button>
                                <br>
                            </div>


                            <div class="col-xs-3 col-sm-3">
                                <div id="preview_dialler_wrong_number" style="display: none;">
                                    <h3 style="text-align: center;">Preview Dialler Wrong Number</h3>

                                    <video width="320" height="240" controls>
                                        <source src="{{env("APP_URL")}}/call_bar/help/preview_dialler_wrong_number.mp4"
                                                type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                                <br>

                                <button class="btn" onclick="$('#preview_dialler_wrong_number').toggle();">
                                    Toggle "Preview Dialler Wrong Number" Video
                                </button>
                            </div>
                        </div>
                        {{-- Help Videos END --}}
                    </div>
                    {{-- Question HelpHome => Help END --}}
                </div>
                {{-- Help END --}}
            </div>
            {{-- Main Panel END --}}


            <div id="notesPanel" onmouseover="enterNotesPanel();">
                <h2>Caller Notes</h2>


                <hr/>


                Please enter some notes below.
                <br>


                <textarea id="txtNotes" onkeyup="NotesTextChanged();" onblur="NotesTextChanged()"
                          onpropertychange="NotesTextChanged()"></textarea>
                <br>


                <span style="font-size:smaller">You can use <span id="spanCharCount">4000</span> more characters.</span>


                <div id="notesSave">
                    <span id="notesSaveMessage"></span>&nbsp;<span class="button"
                                                                   onclick="btnSaveNotes_Click()">Save</span>
                </div>


                <hr/>


                Previous Notes <span style="font-size:smaller">(newest first)</span>

                <div id="notesHistory"></div>
            </div>


            <div id="divOutcomesBottom"></div>
        </div>


        <div id="divEmulation">EMULATION MODE</div>


        <div id="divDatePicker"></div>


        <div class="preview" id="divPreviewMode">
            <div>PREVIEWING</div>


            <div>Customers Number:
                <select id="selectCustomerDDI">
                    <option value="Other">Other</option>
                </select>&nbsp;

                <input id="txtPreviewDDI" type="text">

                <span class="button previewingButton" onclick="btnPreviewDial_Click();">Dial</span>
            </div>


            <div>
                <button class="button previewingButton" onclick="logout_Click();">Logout</button>
                &nbsp;
                <button class="button previewingButton" onclick="reassign_Click();">Reassign Task</button>
                &nbsp;
                <button class="button previewingButton" onclick="reschedule_Click();">Reschedule Task</button>
            </div>
        </div>


        <div class="preview" id="divRelease">
            <div>RELEASE RECORD -Select State</div>


            <div>
                <select id="selectState"></select>&nbsp;
                <span class="button previewingButton" onclick="btnRelease_Click();">Log Out</span>
            </div>


            <div style="font-size:10pt; font-style:italic; font-weight: normal; height:30px;">
                The record will be re-allocated to you when you log back in.
            </div>


            <div>
                <button class="button previewingButton" onclick="dial_Click();">Cancel</button>
            </div>
        </div>


        <div id="divReassign">
            <div>Reassign Task</div>


            <div>
                <select id="selectReassignList"></select>&nbsp;
                <span class="button previewingButton" onclick="btnReassign_Click();">Reassign</span>
            </div>


            <div style="font-size:10pt; font-style:italic; font-weight:normal;">
                The record will be added to the selected list
            </div>


            <div>
                <button class="button previewingButton" onclick="dial_Click();">Cancel</button>
            </div>
        </div>


        <div id="divReschedule" class="reschedule">
            <div>
                Reschedule or Complete Task
            </div>


            <div>
                Reason for Reschedule: <select id="selectRescheduleReason">
                    {{--<option value="13">TEST reason 13</option>--}}
                    {{--<option value="23">TEST reason 23</option>--}}
                </select>
            </div>


            <div id="enlarge_reschedule" class="col-xs-12 col-sm-12" style="display: none;">
                <div class="col-xs-4 col-sm-4"></div>

                <div class="col-xs-4 col-sm-4">
                    <button class="btn yes" onclick="enlarge_reschedule();">
                        Enlarge Reschedule Screen
                    </button>
                </div>

                <div class="col-xs-4 col-sm-4"></div>
            </div>


            <div>
                <span id="spanRescheduleTip" style="font-size:small">Please Select an Option</span>
            </div>


            {{--<div id="divReschedule_Time" style="display:none;">--}}
            <div id="divReschedule_Time">
                Reschedule For: <input id="textRescheduleDate" type="text"></input> @
                <select id="selectRescheduleHour"></select> :
                <select id="selectRescheduleMinute"></select>
            </div>


            <div id="divRescheduleButtons">
                <button class="button previewingButton" id="btnCancelReschedule" onclick="dial_Click();">Cancel</button>

                <button class="button previewingButton" id="btnSubmitReschedule" onclick="btnSubmitReschedule_Click();">
                    Submit
                </button>
            </div>
        </div>


        <div class="dialling" id="divDialling">
            Dialling <span id="spanCustomerDDI"></span>...
        </div>


        <div id="divMaskMessageBox">
            <div id="divMessageBox">
                <div>
                    <h1 id="messageBoxTitle"></h1>
                </div>


                <hr/>


                <div>
                    <span id="messageBoxMessage"></span>
                </div>


                <hr/>


                <div class="dialogButtons">
                    <button id="btnMessageBoxOk">OK</button>
                </div>
            </div>
        </div>


        <div id="divMaskSaving">
            <div id="divSaving">
                <h1>Saving...</h1>
            </div>
        </div>
        <div id="divMaskSaving">
            <div id="divSaving">
                <h1>Saving...</h1>
            </div>
        </div>
    </div>


    <script src="{{env("APP_URL")}}/call_bar/Externals/jquery-ui-1.11.2.custom/external/jquery/jquery.js"></script>

    <script src="{{env("APP_URL")}}/call_bar/Externals/jquery-ui-1.11.2.custom/jquery-ui.js"></script>

    <script language="javascript" type="text/javascript"
            src="{{env("APP_URL")}}/call_bar/js/Ultra.js"></script>

    <script language="javascript" type="text/javascript"
            src="{{env("APP_URL")}}/call_bar/js/Config.js"></script>

    <script language="javascript" type="text/javascript"
            src="{{env("APP_URL")}}/call_bar/js/EmulateCallControl.js"></script>

    <script language="javascript" type="text/javascript"
            src="{{env("APP_URL")}}/call_bar/js/ub_functions.js"></script>


    </body>


@endsection




