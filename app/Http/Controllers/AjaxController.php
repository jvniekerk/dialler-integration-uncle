<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AjaxController extends Controller {
    public function post(Request $request) {
        $transaction_id = (int)htmlentities($request->transaction_id);
        $loanno = (int)htmlentities($request->loanno);


        $response = array(
            'status'              => 'error',
            'loanno'              => $loanno,
            'transaction_id'      => $transaction_id,
            'loanno_type'         => gettype($loanno),
            'transaction_id_type' => gettype($transaction_id),
        );


        if (is_int($loanno) && is_int($transaction_id)) {
            if ($loanno > 0 && $transaction_id > 0) {
                $sql = "
                insert ignore into incoming_calls_loanno
                (
                    created_at,
                    updated_at,
                    transaction_id,
                    loanno
                )
                select now(), now(), ?, ?;";

                DB::insert($sql, [$transaction_id, $loanno]);

                $response['status'] = 'success';
            }
        }

        return response()->json($response);
    }
}
