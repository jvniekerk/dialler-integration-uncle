<?php

namespace App\Services\reports;


class data_view_report {
    /**
     * Form to refresh the report page
     *
     * @var string // html
     */
    private $refresh_form;

    /**
     * Reports the cycles of the process run
     * @var string
     */
    private $cycles_report;

    /**
     * The summary report
     * @var string
     */
    private $summary_report;

    /**
     * Each step of the process cycles
     * @var string
     */
    private $steps_report;

    /**
     * Number of minutes to request the report data for
     * @var integer
     */
    private $report_mins;

    /**
     * Report to display
     * @var string
     */
    private $report;

    /**
     * @var string
     */
    private $process_type;

    /**
     * Details of the report
     * @var array string
     */
    private $report_details = array();

    /**
     * Different processes
     * @var array
     */
    private $processes = array(
        'Call Attempts' => 'call_attempts',
        'Dispositions'  => 'dispositions',
        'Agent States'  => 'agent_states'
    );


    /**
     * data_view_report constructor.
     *
     * @param $process_type
     * @param $report_mins
     */
    public function __construct($process_type, $report_mins) {
        $this->report_mins = $report_mins;

        $this->process_type = $process_type;

        $this->set_refresh_form();

        $this->set_report_details();

        $this->set_reports();
    }


    /**
     * Sets the report details
     */
    private function set_report_details() {
        if ($this->process_type == 'data_download') {
            $this->report_details = array(
                'reports_header' => 'Data Download',
                'process'        => 'Data download procedure'
            );

            return;
        }


        if ($this->process_type == 'data_sync') {
            $this->report_details = array(
                'reports_header' => 'Data Sync',
                'process'        => 'Data sync procedure'
            );

            return;
        }


        if ($this->process_type == 'data_cleanup') {
            $this->report_details = array(
                'reports_header' => 'Old Accounts Deleted',
                'process'        => 'Delete old accounts procedure'
            );

            return;
        }


        if ($this->process_type == 'data_check') {
            $this->report_details = array(
                'reports_header' => 'Account State Check',
                'process'        => 'Account states check procedure'
            );

            return;
        }


        if ($this->process_type == 'error_log') {
            $this->report_details = array(
                'reports_header' => 'Integration Errors',
                'process'        => 'Error log'
            );

            return;
        }


        if ($this->process_type == 'lists_accounts') {
            $this->report_details = array(
                'reports_header' => 'Lists Accounts Summary',
                'process'        => 'List accounts'
            );

            return;
        }
    }


    /**
     * Creates the form to refresh the report
     */
    private function set_refresh_form() {
        $top = "<div style='float: right;'><button onclick='location.href = \"#top\"'>TOP</button></div>";


        $this->refresh_form = "
            <div>
                <form method='get'>
                    Minutes for report: <input type='text' value='$this->report_mins' name='report_mins' style='text-align: center;'>
                    <input type='submit' value='Refresh'>
                </form>
            </div>
            $top
            <br>
            ";
    }


    /**
     * Sets the report
     */
    private function set_report() {
        $cycles = $this->process_type == 'error_log' || $this->process_type == 'lists_accounts' ? '' : ' Cycles';

        $id = str_replace(' ', '_', $this->process_type);

        $this->report = "

            " . str_replace("<form method='get'>", "<form method='get' id='$id' action='#$id'>", $this->refresh_form) . " 

            <h2>" . $this->report_details['reports_header'] . "$cycles</h2>

            $this->cycles_report 

            ";

        if ($this->process_type != 'error_log' && $this->process_type != 'lists_accounts') {
            $this->report .= "
            <h3>" . $this->report_details['reports_header'] . " Summary</h3>

            $this->summary_report


            <h3>" . $this->report_details['reports_header'] . " Steps</h3>

        $this->steps_report";


            if ($this->process_type == 'data_download') {
                $this->report .= $this->add_summary_report();
            }
        }
    }


    /**
     * Get today's activities
     * @param $process
     *
     * @return string
     */
    private function get_activities_today($process) {
        $report = new data_view($this->process_type, $process, $this->report_mins, false, true);

        return $report->get_report_table();
    }


    /**
     * Get today's activities by list
     * @param $process
     *
     * @return string
     */
    private function get_activities_today_by_list($process) {
        $report = new data_view($this->process_type, $process, $this->report_mins, false, true, true);

        return $report->get_report_table();
    }


    /**
     * Adds the summary reports to the overall report
     * @return string
     */
    private function add_summary_report() {
        $add_report = "";


        foreach ($this->processes as $key => $process) {
            $id = str_replace(' ', '_', $this->process_type) . "_$process";


            $add_report .= "

            
            " . str_replace("<form method='get'>", "<form method='get' id='$id' action='#$id'>", $this->refresh_form) . "

            <h3>$key Download Cycles</h3>

            " . $this->get_process_summary_reports($process) . "
            
            
            <h3>$key Today</h3>
                
            " . $this->get_activities_today($process) . "
            
            
            <h3>$key Today by List</h3>

            " . $this->get_activities_today_by_list($process) . "


            ";
        }

        return $add_report;
    }


    /**
     * Returns the summary report table
     * @param $process
     *
     * @return string
     */
    private function get_process_summary_reports($process) {
        $report = new data_view($this->process_type, $process, $this->report_mins);

        return $report->get_report_table();
    }


    /**
     * Returns the report
     * @return string
     */
    public function get_report() {
        return $this->report;
    }


    /**
     * Creates the report
     */
    private function set_reports() {
        $this->set_download_cycles();


        if ($this->process_type != 'error_log' && $this->process_type != 'lists_accounts') {
            $this->set_download_summary();

            $this->set_download_steps();
        }


        $this->set_report();
    }


    /**
     * Sets the cycles report
     */
    private function set_download_cycles() {
        $report = new data_view($this->process_type, $this->report_details['process'], $this->report_mins);

        $this->cycles_report = $report->get_report_table();
    }


    /**
     * Sets the summary report
     */
    private function set_download_summary() {
        $report = new data_view($this->process_type, $this->report_details['process'], $this->report_mins, true);

        $this->summary_report = $report->get_report_table();
    }


    /**
     * Sets the steps report
     */
    private function set_download_steps() {
        $report = new data_view($this->process_type, '', $this->report_mins);

        $this->steps_report = $report->get_report_table();
    }
}