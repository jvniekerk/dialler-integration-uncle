<?php

namespace App\Services\reports;

use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: jvniekerk
 * Date: 20/02/2019
 * Time: 20:23
 */
//use Illuminate\Support\Facades\DB;

class data_view {
    /**
     * Number of minutes for which the report should be shown
     * @var int|string
     */
    private $report_mins = 120;

    /**
     * Type of process the report is for, e.g. data download, data sync etc
     * @var string
     */
    private $process_type;

    /**
     * Type of process type the report is for, e.g. call attempts for data download etc
     * @var string
     */
    private $process = '';

    /**
     * Data returned from the database for the specified report
     * @var array
     */
    private $report_array;

    /**
     * Number of rows in the report
     * @var integer
     */
    private $report_rows;

    /**
     * Table to display for the report
     * @var string (HTML table)
     */
    private $report_table;

    /**
     * Columns on the report
     * @var string
     */
    private $columns = "";

    /**
     * Criteria to use to select the relevant data from the database
     * @var string
     */
    private $criteria;

    /**
     * Summary report or detailed
     * @var bool
     */
    private $summary = false;

    /**
     * Database join for some reports
     * @var string
     */
    private $join = '';

    /**
     * Default database table to query
     * @var string
     */
    private $main_table = 'process_run';

    /**
     * SQL query for call attempts activities report
     * @var string
     */
    private $call_attempts_activities_summary_sql = "count(distinct ca.transaction_id) as calls,
                    count(case when ca.talk >0 then ca.id end) as talk_calls,
                    concat(floor(sum(ca.dial_time) /3600), 
                        ':', concat(case when length(floor((sum(ca.dial_time) - floor(sum(ca.dial_time) /3600) *3600) /60)) =1 then '0' else '' end,
                                floor((sum(ca.dial_time) - floor(sum(ca.dial_time) /3600) *3600) /60)), 
                        ':', concat( case when length(sum(ca.dial_time) - floor(sum(ca.dial_time) /3600) *3600 - floor((sum(ca.dial_time) - floor(sum(ca.dial_time) /3600) *3600) /60) *60) =1 then '0' else '' end,
                                sum(ca.dial_time) - floor(sum(ca.dial_time) /3600) *3600 - floor((sum(ca.dial_time) - floor(sum(ca.dial_time) /3600) *3600) /60) *60)
                    ) as dial_time,
                    avg(case when ca.talk >0 then ca.dial_time end) as avg_dial_time,
                    concat(floor(sum(ca.ivr_time) /3600), 
                        ':', concat(case when length(floor((sum(ca.ivr_time) - floor(sum(ca.ivr_time) /3600) *3600) /60)) =1 then '0' else '' end,
                                floor((sum(ca.ivr_time) - floor(sum(ca.ivr_time) /3600) *3600) /60)), 
                        ':', concat( case when length(sum(ca.ivr_time) - floor(sum(ca.ivr_time) /3600) *3600 - floor((sum(ca.ivr_time) - floor(sum(ca.ivr_time) /3600) *3600) /60) *60) =1 then '0' else '' end,
                                sum(ca.ivr_time) - floor(sum(ca.ivr_time) /3600) *3600 - floor((sum(ca.ivr_time) - floor(sum(ca.ivr_time) /3600) *3600) /60) *60)
                    ) as ivr_time,
                    avg(case when ca.talk >0 then ca.ivr_time end) as avg_ivr_time,
                    concat(floor(sum(ca.call_wait_time) /3600), 
                        ':', concat(case when length(floor((sum(ca.call_wait_time) - floor(sum(ca.call_wait_time) /3600) *3600) /60)) =1 then '0' else '' end,
                                floor((sum(ca.call_wait_time) - floor(sum(ca.call_wait_time) /3600) *3600) /60)), 
                        ':', concat( case when length(sum(ca.call_wait_time) - floor(sum(ca.call_wait_time) /3600) *3600 - floor((sum(ca.call_wait_time) - floor(sum(ca.call_wait_time) /3600) *3600) /60) *60) =1 then '0' else '' end,
                                sum(ca.call_wait_time) - floor(sum(ca.call_wait_time) /3600) *3600 - floor((sum(ca.call_wait_time) - floor(sum(ca.call_wait_time) /3600) *3600) /60) *60)
                    ) as call_wait_time,
                    avg(case when ca.talk >0 then ca.call_wait_time end) as avg_call_wait_time,
                    concat(floor(sum(ca.talk) /3600), 
                        ':', concat(case when length(floor((sum(ca.talk) - floor(sum(ca.talk) /3600) *3600) /60)) =1 then '0' else '' end,
                                floor((sum(ca.talk) - floor(sum(ca.talk) /3600) *3600) /60)), 
                        ':', concat( case when length(sum(ca.talk) - floor(sum(ca.talk) /3600) *3600 - floor((sum(ca.talk) - floor(sum(ca.talk) /3600) *3600) /60) *60) =1 then '0' else '' end,
                                sum(ca.talk) - floor(sum(ca.talk) /3600) *3600 - floor((sum(ca.talk) - floor(sum(ca.talk) /3600) *3600) /60) *60)
                    ) as talk,
                    avg(case when ca.talk >0 then ca.talk end) as avg_talk,
                    concat(floor(sum(ca.wrap) /3600), 
                        ':', concat(case when length(floor((sum(ca.wrap) - floor(sum(ca.wrap) /3600) *3600) /60)) =1 then '0' else '' end,
                                floor((sum(ca.wrap) - floor(sum(ca.wrap) /3600) *3600) /60)), 
                        ':', concat( case when length(sum(ca.wrap) - floor(sum(ca.wrap) /3600) *3600 - floor((sum(ca.wrap) - floor(sum(ca.wrap) /3600) *3600) /60) *60) =1 then '0' else '' end,
                                sum(ca.wrap) - floor(sum(ca.wrap) /3600) *3600 - floor((sum(ca.wrap) - floor(sum(ca.wrap) /3600) *3600) /60) *60)
                    ) as wrap,
                    avg(case when ca.talk >0 then ca.wrap end) as avg_wrap";

    /**
     * Table to query for call attempts activities report
     * @var string
     */
    private $call_attempts_activities_from_table_sql = "call_attempts ca";

    /**
     * Where clause for call attempts activities report
     * @var string
     */
    private $call_attempts_activities_where_sql = "ca.start >= CURRENT_DATE";

    /**
     * SQL query for dispositions activities report
     * @var string
     */
    private $dispositions_activities_summary_sql = "count(distinct ca.transaction_id) as calls,
                    count(distinct case when ca.talk >0 then ca.transaction_id end) as talk_calls,
                    count(distinct d.transaction_id) as dispositions,
                    count(distinct case when d.disposition_code in (512, 503) or d.description like '%resolution%' or d.description like '%complete%' then d.transaction_id end) as outcomes";

    /**
     * Table/s to query for dispositions activities report
     * @var string
     */
    private $dispositions_activities_from_table_sql = "call_attempts ca
                    left join dispositions d on ca.transaction_id = d.transaction_id";

    /**
     * Where clause for dispositions activities report
     * @var string
     */
    private $dispositions_activities_where_sql = "ca.start >= CURRENT_DATE";

    /**
     * SQL query for agent states activities report
     * @var string
     */
    private $agent_states_activities_summary_sql = "count(a.id) as state_changes,
                count(distinct a.user_id) as users,
                count(distinct a.team_id) as teams";

    /**
     * Table to query for agent states activities report
     * @var string
     */
    private $agent_states_activities_from_table_sql = "agent_states a";

    /**
     * Where clause for agent states activities report
     * @var string
     */
    private $agent_states_activities_where_sql = "a.start >= CURRENT_DATE";

    /**
     * Order by clause for agent states activities report
     * @var string
     */
    private $agent_states_activities_order_sql = "a.list_id";


    /**
     * data_view constructor.
     *
     * @param        $process_type
     * @param string $process
     * @param int    $report_mins
     * @param bool   $summary
     * @param bool   $activities_today
     * @param bool   $activities_by_list
     */
    public function __construct($process_type,
                                $process = '',
                                $report_mins = 120,
                                $summary = false,
                                $activities_today = false,
                                $activities_by_list = false) {
        $this->report_mins = is_numeric($report_mins) ? htmlentities($report_mins) : $this->report_mins;

        $this->summary = $summary;

        $this->process_type = $process_type;


        if (strlen($process)) {
            $this->set_process($process);
        } else {
            $this->set_join();
        }


        $activities_today ?: $this->set_main_table();


        $this->set_report_table($activities_today, $activities_by_list);
    }


    /**
     * Sets the default table to query for the report
     */
    private function set_main_table() {
        $this->main_table = $this->process_type == 'error_log' ? $this->process_type : $this->main_table;
    }


    /**
     * Sets the SQL code query join clause
     */
    private function set_join() {
        if ($this->process == '') {
            if ($this->process_type == 'data_download') {
                $this->join = "left join data_download d on p.batch_id = d.batch_id and p.process = d.download_type";
            } else {
                $this->join = "left join update_data d on p.batch_id = d.batch_id and p.id = d.sub_id";
            }
        }
    }


    /**
     * Sets the columns for the report SQL query
     */
    private function set_columns() {
        if ($this->process_type == 'error_log') {
            $this->columns = "p.created_at, p.updated_at, p.batch_id, p.subject, p.subject, p.message, p.sent, p.response";

            return;
        }


        /**
         * Summary report columns
         */
        if ($this->summary) {
            $this->columns = "
                min(TIMEDIFF(case when completed =1 then p.updated_at else now() end, p.created_at)) as min_duration,
                max(TIMEDIFF(case when completed =1 then p.updated_at else now() end, p.created_at)) as max_duration,
                concat(avg(TIMEDIFF(case when p.completed =1 then p.updated_at else now() end, p.created_at)), ' seconds') as avg_duration,
                count(distinct p.id) as cycles,
                sum(p.completed) as complete,
                count(distinct p.id) - sum(p.completed) as incomplete
            ";
        } else {
            /**
             * Columns for non-summary reports
             */
            $this->columns = "
                p.created_at,
                p.updated_at,
                p.batch_id,
                p.completed,
                TIMEDIFF(case when p.completed =1 then p.updated_at else now() end, p.created_at) as duration,
                TIMEDIFF(now(), p.created_at) as time_since_start,
                TIMEDIFF(now(), p.updated_at) as time_since_end
            ";
        }


        if ($this->process == '') {
            $this->columns .= ", p.process, d.request, d.response";
        }
    }


    /**
     * Sets the where clause for the SQL code
     */
    private function set_criteria() {
        if ($this->process_type == 'error_log') {
            $this->criteria = " p.created_at >= CURRENT_DATE ";
        } else {
            $process = strlen($this->process) ? " and p.process = '" . $this->process . "'" : '';

            $this->criteria = " time_to_sec(TIMEDIFF(now(), p.created_at)) /60. <= ? and p.process_type = '" . $this->process_type . "' $process";
        }
    }


    /**
     * Sets the report process
     * @param $process
     */
    private function set_process($process) {
        $this->process = $process;
    }


    /**
     * Gets the SQL code for the daily activities reports
     * @return string
     */
    private function get_activities_today_sql() {
        if ($this->process == 'call_attempts') {
            return "
                select
                  $this->call_attempts_activities_summary_sql
                from $this->call_attempts_activities_from_table_sql
                where $this->call_attempts_activities_where_sql
                  ;
                ";
        } elseif ($this->process == 'dispositions') {
            return "
                select
                    $this->dispositions_activities_summary_sql
                from $this->dispositions_activities_from_table_sql
                where $this->dispositions_activities_where_sql
                  ;
                ";
        }

        return "
            select
                $this->agent_states_activities_summary_sql
            from $this->agent_states_activities_from_table_sql
            where $this->agent_states_activities_where_sql
            order by $this->agent_states_activities_order_sql
              ;
            ";
    }


    /**
     * Gets the SQL code for the daily activities reports by lists
     * @return string
     */
    private function get_activities_today_by_list_sql() {
        if ($this->process == 'call_attempts') {
            return "
                select
                    ca.list_id,
                    case when ca.list_id =0 then 'Incoming' else l.list_name end as list_name,
                    $this->call_attempts_activities_summary_sql
                from $this->call_attempts_activities_from_table_sql
                    left join lists l on ca.list_id = l.list_id
                where $this->call_attempts_activities_where_sql
                group by ca.list_id, case when ca.list_id =0 then 'Incoming' else l.list_name end
                order by ca.list_id
                  ;
                ";
        } elseif ($this->process == 'dispositions') {
            return "
                select
                    ca.list_id,
                    case when ca.list_id =0 then 'Incoming' else l.list_name end as list_name,
                    $this->dispositions_activities_summary_sql
                from $this->dispositions_activities_from_table_sql
                    left join lists l on ca.list_id = l.list_id
                where $this->dispositions_activities_where_sql
                group by ca.list_id, case when ca.list_id =0 then 'Incoming' else l.list_name end
                order by ca.list_id
                  ;
                ";
        }

        return "
            select
                a.list_id,
                case when a.list_id =0 then 'Incoming or Other type activity' else l.list_name end as list_name,
                $this->agent_states_activities_summary_sql
            from $this->agent_states_activities_from_table_sql
                left join lists l on a.list_id = l.list_id
            where $this->agent_states_activities_where_sql
            group by a.list_id, case when a.list_id =0 then 'Incoming or Other type activity' else l.list_name end
            order by $this->agent_states_activities_order_sql
              ;
            ";
    }


    /**
     * Sets the array for the reports from the database data
     * @param bool $activities_today
     * @param bool $activities_by_list
     */
    private function set_report_array($activities_today = false, $activities_by_list = false) {
        if ($activities_today) {
            if ($activities_by_list) {
                $sql = $this->get_activities_today_by_list_sql();
            } else {
                $sql = $this->get_activities_today_sql();
            }
        } else {
            if ($this->process_type == 'lists_accounts') {
                $sql = "
                select
                    p.list_id, l.list_name, count(p.id) as accounts
                    , count(case when p.lms_status <>4 then p.id end) as active
                    , count(case when p.lms_status =4 then p.id end) as inactive
                from current_accounts p 
                    left join lists l on p.list_id = l.list_id
                group by p.list_id, l.list_name
                order by l.id;
                ";
            } else {
                $this->set_columns();

                $this->set_criteria();

                $sql = "select " . $this->columns . " from $this->main_table p " . $this->join . " where " . $this->criteria . " order by p.id desc limit 200;";
            }
        }


        $this->report_array = DB::select($sql, [$this->report_mins]);

        $this->set_report_rows();
    }


    /**
     * Sets the number of rows for the report
     */
    private function set_report_rows() {
        $this->report_rows = count($this->report_array);
    }


    /**
     * Returns the number of rows for the report
     * @return int
     */
    public function get_report_rows() {
        return $this->report_rows;
    }


    /**
     * Sets the HTML table for the report
     * @param bool $activities_today
     * @param bool $activities_by_list
     */
    private function set_report_table($activities_today = false, $activities_by_list = false) {
        $this->set_report_array($activities_today, $activities_by_list);


        if ($this->report_rows) {
            $id = $this->report_rows;

            $class = str_replace(' ', '_', $this->process_type)
                . "_" . str_replace(' ', '_', $this->process)
                . ($this->summary ? "_summary" : "")
                . ($activities_today ? "_today" : "")
                . ($activities_by_list ? "_bylist" : "");


            if ($this->summary || $class == "lists_accounts_List_accounts") {
                $hide_rows = "";
            } else {
                $hide_rows = "
            <style>
                .$class {
                    display: none;
                }
            </style>";
            }


            $this->report_table = "$hide_rows
            
            <div>
                <button onclick='$(\".$class\").toggle();' style='margin: 1em;'>Toggle Table Rows</button>
            </div>
            
            <div>
                <table>
                    <tr>
                        <th>row</th>";

            foreach ($this->report_array[0] as $item => $value) {
                $this->report_table .= "<th>$item</th>";
            }

            $this->report_table .= "</tr>";


            foreach ($this->report_array as $report) {
                $this->report_table .= "<tr class='$class'><td>$id</td>";

                foreach ($report as $item => $value) {
                    $this->report_table .= "<td>$value</td>";
                }

                $this->report_table .= "</tr>";

                $id -= 1;
            }

            $this->report_table .= "
                </table>
            </div>
            
            <hr>";
        } else {
            $this->report_table = "<div>No report data for the past " . $this->report_mins . " minutes!!</div>";
        }
    }


    /**
     * Returns the report table as a string
     * @return string
     */
    public function get_report_table() {
        return $this->report_table;
    }
}